# Ptits Pains GUI

## Description

As in my team we used to bring the dinner rolls every Friday, I said to myself why not create an application that can manage this.

Petits pains (which means dinner rolls in english) is a tool that allows you to schedule the days of rolls within a team. It allows you to add a user, to calculate his next dinner rolls day and to mention what he will bring back that day.
This tool also offers an administrator area which allows you to manage users, configure the application (choose the day of dinner rolls for example) and other functionalities which I will present later.

I used React and Material UI to build the frontend part and for business part i used the firebase functions. Besides, I used several services of the firebase platform such as storage, functions, authentication and database (RealTime DataBase).

I invite you to see the repo git on which I put the backend part and which describes the data model and the role of each of our business functions.

- [Ptits pains functions](https://gitlab.com/ptits-pains/ptits-pains-functions)

## Getting started

### Live demo

You can view a live demo over at https://ptits-pains.firebaseapp.com

you can access the app via these accounts :

* Admin Account

| User  | Password  |
|---|---|
|  admin@gmail.com | password  |

* Users Accounts

| User  | Password  |
|---|---|
| bret@gmail.com  | password  |
|  glen@gmail.com |  password |
|  joel@gmail.com |  password |
|  dorothy@gmail.com |  password |
|  janett@gmail.com |  password |
|  deeanna@gmail.com |  password |

or you can add a new user if your want (use the subscribe page).

### Functionality overview

#### General functionality

* Authenticate users
* Subscribe users

I used the firebase authentication api to make this functionalities.

#### The general page breakdown looks like this

* Main page (URL : / )
  * Authenticate users
  
* Sign up page (URL : /signup )
  * Subscribe users

* Home page (URL : /dashboard )
  * Show the calendar of the current month : this calendar displays holidays, non working days and users who will bring back dinner rolls this month
  * For each user/rolls day a card will be displayed to show users participation for this day

* Switch page (URL : /switch )
  * User can switch his rolls day with a college via this page

* Requests page (URL : /requests )
  * For each switch days request a record will be displayed in this page to show the advance status of your request

* Notifications page (URL : /notifications )
  * after each request, the concerned user will receive a notification and can either accept or delete the switch days request

* Account settings page (URL : /account/settings )
  * Change password
  * Change Email Address
  * Change language : the user can change the app language. The app supports two languages French and English
  * Delete its account

* Account page (URL : /account )
  * Change users data : personal data like first name, last name ...
  * Change user picture

**Admin Area**

* Users page (URL : /users )
  * Add administrators
  * Delete accounts

* Schedule page (URL : /schedule )
  * force a rolls day : the backend part is the responsible for ordering and scheduling the dinner rolls days. But, if the admin would like the change the calculated date of a user (it's a holiday that the admin forget to add it) he can do it via this page


* Settings page (URL : /settings )
  * Configure the dinner rolls day, the non working days and the holidays : this data will be used to calculate the dinner rolls days


* Technologies page (URL : /technologies )
  * the used frameworks, libraries and tools in this application

#### ScreenShots

* Dashboard Page
![dashboard](/uploads/6168a36fadb98d220c8559389b2c64f2/dashboard.png)

* Schedule Page
![schedule](/uploads/1f3abfbf8e8fe7bf9fe7eba263f90d60/schedule.png)

* Settings Page
![settings](/uploads/2b33d3af26bb22377735fe0f4402692b/settings.png)

## Requirements

For development, you will only need Node.js installed on your environment.

### Node

[Node](http://nodejs.org/) is really easy to install & now include [NPM](https://npmjs.org/).
You should be able to run the following command after the installation procedure
below.

    $ node --version
    v10.16.3

    $ npm --version
    6.9.0

#### Node installation on OS X

You will need to use a Terminal. On OS X, you can find the default terminal in
`/Applications/Utilities/Terminal.app`.

Please install [Homebrew](http://brew.sh/) if it's not already done with the following command.

    $ ruby -e "$(curl -fsSL https://raw.github.com/Homebrew/homebrew/go/install)"

If everything when fine, you should run

    brew install node

#### Node installation on Linux

    sudo apt-get install python-software-properties
    sudo add-apt-repository ppa:chris-lea/node.js
    sudo apt-get update
    sudo apt-get install nodejs

#### Node installation on Windows

Just go on [official Node.js website](http://nodejs.org/) & grab the installer.
Also, be sure to have `git` available in your PATH, `npm` might need it.

## Installation

    $ git clone https://gitlab.com/ptits-pains/ptits-pains-gui.git
    $ cd ptits-pains-gui
    $ npm install

### Configure your firebase app

First of all, you have to create a new project on firebase. Then, create a new app and register it with firebase. check the firebase documentation below :

- [Add firebase to your app](https://firebase.google.com/docs/web/setup)

#### Enable authentication

And now it's time to activate the authentication api. In our app we used the simple way, we used the Email/Password authentication. Use this link to enable this method.

- [enable authentication provider](https://firebase.google.com/docs/auth/web/password-auth#before_you_begin)

#### Enable Real Time DataBase

To enable the real time database, Go to :

1. firebase console
2. choose your project
3. go to Database
4. don't choose the cloud fire store, scroll down
5. click on create a database within the *Realtime Database* area
6. click on activate
7. wait until it finishes loading, go to rules and use this config

```json
{
  "rules": {
    ".read": true,
    ".write": "auth != null",
      "users": {
        ".read": true,
          ".write": "!data.exists()"
    }
  }
}
```

#### Enable Storage

You have to enable the storage on your project. It's pretty simple, follow this steps :

1. go to the firebase console
2. select storage
3. Click on begin
4. choose you zone
5. click on next

Have a look on this link :
- [https://firebase.google.com/docs/storage/android/start#create-default-bucket](https://firebase.google.com/docs/storage/android/start#create-default-bucket)

Use the default rules :

```json
rules_version = '2';
service firebase.storage {
  match /b/{bucket}/o {
    match /{allPaths=**} {
      allow read, write: if request.auth != null;
    }
  }
}
```

#### Install the firebase functions

check the functions project to install them :

- [Ptits pains functions](https://gitlab.com/ptits-pains/ptits-pains-functions)

#### Add your app config file

Copy the content of your firebase config file. This link will help you :

- [https://support.google.com/firebase/answer/7015592?hl=en](https://support.google.com/firebase/answer/7015592?hl=en)

And place it in `src/config/firebase.js`. Try to paste only the content of the object (within the initializeApp function) like this example :

```json
const fireApp = firebase.initializeApp({
  apiKey: "AIzaSyCnF1yrweEcsVRI5aB-fdyn2ZvN-r_rK_w",
  authDomain: "ptits-pains.firebaseapp.com",
  databaseURL: "https://ptits-pains.firebaseio.com",
  projectId: "ptits-pains",
  storageBucket: "ptits-pains.appspot.com",
  messagingSenderId: "494386304277",
  appId: "1:494386304277:web:2912999582b9b758a1051c",
  measurementId: "G-4XC7QD4BDT"
});
```

#### Configure the app

Start the application using this command :

    $ npm start

Create an account via this link :

- [sign up](http://localhost:3000/signup)

Now you have to go the firebase real time database and make this user an administrator by adding a new property called isAdmin and initialize its value to true.

![make_admin](/uploads/eba8c781e0aef6c7c9892cac8670e370/make_admin.png)

It's simple and you have to do it once. The next time you need to add an admin you can use the app (/users).

![users](/uploads/21384062dffe1f419a432a0e289c9712/users.JPG)

Log in using this link :

- [login](http://localhost:3000)

And go to settings :

- [settings](http://localhost:3000/settings)

Well, now you can add holidays, non working day and (most important) the dinner rolls day. 

![settings](/uploads/77e4361b18bccad0eb77b80c5dfcd443/settings.JPG)

Be careful with the dinner rolls day area, you must init it once and don't change it after. The first time you init this data a function will be triggered to calculate the dinners rolls days of the app.

### Start & watch

    $ npm start

### Simple build for production

    $ npm run build

## Docker Image

You can build a docker image using this commands :

    $ docker build -t petits-pains-gui:v1.0.0 .

    $ docker run --rm --publish 3000:80 petits-pains-gui:v1.0.0

## Deploy the app on firebase

You can use the firebase hosting to deploy your app on the net. To do so, follow these steps :

1. to build the project use this command

  $ npm run build

2. make sure that you have installed the firebase sdk (check the functions project)

3. log into your firebase account using this command

  $ firebase login

4. update the .firebaserc file by setting your project id, then use this command to deploy the app

  $ firebase deploy

## Languages & tools

### React

- [React](https://reactjs.org/) is a JavaScript library for building user interfaces.

### Redux

- [Redux](https://reactjs.org/) helps to centralize your application's state and logic enables powerful capabilities like undo/redo, state persistence, and much more.

### Material UI

- [Material-UI](https://material-ui.com/getting-started/installation/) is an excellent framework to develop beautiful React components faster.

### JSX

- [JSX](https://reactjs.org/docs/glossary.html#jsx) is a syntax extension to JavaScript. It is similar to a template language, but it has full power of JavaScript.

### Moment JS

- [Moment JS](https://momentjs.com/) is a library used to parse, validate, manipulate, and display dates and times in JavaScript..

### Visual Studio Code

- [Visual Studio Code](https://code.visualstudio.com/) is a lightweight but powerful source code editor which runs on your desktop and is available for Windows, macOS and Linux. It comes with built-in support for JavaScript, TypeScript and Node.js .

### Static server with Livereload

The app embed for development a static connect server with livereload plugged.
So each time you start the app, you get automatic refresh in the browser whenever you update a file.

---

## Authors

- Mohamed Ali AMDOUNI