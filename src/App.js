import React, { Suspense } from 'react';
import Layout from './components/Navigation/Layout/Layout';
import { Route, Switch, BrowserRouter } from "react-router-dom";
import SignIn from './containers/Authentication/SignIn/SignIn';
import SignUp from './containers/Authentication/SignUp/SignUp';
import DashBoard from './containers/DashBoard/DashBorad';
import Account from './containers/Account/Account';
import Settings from './containers/Settings/Settings';
import LogOut from './containers/Authentication/LogOut/LogOut';
import * as appRoutes from './constants/routes';
import SwitchUsers from './containers/SwitchUsers/SwitchUsers';
import Notifications from './containers/Notifications/Notifications';
import Requests from "./containers/Requests/Requests";
import AccountSettings from "./containers/AccountSettings/AccountSettings";
import ManageUsers from "./containers/ManageUsers/ManageUsers";
import ScheduleDays from "./containers/ScheduleRollsDays/ScheduleRollsDays";
import Technologies from "./components/Navigation/Content/Technologies/Technologies";
import Page404 from "./components/UI/404/404";
import { connect } from 'react-redux';

class App extends React.Component {

  render() {

    let routes = (
      <BrowserRouter>
        <Switch>
          <Route exact path='/' component={SignIn} />
          <Route path={appRoutes.SIGN_UP} component={SignUp} />
          <Route path={appRoutes.ACCOUNT_SETTINGS} render={() => <Layout><AccountSettings /></Layout>} />
          <Route path={appRoutes.DASH_BOARD} render={() => <Layout><DashBoard /></Layout>} />
          <Route path={appRoutes.ACCOUNT} render={() => <Layout><Account /></Layout>} />
          <Route path={appRoutes.SWITCH} render={() => <Layout><SwitchUsers /></Layout>} />
          <Route path={appRoutes.NOTIFICATIONS} render={() => <Layout><Notifications /></Layout>} />
          <Route path={appRoutes.REQUESTS} render={() => <Layout><Requests /></Layout>} />
          <Route path={appRoutes.TECHNOLOGIES} render={() => <Layout><Technologies /></Layout>} />
          <Route path={appRoutes.LOG_OUT} component={LogOut} />
          <Route component={Page404} />
        </Switch>
      </BrowserRouter>
    )
    if(this.props.isAdmin){
      routes = (
        <BrowserRouter>
          <Switch>
            <Route exact path='/' component={SignIn} />
            <Route path={appRoutes.SIGN_UP} component={SignUp} />
            <Route path={appRoutes.ACCOUNT_SETTINGS} render={() => <Layout><AccountSettings /></Layout>} />
            <Route path={appRoutes.DASH_BOARD} render={() => <Layout><DashBoard /></Layout>} />
            <Route path={appRoutes.USERS} render={() => <Layout><ManageUsers /></Layout>} />
            <Route path={appRoutes.ACCOUNT} render={() => <Layout><Account /></Layout>} />
            <Route path={appRoutes.SETTINGS} render={() => <Layout><Settings /></Layout>} />
            <Route path={appRoutes.SWITCH} render={() => <Layout><SwitchUsers /></Layout>} />
            <Route path={appRoutes.NOTIFICATIONS} render={() => <Layout><Notifications /></Layout>} />
            <Route path={appRoutes.REQUESTS} render={() => <Layout><Requests /></Layout>} />
            <Route path={appRoutes.TECHNOLOGIES} render={() => <Layout><Technologies /></Layout>} />
            <Route path={appRoutes.SCHEDULE} render={() => <Layout><ScheduleDays /></Layout>} />
            <Route path={appRoutes.LOG_OUT} component={LogOut} />
            <Route component={Page404} />
          </Switch>
        </BrowserRouter>
      );
    }

    return (
      <React.Fragment>
        <Suspense fallback={(<div>Loading</div>)}>
          {routes}
        </Suspense>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
      isAdmin: state.userDetails.isAdmin
  };
};

export default connect(mapStateToProps)(App);
