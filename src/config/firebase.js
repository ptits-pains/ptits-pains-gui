import Rebase from "re-base";
import firebase from "firebase/app";
import "firebase/database";
import "firebase/storage";

const fireApp = firebase.initializeApp({
  apiKey: "AIzaSyCnF1yrweEcsVRI5aB-fdyn2ZvN-r_rK_w",
  authDomain: "ptits-pains.firebaseapp.com",
  databaseURL: "https://ptits-pains.firebaseio.com",
  projectId: "ptits-pains",
  storageBucket: "ptits-pains.appspot.com",
  messagingSenderId: "494386304277",
  appId: "1:494386304277:web:2912999582b9b758a1051c",
  measurementId: "G-4XC7QD4BDT"
});

//init data base
const fireBase = Rebase.createClass(firebase.database());

//init cloud storage
const storage = fireApp.storage();

export { fireApp, storage, fireBase as default };
