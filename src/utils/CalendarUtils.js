import * as dateUtils from './DateUtils';
import * as arrayUtils from './ArrayUtils';
import * as objectUtils from './ObjectUtils';

/**
 * configure the current month of the calendar
 * init holidays
 * init non working days
 * init rolls day
 */
export const configure = (nonWorkingDays, rollsDay, holidays, month, rollsMonth) => {

    //first sunday of the month or the last of the previous month
    let startOfCurrentCalendar = month
        .clone()
        .startOf("month")
        .day(0);

    //first saturday of the next month
    let endOfCurrentCalendar = month
        .clone()
        .endOf("month")
        .day(6);

    let nbDays = dateUtils.diffTwoDatesDays(startOfCurrentCalendar, endOfCurrentCalendar);
    let calendarDetails = new Map();
    let calendarDay = startOfCurrentCalendar;
    for (let i = 0; i <= nbDays; i++) {

        let details = {}
        let currentDayWeek = "" + calendarDay.day();
        if (arrayUtils.contains(nonWorkingDays, currentDayWeek)) {

            details.isNonWorkingDay = true;
        }

        if (currentDayWeek === rollsDay) {

            details.isRollsDay = true;
        }

        //check if the current day is a holiday
        if (holidays)
            Object.keys(holidays).forEach(item => {

                if (
                    dateUtils.isSameDay(
                        dateUtils.stringToMomentDate(item),
                        calendarDay
                    )
                ) {
                    details.holidays = {};
                    details.holidays.isHoliday = true;
                    details.holidays.details = objectUtils.updateObject(holidays[item]);
                }
            })

        //check if there's a user assigned to this day
        let dateKey = dateUtils.momentDateToString(calendarDay);
        if (rollsMonth && rollsMonth.has(dateKey)) {

            details.userDetails = rollsMonth.get(dateKey);
        }

        // add the details to resulted map
        calendarDetails.set(
            dateUtils.momentDateToString(calendarDay),
            details
        )
        calendarDay.add(1, "day");
    }

    // console.log('calendarDetails',calendarDetails)
    return calendarDetails;
}