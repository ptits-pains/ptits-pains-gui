/**
 * check if the passed string is empty or not
 * @param {string} str the string to check
 */
export const isEmpty = str => {
  if (!str || str.trim() === "") {
    return true;
  }
  return false;
};

/**
 * check if a string has a minimum size of
 * @param {string} str the string to test
 * @param {number} size the size of the string to check
 */
export const hasMinSize = (str,size) => {

  return str.length >= size;
}

/**
 * check if a string does not have a minimum size of
 * @param {string} str the string to test
 * @param {number} size the size of the string to check
 */
export const doesNotHasMinSize = (str,size) => {

  return !hasMinSize(str,size);
}

/**
 * checks string equality
 * @param {string} str1 
 * @param {string} str2 
 */
export const isEqual = (str1,str2) => {

  if(!str1 || !str2){
    return false;
  }
  return str1 === str2;
}