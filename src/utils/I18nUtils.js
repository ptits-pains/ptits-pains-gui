import i18n from '../i18n';

/**
 * get used language
 */
export const getLanguage = () => {

    return i18n.languages[0];
}

/**
 * returns true if the current language is french
 */
export const isFrenchLanguage = () => {

    return getLanguage() === "fr";
}