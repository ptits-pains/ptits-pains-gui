import * as stringUtils from './StringUtils';
/**
 * get initails of the subscribed user
 * @param {string} firstName user first name
 * @param {string} lastName user last name
 */
export const getInitials = (firstName, lastName) => {

    var res = "";
    if(stringUtils.isEmpty(firstName)){
        res = "X";
    }else{
        res=firstName.charAt(0);
    }

    if(stringUtils.isEmpty(lastName)){
        res+="XX";
    }else{
        res+= lastName.charAt(0) + lastName.charAt(lastName.length-1);
    }

    return res.toUpperCase();
}