/**
 * remove en element from an array
 * @param {primitive Object} element element to remove
 * @param {Array} array js Array
 */
export const removeElement = (element, array) => {

    if (!array || array.size === 0) return;
    let index = array.indexOf(element);
    if (index !== -1) {
        array.splice(index, 1);
    }
}

/**
 * returns true if the the array contains the element false otherwise
 * @param {Array} array a js array
 * @param {primitive} element element to find
 */
export const contains = (array, element) => {

    //TODO change it to use array.includes(element)
    if (!array || array.size === 0) return false;
    if (array.indexOf(element) !== -1) {
        return true;
    }
    return false;
}