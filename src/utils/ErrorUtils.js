/**
 * in this function we will build an error object the result will be like an array this
 * [
 *  {
 *      errorMessage:  __errorMessage,
 *      toShowError:   __toShowError
 *  },
 *  ...
 * ]
 */
export const buildError = objError => {

    if (!objError) return null;
    if (objError.length % 2 !== 0) return null;

    let result = [];
    for (let i = 0; i < objError.length; i = i + 2) {
        result.push({
            errorMessage: objError[i],
            toShowError: objError[i + 1]
        })
    }
    return result;
}