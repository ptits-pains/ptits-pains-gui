
/**
 * transform a js firebase object to a map
 * @param {Object} obj google object
 */
export const fireObjectToMap = obj => {

    let map = new Map();
    Object.keys(obj).forEach(key => {

        map.set(key, obj[key])
    });
    return map;
}

/**
 * returns true if two maps matches
 * @param {Map 1} map1 map
 * @param {Map 2} map2 map
 */
export function compareMaps(map1, map2) {
    if (!map1 && !map2) {
        return true;
    }

    if ((!map1 && map2) || (map1 && !map2)) {
        return false;
    }

    if (map1.size !== map2.size) {
        return false;
    }

    let testVal;
    for (var [key, val] of map1) {
        testVal = map2.get(key);
        // in cases of an undefined value, make sure the key
        // actually exists on the object so there are no false positives
        if (
            (testVal === undefined && !map2.has(key)) ||
            !isEquivalent(testVal, val)
        ) {
            return false;
        }
    }
    return true;
}

/**
 * this will compares 2 objects that has only primitives properties (string in our case)
 * @param {object 1} a first object to compare
 * @param {object 2} b second object to compare
 */
function isEquivalent(a, b) {
    // Create arrays of property names
    var aProps = Object.getOwnPropertyNames(a);
    var bProps = Object.getOwnPropertyNames(b);

    // If number of properties is different,
    // objects are not equivalent
    if (aProps.length !== bProps.length) {
        return false;
    }

    for (var i = 0; i < aProps.length; i++) {
        var propName = aProps[i];

        // If values of same property are not equal,
        // objects are not equivalent
        if (a[propName] !== b[propName]) {
            return false;
        }
    }

    // If we made it this far, objects
    // are considered equivalent
    return true;
}

/**
 * test if a map is empty or not
 * @param {Map} obj the map
 */
export const isEmpty = obj => {
    if (!obj || obj.size === 0) {
        return true;
    }
    return false;
};

/**
 * get the values of a map
 * @param {Map} map a js map
 */
export const getMapValues = map => {
    if (isEmpty(map)) return [];
    return Array.from(map, ([key, value]) => value);
};

/**
 * return the map keys
 * @param {Map} myMap map
 */
export const getMapKeys = myMap => {
    if (isEmpty(myMap)) return [];
    return [ ...myMap.keys() ];
}

//  loop through a map
// for (const [key, value] of myMap.entries()) {
//     console.log(key, value);
// }
