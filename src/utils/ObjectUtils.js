
/**
 * add new properties to an existent object
 * @param {Object} oldObject old object
 * @param {Object} updatedProperties new properties
 */
export const updateObject = (oldObject, updatedProperties) => {
    return {
        ...oldObject,
        ...updatedProperties
    };
};

/**
 * as it's name said this method will check if one (or many) of the values of the object is null and
 * will initialize it to an empty string
 * @param {Object} obj the passed object
 */
export const setToEmptyIfNull = obj => {

    if (!obj) return "";
    Object.keys(obj).forEach(key => {

        if (!obj[key]) obj[key] = "";
    })
    return obj;
}

