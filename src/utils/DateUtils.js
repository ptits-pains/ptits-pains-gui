import moment from "moment";

/**
 * used to convert the input date to a string date in this format : yyyy-MM-dd
 * @param {Date} date
 */
export const jsDateToString = date => {
  let day = date.getDate();
  let monthIndex = date.getMonth() + 1;
  let year = date.getFullYear();
  day = day < 10 ? "0" + day : day;
  monthIndex = monthIndex < 10 ? "0" + monthIndex : monthIndex;
  return year + "-" + monthIndex + "-" + day;
};

/**
* format and return a js date from a string date
* @param {string} date date in this format YYYY-MM-DD
*/
export const stringToJsDate = str => {
  if (!str) return null;
  return momentToJsDate(stringToMomentDate(str));
};

/**
 * transform a moment date to a js date
 * @param {Moment} date the moment data
 */
export const momentToJsDate = date => {
  if (!date) return null;
  return date.toDate();
};

/**
 * format and return a string as a date
 * @param {string} str
 */
export const stringToMomentDate = str => {
  return moment(str, "YYYY-MM-DD");
};

/**
 * format and return a string as a date
 * the string may contain the time zone
 * @param {string} str
 */
export const fromStringToMomentDate = str => {
  return moment(str);
};

/**
 * format and return a date as a string
 * @param {moment} date
 */
export const momentDateToString = date => {
  return moment(date).format("YYYY-MM-DD");
};

/**
 * return number of days between two dates (absolute value)
 */
export const diffTwoDatesDays = (date1, date2) => {

  return Math.abs(date2.diff(date1, "days"));
};

/**
 * return number of days between two dates (signed return). dates form is string YYYY-MM-DD
 */
export const realDiffTwoDatesDaysString = (date1, date2) => {

  return stringToMomentDate(date1).diff(stringToMomentDate(date2), "days");
};

/**
 * return true if the dates days (not time) matches
 */
export const isSameDay = (date1, date2) => {
  return date2.isSame(date1, "days");
};

/**
 * return current week number
 */
export const getCurrentWeekNumber = () => {

  return moment().weekYear();
}

/**
 * return current year
 */
export const getCurrentYear = () => {

  return moment().year();
}

/**
 * returns true if the first date is greater or equal than the second date
 * @param {Moment} date1 first date
 * @param {Moment} date2 second date
 */
export const isSameOrAfter = (date1, date2) => {
  if (!date1 || !date2) return false;
  return date1.isSameOrAfter(date2, "day");
};

/**
 * returns true if the first date is lower or equal than the second date
 * @param {Moment} date1 first date
 * @param {Moment} date2 second date
 */
export const isSameOrBefore = (date1, date2) => {
  if (!date1 || !date2) return false;
  return date1.isSameOrBefore(date2, "day");
};

/**
 * checks if date1 is before date2
 * @param {Moment} date1 date 1
 * @param {Moment} date2 date 2
 */
export const isBefore = (date1, date2) => {
  if (!date1 || !date2) return false;
  return date1.isBefore(date2, "day");
};

/**
 * checks if date1 is after date2
 * @param {Moment} date1 date 1
 * @param {Moment} date2 date 2
 */
export const isAfter = (date1, date2) => {
  if (!date1 || !date2) return false;
  return date1.isAfter(date2, "day");
};

/**
 * return time
 */
export const now = () => {
  return moment();
};

/**
 * return current timestamp
 */
export const timestamp = () => {
  return moment().unix();
};

/**
 * return current timestamp
 */
export const isoStringDate = () => {
  return moment().toISOString();
};

/**
 * format and return a month as a string
 * @param {moment} date
 */
export const momentMonthToString = moment => {
  return moment.format("YYYY-MM");
};

/**
 * format a date to a literal month year format
 * @param {Moment} moment date as moment
 */
export const formatToMonthYearLiteral = moment => {
  return moment.format("MMMM YYYY");
};

/**
 * format a date to french date
 * @param {Moment} moment date as moment
 */
export const formatToFullFrenchDate = moment => {
  return "Le " + moment.format("DD MMMM YYYY");
};

/**
 * format a date to french time
 * @param {Moment} moment time as moment
 */
export const formatToFullFrenchTime = moment => {
  return "Le " + moment.format("DD MMMM YYYY, HH:mm:ss");
};