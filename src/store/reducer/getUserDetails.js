import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../../utils/ObjectUtils';

const initialState = {
    createdAt: null,
    emailAddress: null,
    userId: null,
    firstName: null,
    initials: null,
    lastName: null,
    description: null,
    job: null,
    phone: null,
    pictureURL: null,
    isAdmin: null,
    deleteAccount: null,
    error: null,
    loading: false
};

const start = (state, action) => {
    return updateObject(state, { error: null, loading: true });
};

const success = (state, action) => {
    return updateObject(state, {
        createdAt: action.userDetails.createdAt,
        emailAddress: action.userDetails.emailAddress,
        userId: action.userDetails.userId,
        firstName: action.userDetails.firstName,
        initials: action.userDetails.initials,
        lastName: action.userDetails.lastName,
        description: action.userDetails.description,
        job: action.userDetails.job,
        phone: action.userDetails.phone,
        pictureURL: action.userDetails.pictureURL,
        deleteAccount: action.userDetails.deleteAccount,
        isAdmin: action.userDetails.isAdmin,
        error: null,
        loading: false
    });
};

const fail = (state, action) => {
    return updateObject(state, {
        error: action.error,
        loading: false
    });
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.GET_USER_DETAILS_START: return start(state, action);
        case actionTypes.GET_USER_DETAILS_SUCCESS: return success(state, action);
        case actionTypes.GET_USER_DETAILS_FAIL: return fail(state, action);
        default:
            return state;
    }
};

export default reducer;