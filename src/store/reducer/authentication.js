import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../../utils/ObjectUtils';

const initialState = {
    email: null,
    userId: null,
    error: null,
    loading: false
};

const authStart = ( state, action ) => {
    return updateObject( state, { error: null, loading: true } );
};

const authSuccess = (state, action) => {
    return updateObject( state, { 
        userId: action.userId,
        email: action.email,
        error: null,
        loading: false
     } );
};

const authFail = (state, action) => {
    return updateObject( state, {
        error: action.error,
        loading: false
    });
};

const logOutStart = ( state, action ) => {
    return updateObject( state, { error: null } );
};

const logOutSuccess = (state, action) => {
    return updateObject( state, { 
        userId: null,
        email: null,
        error: null
     } );
};

const logOutFail = (state, action) => {
    return updateObject( state, {
        error: action.error
    });
};

const reducer = ( state = initialState, action ) => {
    switch ( action.type ) {
        case actionTypes.AUTH_START: return authStart(state, action);
        case actionTypes.AUTH_SUCCESS: return authSuccess(state, action);
        case actionTypes.AUTH_FAIL: return authFail(state, action);
        case actionTypes.AUTH_LOGOUT_START: return logOutStart(state, action);
        case actionTypes.AUTH_LOGOUT_SUCCESS: return logOutSuccess(state,action);
        case actionTypes.AUTH_LOGOUT_FAIL: return logOutFail(state, action);
        default:
            return state;
    }
};

export default reducer;