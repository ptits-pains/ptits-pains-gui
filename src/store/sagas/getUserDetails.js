import { put } from "redux-saga/effects";
import base from '../../config/firebase';
import * as actions from "../actions/getUserDetails";


export function* getUserDetails(action) {
  
  yield put(actions.start());
  try {
    const response = yield base.fetch(`users/${action.userId}`, {context: this,asArray: false})
    yield put(actions.success(response));
  } catch (error) {
    yield put(actions.fail(error.message));
  }
}
