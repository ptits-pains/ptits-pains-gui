import { takeEvery } from "redux-saga/effects";
import * as actionTypes from "../actions/actionTypes";
import {
  logoutSaga,
  authUserSaga,
} from "./authentication";
import {
  getUserDetails
} from "./getUserDetails";

export function* watch() {

  //authentication
  yield takeEvery(actionTypes.AUTH_LOGOUT, logoutSaga);
  yield takeEvery(actionTypes.AUTH_USER, authUserSaga);

  //get user details
  yield takeEvery(actionTypes.GET_USER_DETAILS, getUserDetails);
}
