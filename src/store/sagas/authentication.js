import { put } from "redux-saga/effects";
import { fireApp } from '../../config/firebase';
import * as actions from "../actions/authentication";

/*
* saga is used to handle all side effects behaviors of our redux because of the redux
* store is synchronous
*/

/**
 * sign out the current user using the firebase api
 * @param {Object} action fired action from SingOut container
 */
export function* logoutSaga(action) {

  yield put(actions.logoutStart());
  try {
    yield fireApp.auth().signOut();
    yield put(actions.logoutSuccess());
  } catch (error) {
    yield put(actions.logoutFail(error.message));
  }
}

/**
 * authenticate user using firebase auth
 * @param {Object} action fired action from SignIn container
 */
export function* authUserSaga(action) {

  yield put(actions.authStart());
  try {
    const response = yield fireApp.auth().signInWithEmailAndPassword(action.email, action.password);
    yield put(actions.authSuccess(response.user.uid, response.user.email));
  } catch (error) {
    yield put(actions.authFail(error.message));
  }
}
