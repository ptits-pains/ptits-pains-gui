import * as actionTypes from "./actionTypes";

// Log in
export const authStart = () => {
  return {
    type: actionTypes.AUTH_START
  };
};

export const authSuccess = (userId, email) => {
  return {
    type: actionTypes.AUTH_SUCCESS,
    userId: userId,
    email: email
  };
};

export const authFail = error => {
  return {
    type: actionTypes.AUTH_FAIL,
    error: error
  };
};

export const authenticate = (email, password) => {
  return {
    type: actionTypes.AUTH_USER,
    email: email,
    password: password
  };
};

// Log out
export const logoutStart = () => {
  return {
    type: actionTypes.AUTH_LOGOUT_START
  };
};

export const logoutSuccess = () => {
  return {
    type: actionTypes.AUTH_LOGOUT_SUCCESS
  };
};

export const logoutFail = error => {
  return {
    type: actionTypes.AUTH_LOGOUT_FAIL,
    error: error
  };
};

export const logout = () => {
  return {
    type: actionTypes.AUTH_LOGOUT
  };
};
