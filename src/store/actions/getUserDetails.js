import * as actionTypes from "./actionTypes";

export const start = () => {
  return {
    type: actionTypes.GET_USER_DETAILS_START
  };
};

export const success = (userDetails) => {
  return {
    type: actionTypes.GET_USER_DETAILS_SUCCESS,
    userDetails: userDetails
  };
};

export const fail = error => {
  return {
    type: actionTypes.GET_USER_DETAILS_FAIL,
    error: error
  };
};

export const process = (userId) => {
  return {
    type: actionTypes.GET_USER_DETAILS,
    userId: userId
  };
};
