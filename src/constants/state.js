export const SENT = 'SENT';
export const IN_PROGRESS = 'IN_PROGRESS';
export const ACCEPTED = 'ACCEPTED';
export const REFUSED = 'REFUSED';
export const DELETED = 'DELETED';
export const DISABLED = 'DISABLED';
