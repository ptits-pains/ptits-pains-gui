
export const ROOT = "/";
export const DASH_BOARD = "/dashboard";
export const SIGN_UP = "/signup";
export const SWITCH = "/switch";
export const REQUESTS = "/requests";
export const USERS = "/users";
export const ACCOUNT = "/account";
export const SETTINGS = "/settings";
export const LOG_OUT = "/logout";
export const NOTIFICATIONS = "/notifications";
export const ACCOUNT_SETTINGS = "/account/settings";
export const TECHNOLOGIES = "/technologies";
export const SCHEDULE = "/schedule";