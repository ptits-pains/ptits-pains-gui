import React from 'react';
import Box from '@material-ui/core/Box';
import IconButton from '@material-ui/core/IconButton';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import Grid from '@material-ui/core/Grid';

export default function Header (props) {

    return (
        <React.Fragment>
            <Grid item xs={2}>
                <Box pt={2}>
                    <IconButton onClick={props.previous} >
                        <ArrowBackIosIcon fontSize="small" />
                    </IconButton>
                </Box>
            </Grid>
            <Grid item xs={8} align='center' >
                <Box pt={2}>
                    {props.month}
                </Box>
            </Grid>
            <Grid item xs={2}>
                <Box pt={2} align="right">
                    <IconButton onClick={props.next} >
                        <ArrowForwardIosIcon fontSize="small" />
                    </IconButton>
                </Box>
            </Grid>
        </React.Fragment>
    );
}