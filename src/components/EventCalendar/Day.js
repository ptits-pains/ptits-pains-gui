import React from "react";
import TableCell from "@material-ui/core/TableCell";
import Box from "@material-ui/core/Box";
import Chip from "@material-ui/core/Chip";
import FlashBadge from "../UI/FlashBadge/FlashBadge";

class Day extends React.Component {

  render() {
    const {
      day: { date, number, selected, setting },
      selectHoliday,
    } = this.props;

    let dayNumber;

    //if the to select day is the same as the current day ==> we gonna style it
    if (selected.isSame(date, "day")) {
      dayNumber = (
        <Chip
          label={number}
          size="small"
          style={{ background: "#C62828", color: "#FFFFFF" }}
        />
      );
    } else {
      dayNumber = <Box m={0.5}>{number}</Box>;
    }

    // we already loaded the calendar settings
    if (setting) {

      if (setting.holidays) {
        return (
          <TableCell
            align="center"
            size="small"
            padding="none"
            key={date.toString()}
            style={{ background: "#F0F0F0" }}
          >
            {dayNumber}
            <Box
              m={1}
              mx={0}
              css={{ cursor: "pointer" }}
              onClick={() => selectHoliday(setting.holidays.details)}
            >
              <FlashBadge />
            </Box>
          </TableCell>
        );
      }

      //if this is a non working day we gonna disable it
      if (setting.isNonWorkingDay) {

        return (
          <TableCell
            align="center"
            size="small"
            padding="none"
            key={date.toString()}
            style={{ background: "#F0F0F0" }}
          >
            {dayNumber}
            <Box m={1} mx={0}>
              .
            </Box>
          </TableCell>
        );
      }

      //if this is the roll day
      if (setting.isRollsDay && setting.userDetails) {

        return (
          <TableCell
            align="center"
            size="small"
            padding="none"
            key={date.toString()}
          >
            {dayNumber}
            <Box
              style={{ background: "#C62828", color: "#FFFFFF" }}
              m={1}
              mx={0}
            >
              {setting.userDetails.initials}
            </Box>
          </TableCell>
        );
      }
    }

    // if we didn't yet load the calendar settings
    return (
      <TableCell
        align="center"
        size="small"
        padding="none"
        key={date.toString()}
      >
        {dayNumber}
        <Box m={1} mx={0}>
          .
        </Box>
      </TableCell>
    );
  }
}

export default Day;
