import React from "react";
import TableRow from "@material-ui/core/TableRow";
import Day from "./Day";
import moment from "moment";
import * as dateUtils from '../../utils/DateUtils';

class Week extends React.Component {
  render() {
    let days = [];
    let { date } = this.props;

    const { month, selected, selectHoliday, settings } = this.props;

    //calculate the date to be selected
    //if the user select a date we will add a style to this date otherwise we will style today
    let toSelectDay = selected;
    if (!selected) {
      toSelectDay = moment();
    }

    for (var i = 0; i < 7; i++) {

      let day = {
        number: date.date(),
        isCurrentMonth: date.month() === month.month(),
        isToday: date.isSame(new Date(), "day"),
        date: date,
        selected: toSelectDay,
        setting: settings ? settings.get(dateUtils.momentDateToString(date)) : null,
        dateIndex: dateUtils.momentDateToString(date),
      };
      days.push(
        <Day
          key={day.dateIndex}
          day={day}
          selectHoliday={selectHoliday}
        />
      );

      date = date.clone();
      date.add(1, "day");
    }

    return <TableRow key={days[0]}>{days}</TableRow>;
  }
}

export default Week;
