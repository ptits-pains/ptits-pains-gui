import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Week from "./Week";
import Header from "./Header";
import LinearProgress from "../../components/UI/LinearProgress/LinearProgress";
import { withTranslation } from 'react-i18next';

const useStyles = () =>
  makeStyles(theme => ({
    root: {
      flexGrow: 1,
      justifyContent: "center",
      flexWrap: "wrap"
    }
  }));

class EventCalendar extends React.Component {

  classes = useStyles();

  constructor(props){
    super(props);
    let {t} = this.props;
    this.weekDays = [
      t('shortWeekDays.sunday'),
      t('shortWeekDays.monday'),
      t('shortWeekDays.tuesday'),
      t('shortWeekDays.wednesday'),
      t('shortWeekDays.thursday'),
      t('shortWeekDays.friday'),
      t('shortWeekDays.saturday'),
    ]
  }

  render() {

    let calendarProgressBar = null;
    let calendarContent = null;

    if (this.props.loading) {
      calendarProgressBar = (
        <TableRow>
          <TableCell colSpan={7}>
            <LinearProgress />
          </TableCell>
        </TableRow>
      );
    }

    if (!this.props.loading) {
      calendarContent = this.renderWeeks();
    }

    return (
      <Grid item xs={12}>
        <Paper className={this.classes.root}>
          <Grid container spacing={0}>
            <Header
              month={this.renderMonthLabel()}
              previous={this.props.previous}
              next={this.props.next}
            />
            <Table className={this.classes.table}>
              <TableHead>
                <TableRow>
                  {this.weekDays.map(day => {

                    return (
                      <TableCell key={day} size="small" padding="none" align="center">
                        {day}
                      </TableCell>
                    )
                  })}
                </TableRow>
                {calendarProgressBar}
              </TableHead>
              <TableBody>{calendarContent}</TableBody>
            </Table>
          </Grid>
        </Paper>
      </Grid>
    );
  }

  /**
   * return the calendar days of the the current month
   */
  renderWeeks = () => {
    let weeks = [];
    let done = false;
    let date = this.props.month
      .clone()
      .startOf("month")
      .day("Sunday");
    let count = 0;
    let monthIndex = date.month();

    while (!done) {
      weeks.push(
        <Week
          key={date.week()}
          date={date.clone()}
          month={this.props.month}
          selectHoliday={day => this.props.selectHoliday(day)}
          selected={this.props.selected}
          settings={this.props.settings}
        />
      );
      date.add(1, "w");
      done = count++ > 2 && monthIndex !== date.month();
      monthIndex = date.month();
    }

    return weeks;
  };

  renderMonthLabel = () => {
    const { month, t } = this.props;
    return <span>{t('moment.monthLabel', { date: month })}</span>;
  };
}

export default withTranslation()(EventCalendar);
