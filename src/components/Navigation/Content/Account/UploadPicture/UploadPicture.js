import React from "react";
import { makeStyles } from '@material-ui/core/styles';
import {
  Card,
  Input,
  CardContent,
  Button,
  Switch,
  Paper,
  Collapse,
  FormControlLabel,
  LinearProgress
} from '@material-ui/core';
import CircularProgress from '@material-ui/core/CircularProgress';
import { useTranslation } from 'react-i18next';

const useStyles = makeStyles(theme => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
    },
  },
  container: {
    display: 'flex',
  },
  paper: {
    margin: theme.spacing(1),
  },
  polygon: {
    fill: theme.palette.common.white,
    stroke: theme.palette.divider,
    strokeWidth: 1,
  },
  circular: {
    marginTop: theme.spacing(2),
  }
}));

function UploadPicture(props) {

  const classes = useStyles();
  const [checked, setChecked] = React.useState(false);
  const { t } = useTranslation();

  const handleChange = () => {
    setChecked(prev => !prev);
  };

  const {
    onPictureChange,
    onPictureUpload,
    onPictureDelete,
    progress,
    image,
    uploading,
    url
  } = props;

  let progressToShow = null;
  if (uploading) {
    if (progress) {
      progressToShow = <CircularProgress size={30} thickness={3} className={classes.circular} variant="determinate" value={progress} />
    } else {
      progressToShow = <LinearProgress />
    }
  }
  let screen = (<Card className={classes.polygon}>
    <CardContent className={classes.root}>
      <div className={classes.root}>
        <Input fullWidth type="file" onChange={onPictureChange} />
        <Button color="primary" variant="text" onClick={onPictureUpload} disabled={image == null}>
          {t('account.uploadPicture.loadPicture')}
        </Button>
        <Button color="secondary" variant="text" onClick={onPictureDelete} disabled={url == null}>
          {t('account.uploadPicture.removePicture')}
        </Button>
        {progressToShow}
      </div>
    </CardContent>
  </Card>
  );

  return (
    <React.Fragment>
      <div>
        <FormControlLabel
          control={<Switch checked={checked} onChange={handleChange} />}
          label={t('account.uploadPicture.modifyPicture')}
        />
        <div className={classes.container}>
          <Collapse in={checked}>
            <Paper elevation={4} className={classes.paper}>
              {screen}
            </Paper>
          </Collapse>
        </div>
      </div>
    </React.Fragment>
  );
}

export default UploadPicture;
