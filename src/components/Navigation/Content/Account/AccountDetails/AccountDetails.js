import React from 'react';
import PropTypes from 'prop-types';
import {
  Card,
  CardHeader,
  CardContent,
  CardActions,
  Divider,
  Grid,
  Button,
  TextField
} from '@material-ui/core';
import LinearProgress from '../../../../UI/LinearProgress/LinearProgress';
import { useTranslation } from 'react-i18next';

const AccountDetails = props => {

  const { t } = useTranslation();

  const {
    firstName,
    lastName,
    initials,
    job,
    phone,
    description,
    loading,
    onFirstNameChange,
    onLastNameChange,
    onInitialsChange,
    onJobChange,
    onPhoneChange,
    onDescriptionChange,
    onSubmit
  } = props;

  let firstNameError = !firstName || firstName.length < 2;
  let lastNameError = !lastName || lastName.length < 2;
  let initialsError = !initials || initials.length !== 3;

  return (
    <Card>
      <CardHeader
        title={t('account.accountDetails.title')}
        subheader={t('account.accountDetails.header')}
      />
      <Divider />
      <CardContent>
        <Grid
          container
          spacing={3}
        >
          <Grid
            item
            md={6}
            xs={12}
          >
            <TextField
              fullWidth
              label={t('account.accountDetails.firstName')}
              margin="dense"
              name="firstName"
              onChange={onFirstNameChange}
              required
              value={firstName}
              variant="outlined"
              disabled={loading}
              helperText={firstNameError ? t('formErrorLabels.minLength2') : null}
              error={firstNameError ? true : false}
            />
          </Grid>
          <Grid
            item
            md={6}
            xs={12}
          >
            <TextField
              fullWidth
              label={t('account.accountDetails.lastName')}
              margin="dense"
              name="lastName"
              onChange={onLastNameChange}
              required
              value={lastName}
              variant="outlined"
              disabled={loading}
              helperText={lastNameError ? t('formErrorLabels.minLength2') : null}
              error={lastNameError ? true : false}
            />
          </Grid>
          <Grid
            item
            md={6}
            xs={12}
          >
            <TextField
              fullWidth
              label={t('account.accountDetails.initials')}
              margin="dense"
              name="initials"
              onChange={onInitialsChange}
              required
              value={initials}
              variant="outlined"
              disabled={loading}
              helperText={initialsError ? t('formErrorLabels.length3') : null}
              error={initialsError ? true : false}
            />
          </Grid>
          <Grid
            item
            md={6}
            xs={12}
          >
            <TextField
              fullWidth
              label={t('account.accountDetails.phoneNumber')}
              margin="dense"
              name="phone"
              onChange={onPhoneChange}
              type="number"
              value={phone}
              variant="outlined"
              disabled={loading}
            />
          </Grid>
          <Grid
            item
            md={6}
            xs={12}
          >
            <TextField
              fullWidth
              label={t('account.accountDetails.job')}
              margin="dense"
              name="job"
              onChange={onJobChange}
              required
              value={job}
              variant="outlined"
              disabled={loading}
            />
          </Grid>
          <Grid
            item
            md={6}
            xs={12}
          >
            <TextField
              fullWidth
              label={t('account.accountDetails.description')}
              margin="dense"
              name="Description"
              onChange={onDescriptionChange}
              required
              value={description}
              variant="outlined"
              multiline={true}
              rows={4}
              rowsMax={6}
              disabled={loading}
            />
          </Grid>
        </Grid>
      </CardContent>
      {loading ? <LinearProgress /> : null}
      <Divider />
      <CardActions>
        <Button
          color="primary"
          variant="contained"
          onClick={onSubmit}
          disabled={loading || firstNameError || lastNameError || initialsError}
        >
          {t('common.update')}
        </Button>
      </CardActions>
    </Card>
  );
};

AccountDetails.propTypes = {
  className: PropTypes.string
};

export default AccountDetails;