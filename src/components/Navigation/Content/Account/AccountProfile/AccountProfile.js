import React from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import {
  Card,
  CardContent,
  Avatar,
  Typography,
  Divider,
  LinearProgress
} from '@material-ui/core';
import accountLogo from "../../../../../assets/account.jpg";
import { useTranslation } from 'react-i18next';

const useStyles = makeStyles(theme => ({
  root: {},
  details: {
    display: 'flex'
  },
  avatar: {
    marginLeft: 'auto',
    height: 110,
    width: 100,
    flexShrink: 0,
    flexGrow: 0
  },
  progress: {
    marginTop: theme.spacing(2)
  },
  uploadButton: {
    marginRight: theme.spacing(2)
  }
}));

const AccountProfile = props => {
  
  const { t } = useTranslation();
  const classes = useStyles();

  const {
    className,
    firstName,
    lastName,
    job,
    withUsFrom,
    pfCompleteness,
    description,
    pictureURL,
    loading,
    ...rest
  } = props;


  return (
    <Card
      {...rest}
      className={clsx(classes.root, className)}
    >
      <CardContent>
        <div className={classes.details}>
          <div>
            <Typography
              gutterBottom
              variant="h5"
            >
              {firstName} {lastName}
            </Typography>
            <Typography
              className={classes.locationText}
              color="textSecondary"
              variant="body1"
            >
              {job}
            </Typography>
            <Typography
              className={classes.dateText}
              color="textSecondary"
              variant="body1"
            >
              {t('account.accountProfile.withUs')} {withUsFrom}
            </Typography>
          </div>
          <Avatar
            className={classes.avatar}
            src={pictureURL ? pictureURL : accountLogo}
          />
        </div>
        <div>
          <Typography variant="overline" display="block" gutterBottom> {t('common.description')}</Typography>
          {description}
        </div>
        <div className={classes.progress}>
          <Typography variant="body1"> {t('account.accountProfile.completeness')} {pfCompleteness}%</Typography>
          <LinearProgress
            value={pfCompleteness}
            variant="determinate"
          />
        </div>
      </CardContent>
      <Divider />
      {loading ? <LinearProgress /> : null}
    </Card>
  );
};

export default AccountProfile;