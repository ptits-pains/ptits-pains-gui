import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Card, CardContent, Grid, Typography, Avatar } from '@material-ui/core';
import ArrowRightIcon from '@material-ui/icons/ArrowRight';
import Fab from '@material-ui/core/Fab';
import DeleteIcon from '@material-ui/icons/Delete';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import NotInterestedIcon from '@material-ui/icons/NotInterested';
import ToggleOffIcon from '@material-ui/icons/ToggleOff';
import CheckIcon from '@material-ui/icons/Check';
import TelegramIcon from '@material-ui/icons/Telegram';
import Box from '@material-ui/core/Box';
import { red, green, grey, blue } from '@material-ui/core/colors';
import * as stateConstants from "../../../../constants/state";
import LinearProgress from '@material-ui/core/LinearProgress';
import { useTranslation } from 'react-i18next';

const useStyles = makeStyles(theme => ({
  root: {
    height: '100%'
  },
  content: {
    alignItems: 'center',
    display: 'flex'
  },
  title: {
    fontWeight: 700
  },
  refused: {
    backgroundColor: red[500],
    height: 56,
    width: 56
  },
  accepted: {
    backgroundColor: green[500],
    height: 56,
    width: 56
  },
  sent: {
    backgroundColor: blue[500],
    height: 56,
    width: 56
  },
  deleted: {
    backgroundColor: grey[500],
    height: 56,
    width: 56
  },
  icon: {
    height: 32,
    width: 32
  },
  difference: {
    marginTop: theme.spacing(2),
    display: 'flex',
    alignItems: 'center'
  },
  body: {
    marginTop: theme.spacing(1)
  },
  fab: {
    margin: theme.spacing(1),
  }
}));

function Request(props) {

  const classes = useStyles();
  const { t } = useTranslation();

  const {
    firstName,
    lastName,
    myDay,
    collegeDay,
    loading,
    state,
    onDeleteClick,
    requestTime
  } = props;

  function determineStatus(state) {

    switch (state) {
      case stateConstants.ACCEPTED:
        return t('notificationsState.accepted');
      case stateConstants.REFUSED:
        return t('notificationsState.refused');
      case stateConstants.DELETED:
        return t('notificationsState.deleted');
      case stateConstants.SENT:
        return t('notificationsState.sent');
      case stateConstants.DISABLED:
        return t('notificationsState.disabled');
      default:
        return ''
    }
  }

  let icon = null;
  if (state) {
    if (state === stateConstants.DELETED) {
      icon = (
        <Avatar className={classes.deleted}>
          <DeleteForeverIcon className={classes.icon} />
        </Avatar>
      )
    } else if (state === stateConstants.SENT) {
      icon = (
        <Avatar className={classes.sent}>
          <TelegramIcon className={classes.icon} />
        </Avatar>
      )
    } else if (state === stateConstants.ACCEPTED) {
      icon = (
        <Avatar className={classes.accepted}>
          <CheckIcon className={classes.icon} />
        </Avatar>
      )
    } else if (state === stateConstants.REFUSED) {
      icon = (
        <Avatar className={classes.refused}>
          <NotInterestedIcon className={classes.icon} />
        </Avatar>
      )
    } else if (state === stateConstants.DISABLED) {
      icon = (
        <Avatar className={classes.deleted}>
          <ToggleOffIcon className={classes.icon} />
        </Avatar>
      )
    }
  }

  return (
    <Card>
      {loading ? <LinearProgress /> : null}
      <CardContent>
        <Grid
          container
          justify="space-between"
        >
          <Grid item>
            <Typography
              className={classes.title}
              color="textSecondary"
              gutterBottom
              variant="body2"
            >
              {firstName + " " + lastName}
            </Typography>
            <Typography className={classes.body} variant="body2">{t('requests.myDay') + myDay}</Typography>
            <Typography className={classes.body} variant="body2">{t('requests.desiredDay') + collegeDay}</Typography>
            <Typography className={classes.body} variant="body2">{t('requests.requestTime') + requestTime}</Typography>
          </Grid>
          <Grid item>
            {icon}
          </Grid>
        </Grid>
        <Box display="flex" p={1}>
          <Box p={1} flexGrow={1}>
            <div className={classes.difference}>
              <ArrowRightIcon />
              <Typography
                variant="caption"
              >
                {t('requests.requestState') + determineStatus(state)}
              </Typography>
            </div>
          </Box>
          <Box p={1}>
            <Fab color="secondary" aria-label="delete" size='small' onClick={onDeleteClick} className={classes.fab} disabled={loading}>
              <DeleteIcon fontSize='small' />
            </Fab>
          </Box>
        </Box>
      </CardContent>
    </Card>
  );
};

export default Request;