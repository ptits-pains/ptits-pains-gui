import React from 'react';
import {
    Card,
    CardHeader,
    CardContent,
    Divider,
} from '@material-ui/core';
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormControl from "@material-ui/core/FormControl";
import { useTranslation } from 'react-i18next';

function EditLanguage(props) {

    const { t } = useTranslation();

    return (
        <Card>
            <CardHeader
                title={t('accountSettings.editLanguage.title')}
                subheader={t('accountSettings.editLanguage.header')}
            />
            <Divider />
            <CardContent>
                <FormControl
                    component="fieldset"
                >
                    <RadioGroup
                        aria-label="Gender"
                        name="gender1"
                        value={props.value}
                        onChange={props.onLanguageChange}
                    >
                        <FormControlLabel
                            value="en"
                            control={<Radio />}
                            label={t('accountSettings.editLanguage.englishLabel')}
                        />
                        <FormControlLabel
                            value="fr"
                            control={<Radio />}
                            label={t('accountSettings.editLanguage.frenchLabel')}
                        />
                    </RadioGroup>
                </FormControl>
            </CardContent>
        </Card>
    );
};

export default EditLanguage;