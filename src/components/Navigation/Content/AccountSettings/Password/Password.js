import React from 'react';
import {
  Card,
  CardHeader,
  CardContent,
  CardActions,
  Divider,
  Button,
  TextField
} from '@material-ui/core';
import LinearProgress from "../../../../UI/LinearProgress/LinearProgress";
import { useTranslation } from 'react-i18next';
import * as stringUtils from "../../../../../utils/StringUtils";

function Password(props) {

  const {
    password,
    confirmPassword,
    loading,
    onPasswordChange,
    onConfirmPasswordChange,
    onSubmit,
    passwordError,
    confirmPasswordError
  } = props;

  const { t } = useTranslation();
  let disableCondition = loading || !stringUtils.isEmpty(passwordError) || !stringUtils.isEmpty(confirmPasswordError);

  return (
    <Card>
      <CardHeader
        title={t('accountSettings.password.title')}
        subheader={t('accountSettings.password.header')}
      />
      <Divider />
      {loading ? <LinearProgress /> : null}
      <CardContent>
        <TextField
          fullWidth
          label={t('accountSettings.password.newPassword')}
          onChange={onPasswordChange}
          style={{ marginTop: '1rem' }}
          type="password"
          value={password}
          variant="outlined"
          error={passwordError !== null}
          helperText={passwordError}
        />
        <TextField
          fullWidth
          label={t('accountSettings.password.confirmPassword')}
          onChange={onConfirmPasswordChange}
          style={{ marginTop: '1rem' }}
          type="password"
          value={confirmPassword}
          variant="outlined"
          error={confirmPasswordError !== null}
          helperText={confirmPasswordError}
        />
      </CardContent>
      <Divider />
      <CardActions>
        <Button
          color="primary"
          variant="contained"
          onClick={onSubmit}
          disabled={disableCondition}
        >
          {t('common.update')}
        </Button>
      </CardActions>
    </Card>
  );
};

export default Password;