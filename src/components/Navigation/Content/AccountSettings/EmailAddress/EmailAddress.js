import React from 'react';
import {
  Card,
  CardHeader,
  CardContent,
  CardActions,
  Divider,
  Button,
  TextField
} from '@material-ui/core';
import LinearProgress from "../../../../UI/LinearProgress/LinearProgress";
import { useTranslation } from 'react-i18next';
import * as stringUtils from "../../../../../utils/StringUtils";

function EmailAddress(props) {

  const {
    onEmailChange,
    loading,
    email,
    onSubmit,
    isNotValidEmail
  } = props;

  const { t } = useTranslation();

  return (
    <Card>
      <CardHeader
        title={t('accountSettings.email.title')}
        subheader={t('accountSettings.email.header')}
      />
      <Divider />
      {loading ? <LinearProgress /> : null}
      <CardContent>
        <TextField
          fullWidth
          type="text"
          variant="outlined"
          value={email}
          onChange={onEmailChange}
          error={isNotValidEmail !== null}
          helperText={isNotValidEmail}
        />
      </CardContent>
      <Divider />
      <CardActions>
        <Button
          color="primary"
          variant="contained"
          disabled={loading || !stringUtils.isEmpty(isNotValidEmail)}
          onClick={onSubmit}
        >
          {t('common.update')}
        </Button>
      </CardActions>
    </Card>
  );
};

export default EmailAddress;