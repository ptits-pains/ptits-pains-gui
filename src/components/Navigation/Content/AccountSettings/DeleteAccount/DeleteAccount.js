import React from 'react';
import {
    Card,
    CardHeader,
    CardContent,
    CardActions,
    Divider,
    Button,
    Typography
} from '@material-ui/core';
import Chip from '@material-ui/core/Chip';
import LinearProgress from "../../../../UI/LinearProgress/LinearProgress";
import { useTranslation } from 'react-i18next';

function DeleteAccount(props) {

    const {
        loading,
        onDeleteAccount,
        onCancelAccount,
        toBeDeleted,
        creationTime
    } = props;
    const { t } = useTranslation();

    return (
        <Card>
            <CardHeader
                title={t('accountSettings.deleteAccount.title')}
                subheader={!toBeDeleted ? t('accountSettings.deleteAccount.header') : null}
            />
            <Divider />
            {toBeDeleted ?
                <Typography color="secondary" gutterBottom variant="h6" >
                    {t('accountSettings.deleteAccount.warning')}
                </Typography>
                :
                null
            }
            {loading ? <LinearProgress /> : null}
            <CardContent>
                <Chip label={t('accountSettings.deleteAccount.chipLabel')} variant="outlined" size="small" color="primary" /> {t('accountSettings.deleteAccount.added')} {creationTime}
            </CardContent>
            <Divider />
            <CardActions>
                <Button
                    color="secondary"
                    variant="contained"
                    onClick={onDeleteAccount}
                    disabled={loading || toBeDeleted}
                >
                    {t('common.deleteDefinitely')}
                </Button>
                <Button
                    color="primary"
                    variant="contained"
                    onClick={onCancelAccount}
                    disabled={loading || !toBeDeleted}
                >
                    {t('common.cancel')}
                </Button>
            </CardActions>
        </Card>
    );
};

export default DeleteAccount;