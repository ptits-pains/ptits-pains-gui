import React from "react";
import Grid from '@material-ui/core/Grid';
import frLocale from "date-fns/locale/fr";
import * as i18nUtils from "../../../../../utils/I18nUtils";
import {
    MuiPickersUtilsProvider,
    KeyboardDatePicker
} from '@material-ui/pickers';
import DateFnsUtils from "@date-io/date-fns";
import Typography from "@material-ui/core/Typography";
import SendIcon from "@material-ui/icons/Send";
import Fab from "@material-ui/core/Fab";
import { makeStyles } from '@material-ui/core/styles';
import { useTranslation } from 'react-i18next';

const useStyles = makeStyles(theme => ({
    root: {

    },
    fab: {
        margin: theme.spacing(1),
    }
}));

const NextDate = props => {

    const { t } = useTranslation();
    const classes = useStyles();

    const {
        loading,
        nextDate,
        onNextDateChange,
        onSubmitClick,
        disabled
    } = props;

    return (
        <React.Fragment>
            <Grid item xs={12}>
                <Typography
                    color="primary"
                    variant="subtitle1"
                    gutterBottom
                >
                    {t('scheduleRollsDays.nextDate.label')}
                </Typography>
            </Grid>
            <Grid item xs={12}>
                <MuiPickersUtilsProvider utils={DateFnsUtils} locale={i18nUtils.isFrenchLanguage() ? frLocale : null}>
                    <KeyboardDatePicker
                        value={nextDate}
                        onChange={onNextDateChange}
                        disabled={loading}
                        format={i18nUtils.isFrenchLanguage() ? "dd MMMM yyyy" : "MMMM dd, yyyy"}
                    />
                </MuiPickersUtilsProvider>
                <Fab
                    color="primary"
                    aria-label="send"
                    size="small"
                    variant="extended"
                    className={classes.fab}
                    disabled={disabled}
                >
                    <SendIcon onClick={onSubmitClick} fontSize="small" />
                </Fab>
            </Grid>
        </React.Fragment>
    )
}

export default NextDate;