import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
    Card,
    CardContent,
    Divider,
    LinearProgress,
    Typography,
    Chip
} from '@material-ui/core';
import AdminLogo from "../AdminLogo/AdminLogo";
import NextDate from "../NextDate/NextDate";
import Dialog from "../../../../UI/Dialog/Dialog";
import { useTranslation } from 'react-i18next';

const useStyles = makeStyles(theme => ({
    root: {},
    details: {
        display: 'flex'
    }
}));

const UserCard = props => {

    const { t } = useTranslation();
    const classes = useStyles();
    const {
        firstName,
        lastName,
        isAdmin,
        isCurrentUser,
        nextDate,
        loading,
        showDialog,
        disableSubmit,
        onNextDateChange,
        onAcceptOverwriteCollegeDay,
        onSubmitClick,
        conflictedUserName
    } = props;

    let header = null;
    if (isCurrentUser) {

        header = <Chip label={t('scheduleRollsDays.you')} variant="outlined" size="small" color="primary" />;
    }

    return (
        <React.Fragment>
            <Card>
                <CardContent>
                    <div className={classes.details}>
                        <div>
                            <Typography
                                gutterBottom
                                variant="h6"
                            >
                                {firstName} {lastName} {header}
                            </Typography>
                            <NextDate
                                nextDate={nextDate}
                                loading={loading}
                                onNextDateChange={onNextDateChange}
                                onSubmitClick={onSubmitClick}
                                disabled={disableSubmit}
                            />
                        </div>
                        <AdminLogo
                            isAdmin={isAdmin}
                        />
                    </div>
                </CardContent>
                <Divider />
                {loading ? <LinearProgress /> : null}
            </Card>
            {!showDialog ? null :
                <Dialog
                    title={t('scheduleRollsDays.dialog.title')}
                    content={t('scheduleRollsDays.dialog.content', { userName: conflictedUserName })}
                    onAcceptClick={onAcceptOverwriteCollegeDay}
                />
            }
        </React.Fragment>
    );
};

export default UserCard;