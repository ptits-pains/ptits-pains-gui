import React from "react";
import VerifiedUserIcon from '@material-ui/icons/VerifiedUser';
import { makeStyles } from '@material-ui/core/styles';
import { green } from '@material-ui/core/colors';
import Box from '@material-ui/core/Box';
import SubTitle from "../../../../UI/SubTitle/SubTitle";
import { useTranslation } from 'react-i18next';

const useStyles = makeStyles(theme => ({
    root: {},
    admin: {
        color: green[500],
        height: 60,
        width: 60,
    },
    user: {
        height: 60,
        width: 60,
    },
    box: {
        marginLeft: 'auto',
        flexShrink: 0,
        flexGrow: 0
    }
}));

const AdminLogo = props => {

    const classes = useStyles();
    const { isAdmin } = props;
    const { t } = useTranslation();

    let iconToShow = (
        <Box component="span" m={1} className={classes.box}>
            <VerifiedUserIcon className={classes.user} />
            <SubTitle>{t('scheduleRollsDays.adminLogo.userLabel')}</SubTitle>
        </Box>
    )
    if (isAdmin) {
        iconToShow = (
            <Box component="span" m={1} className={classes.box}>
                <VerifiedUserIcon className={classes.admin} />
                <SubTitle>{t('scheduleRollsDays.adminLogo.adminLabel')}</SubTitle>
            </Box>
        )
    }

    return iconToShow
}

export default AdminLogo;