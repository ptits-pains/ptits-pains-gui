import React from 'react';
import DeleteIcon from '@material-ui/icons/Delete';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import IconButton from '@material-ui/core/IconButton';
import UndoIcon from '@material-ui/icons/Undo';
import SmallCircularProgress from "../../../../UI/SmallCircularProgress/SmallCircularProgress";
import { useTranslation } from 'react-i18next';

const DeleteAccount = (props) => {

    const [open, setOpen] = React.useState(false);
    const { t } = useTranslation();

    const handleClickOpen = () => {
        setOpen(true);
    };

    const {
        deleteAccount,
        loading,
        name,
        onDeleteAccount,
        onCancelDeleteAccount
    } = props;

    const handleClose = (event, choice) => {
        setOpen(false);
        if (choice === 'ok') {
            onDeleteAccount();
        }
    };

    let buttonToShow = null;
    if (deleteAccount) {

        buttonToShow = (
            <IconButton onClick={onCancelDeleteAccount} disabled={loading}>
                {loading ? <SmallCircularProgress /> : <UndoIcon color="primary" />}
            </IconButton>)
    } else {

        buttonToShow = (
            <IconButton onClick={handleClickOpen} disabled={loading || deleteAccount}>
                {loading ? <SmallCircularProgress /> : <DeleteIcon color="secondary" />}
            </IconButton>)
    }

    return (
        <div>
            {buttonToShow}
            <Dialog
                open={open}
                onClose={handleClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">{t('manageUsers.deleteAccount.title')}</DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                       {t('manageUsers.deleteAccount.content') + name} ?
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={(event) => handleClose(event, 'ko')} color="primary">
                    {t('common.no')}
                    </Button>
                    <Button onClick={(event) => handleClose(event, 'ok')} color="primary" autoFocus>
                    {t('common.yes')}
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    )
}

export default DeleteAccount;