import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Grid } from '@material-ui/core';
import Technology from "./Technology/Technology";
import react from "../../../../assets/technologies/react.png";
import material from "../../../../assets/technologies/material.png";
import firebase from "../../../../assets/technologies/firebase.png";
import jsx from "../../../../assets/technologies/jsx.png";
import gitlab from "../../../../assets/technologies/gitlab.jpg";
import git from "../../../../assets/technologies/git.png";
import redux from "../../../../assets/technologies/redux.png";
import moment from "../../../../assets/technologies/moment.png";
import docker from "../../../../assets/technologies/docker.jpg";

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3)
  },
  content: {
    marginTop: theme.spacing(2)
  },
  pagination: {
    marginTop: theme.spacing(3),
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end'
  }
}));

const Technologies = () => {
  const classes = useStyles();

  const [technoList] = useState(technoData);

  return (
    <div className={classes.root}>
      <div className={classes.content}>
        <Grid
          container
          spacing={3}
        >
          {technoList.map(techno => (
            <Grid
              item
              key={techno.id}
              lg={4}
              md={6}
              xs={12}
            >
              <Technology techno={techno} />
            </Grid>
          ))}
        </Grid>
      </div>
    </div>
  );
};

export default Technologies;

const technoData = [
  {
    id: 0,
    title: 'React',
    description:
      'React is a library for building composable user interfaces. It encourages the creation of reusable UI components, which present data that changes over time.',
    imageUrl: react
  },
  {
    id: 1,
    title: 'Firebase',
    description:
      'Firebase is a mobile and web development platform. It offers many services, in this application we used services such as real time database, authentication, functions, storage and hosting.',
    imageUrl: firebase
  },
  {
    id: 2,
    title: 'JSX',
    description:
      'JSX allows us to write HTML elements in JavaScript and place them in the DOM without any createElement()  and/or appendChild() methods. JSX converts HTML tags into react elements. We are not required to use JSX, but JSX makes it easier to write React applications.',
    imageUrl: jsx
  },
  {
    id: 3,
    title: 'Material UI',
    description:
      'We used this library to build the material design of our applications. This library offers a lot of react components to make our development faster and easier.',
    imageUrl: material
  },
  {
    id: 4,
    title: 'Git',
    description:
      'Git is a free and open source distributed version control system designed to handle everything from small to very large projects with speed and efficiency.',
    imageUrl: git
  },
  {
    id: 5,
    title: 'GitLab',
    description:
      'GitLab is a web-based DevOps lifecycle tool that provides a Git-repository manager providing wiki, issue-tracking and CI/CD pipeline[7] features, using an open-source license, developed by GitLab Inc.',
    imageUrl: gitlab
  },
  {
    id: 6,
    title: 'Redux',
    description:
      "Redux helps to centralize your application's state and logic enables powerful capabilities like undo/redo, state persistence, and much more.",
    imageUrl: redux
  },
  {
    id: 7,
    title: 'Moment',
    description:
      'Moment is a javascript library that will help us to parse, validate, manipulate, and display dates and times.',
    imageUrl: moment,
  },
  {
    id: 8,
    title: 'Docker',
    description:
      'Docker is a set of platform as a service (PaaS) products that use OS-level virtualization to deliver software in packages called containers.',
    imageUrl: docker
  }
];