import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
  Card,
  CardContent,
  Typography,
} from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  root: {},
  imageContainer: {
    height: 64,
    width: 64,
    margin: '0 auto',
    border: `1px solid ${theme.palette.divider}`,
    borderRadius: '5px',
    overflow: 'hidden',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  image: {
    width: '100%'
  },
  statsItem: {
    display: 'flex',
    alignItems: 'center'
  },
  statsIcon: {
    color: theme.palette.icon,
    marginRight: theme.spacing(1)
  }
}));

const Technology = props => {

  const { techno } = props;
  const classes = useStyles();

  return (
    <Card>
      <CardContent>
        <div className={classes.imageContainer}>
          <img
            alt="Technology"
            className={classes.image}
            src={techno.imageUrl}
          />
        </div>
        <Typography
          align="center"
          gutterBottom
          variant="h4"
        >
          {techno.title}
        </Typography>
        <Typography
          align="center"
          variant="body1"
        >
          {techno.description}
        </Typography>
      </CardContent>
    </Card>
  );
};

export default Technology;