import React from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import CardHeader from '@material-ui/core/CardHeader';
import {
  Card,
  CardContent,
  CardActions,
  Typography,
  Grid,
  Divider
} from '@material-ui/core';
import IconButton from '@material-ui/core/IconButton';
import HighlightOffIcon from '@material-ui/icons/HighlightOff';
import CheckCircleOutlineIcon from '@material-ui/icons/CheckCircleOutline';
import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline';
import account from "../../../../assets/account.jpg";
import SmallCircularProgress from "../../../UI/SmallCircularProgress/SmallCircularProgress";
import { useTranslation } from 'react-i18next';
import * as stateConstants from '../../../../constants/state';


const useStyles = makeStyles(theme => ({
  root: {},
  imageContainer: {
    height: 84,
    width: 84,
    margin: '0 auto',
    border: `1px solid ${theme.palette.divider}`,
    borderRadius: '5px',
    overflow: 'hidden',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  image: {
    width: '100%'
  },
  statsItem: {
    display: 'flex',
    alignItems: 'center'
  },
  statsIcon: {
    color: theme.palette.icon,
    marginRight: theme.spacing(1)
  }
}));

const Notification = props => {

  const classes = useStyles();

  const {
    className,
    product,
    firstName,
    lastName,
    photoUrl,
    myDay,
    collegeDay,
    loading,
    onAcceptClick,
    onRefuseClick,
    state,
    onDeleteClick,
    ...rest
  } = props;

  const { t } = useTranslation();

  function determineStatus(state) {

    switch (state) {
      case stateConstants.IN_PROGRESS:
        return t('notificationsState.progress');
      case stateConstants.ACCEPTED:
        return t('notificationsState.accepted');
      case stateConstants.REFUSED:
        return t('notificationsState.refused');
      case stateConstants.DISABLED:
        return t('notificationsState.disabled');
      default:
        return ''
    }
  }

  let disabled = loading || [stateConstants.ACCEPTED, stateConstants.REFUSED, stateConstants.DISABLED].includes(state);

  return (<div>
    <Card
      {...rest}
      className={clsx(classes.root, className)}
    >
      <CardHeader
        title={firstName + " " + lastName}
        subheader={t('notifications.header') + myDay}
      />
      <CardContent>
        <div className={classes.imageContainer}>
          <img
            alt="avatar"
            className={classes.image}
            src={photoUrl ? photoUrl : account}
          />
        </div>
        <Typography
          align="center"
          variant="body1"
        >
          {t('notifications.message') + collegeDay}
        </Typography>
      </CardContent>
      <Divider />
      <CardActions>
        <Grid
          container
          justify="space-between"
        >
          <Grid
            className={classes.statsItem}
            item
          >
            {loading ? <SmallCircularProgress /> :
              <Typography
                display="inline"
                variant="body2"
              >
                {t('notifications.state') + determineStatus(state)}
              </Typography>
            }
          </Grid>
          <Grid
            className={classes.statsItem}
            item
          >
            <IconButton onClick={onAcceptClick} disabled={disabled}>
              <CheckCircleOutlineIcon />
            </IconButton>
            <IconButton onClick={onRefuseClick} disabled={disabled}>
              <HighlightOffIcon />
            </IconButton>
            <IconButton onClick={onDeleteClick} disabled={loading}>
              <DeleteOutlineIcon />
            </IconButton>
          </Grid>
        </Grid>
      </CardActions>
    </Card>
  </div>
  );
};

export default Notification;