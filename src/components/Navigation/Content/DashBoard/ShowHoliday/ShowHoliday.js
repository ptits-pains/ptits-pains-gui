import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import SubTitle from "../../../../UI/SubTitle/SubTitle";
import FlashBadge from "../../../../UI/FlashBadge/FlashBadge";
import { useTranslation } from 'react-i18next';

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1
    },
    paper: {
        padding: theme.spacing(2),
        marginTop: theme.spacing(3)
    }
}));

function ShowHoliday(props) {
    const classes = useStyles();
    const { t } = useTranslation();
    return (
        <Grid item className={classes.root} xs={12} md={6}>
            <Paper className={classes.paper}>
                <Grid container spacing={2}>
                    <Grid item xs={2}>
                        <FlashBadge />
                    </Grid>
                    <Grid item xs={5}>
                        <SubTitle>
                            {t('moment.fullDate', { date:  props.holiday.date })}
                        </SubTitle>
                    </Grid>
                    <Grid item xs={5}>
                        <SubTitle>{props.holiday.name}</SubTitle>
                    </Grid>
                </Grid>
            </Paper>
        </Grid>
    );
}

export default ShowHoliday;
