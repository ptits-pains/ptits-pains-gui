import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import CardMedia from '@material-ui/core/CardMedia';
import croissant from '../../../../../assets/croissant.jpg';
import ThumbUpAltIcon from '@material-ui/icons/ThumbUpAlt';
import ThumbDownAltIcon from '@material-ui/icons/ThumbDownAlt';
import clsx from 'clsx';
import CardActions from '@material-ui/core/CardActions';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import { red } from '@material-ui/core/colors';
import { green } from '@material-ui/core/colors';
import { grey } from '@material-ui/core/colors';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import SubTitle from "../../../../UI/SubTitle/SubTitle";
import LinearProgress from "../../../../UI/LinearProgress/LinearProgress";
import { useTranslation } from 'react-i18next';

const useStyles = makeStyles(theme => ({
    card: {
        display: 'flex',
        marginTop: theme.spacing(3)
    },
    details: {
        display: 'flex',
        flexDirection: 'column',
    },
    content: {
        flex: '1 0 auto',
    },
    cover: {
        width: 151,
    },
    expand: {
        transform: 'rotate(0deg)',
        marginLeft: 'auto',
        transition: theme.transitions.create('transform', {
            duration: theme.transitions.duration.shortest,
        }),
    },
    expandOpen: {
        transform: 'rotate(180deg)',
    },
    top: {
        color: green[500],
    },
    flop: {
        color: red[500],
    },
    disabled: {
        color: grey[500],
    }
}));

export default function RollsCardToShow(props) {
    const classes = useStyles();
    const [expanded, setExpanded] = React.useState(false);
    const handleExpandClick = () => {
        setExpanded(!expanded);
    };
    const { t } = useTranslation();

    const {
        participant,
        loading,
        userName,
        rollsDay,
        usersNames,
        onAddMeToTheList,
        onRemoveMeFromTheList
    } = props;
    let usersList = null;
    if (usersNames) {
        usersList = usersNames.map(userName => {
            let key = userName[Object.keys(userName)[0]];
            return <SubTitle key={key}>{key}</SubTitle>
        })
    }
    return (
        <Card className={classes.card}>
            <div className={classes.details}>
                {loading ? <LinearProgress /> : null}
                <CardContent className={classes.content}>
                    <Typography variant="subtitle1">
                        {userName}
                    </Typography>
                    <SubTitle>
                        {t('moment.fullDate', { date: rollsDay })}
                    </SubTitle>
                    {props.children}
                </CardContent>
                <CardActions disableSpacing>
                    <IconButton disabled={loading || participant} onClick={onAddMeToTheList}>
                        <ThumbUpAltIcon className={loading ? classes.grey : participant ? classes.top : classes.grey} />
                    </IconButton>
                    <IconButton disabled={loading || !participant} onClick={onRemoveMeFromTheList}>
                        <ThumbDownAltIcon className={loading ? classes.grey : !participant ? classes.flop : classes.grey} />
                    </IconButton>
                    <IconButton
                        className={clsx(classes.expand, {
                            [classes.expandOpen]: expanded,
                        })}
                        onClick={handleExpandClick}
                        aria-expanded={expanded}
                        aria-label="show more"
                    >
                        <ExpandMoreIcon />
                    </IconButton>
                </CardActions>
                <Collapse in={expanded} timeout="auto" unmountOnExit>
                    <CardContent>
                        {usersList}
                    </CardContent>
                </Collapse>
            </div>
            <CardMedia
                className={classes.cover}
                image={croissant}
                title="croissant logo"
            />
        </Card>
    );
}