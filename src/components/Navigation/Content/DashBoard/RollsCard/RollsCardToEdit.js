import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import EditIcon from '@material-ui/icons/Edit';
import HorizontalBox from '../../../../UI/HorizontalBox/HorizontalBox';
import SaveIcon from '@material-ui/icons/Save';
import { SAVED_CARD, ERROR_SAVING_CARD, EDITING_CARD, SAVING_CARD, CREATE_CARD } from '../../../../../containers/DashBoard/RollsList/RollsCard/RollsCardToEdit';
import CircularProgress from '@material-ui/core/CircularProgress';
import ReplayIcon from '@material-ui/icons/Replay';
import SubTitle from "../../../../UI/SubTitle/SubTitle";
import { useTranslation } from 'react-i18next';

const useStyles = makeStyles(theme => ({
    card: {
        display: 'flex',
        marginTop: theme.spacing(3)
    },
    details: {
        display: 'flex',
        flexDirection: 'column',
    },
    content: {
        flex: '1 0 auto',
    },
    cover: {
        width: 151,
    }
}));

export default function RollsCardToEdit(props) {

    const classes = useStyles();
    const { t } = useTranslation();

    const {
        cardState,
        rollsDay
    } = props;

    let card = null;
    if (cardState === CREATE_CARD) {
        card = (
            <IconButton onClick={() => props.editCard()}>
                <EditIcon />
            </IconButton>
        )
    } else if (cardState === EDITING_CARD) {
        card = (
            <IconButton onClick={() => props.saveAllProducts()}>
                <SaveIcon />
            </IconButton>
        )
    } else if (cardState === SAVING_CARD) {

        card = <CircularProgress />
    } else if (cardState === SAVED_CARD) {

        card = (
            <IconButton onClick={() => props.editCard()}>
                <EditIcon />
            </IconButton>
        )
    } else if (cardState === ERROR_SAVING_CARD) {

        card = (
            <IconButton onClick={() => props.saveAllProducts()}>
                <ReplayIcon />
            </IconButton>
        )
    } 
    
    return (
        <Card className={classes.card}>
            <div className={classes.details}>
                <CardContent className={classes.content}>
                    <Typography variant="subtitle1">
                        {props.userName}
                    </Typography>
                    <SubTitle>
                        {t('moment.fullDate', { date: rollsDay })}
                    </SubTitle>
                    {props.children}
                </CardContent>
                <HorizontalBox justifyContent="flex-end">
                    {card}
                </HorizontalBox>
            </div>
        </Card>
    );
}