import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import TextField from '@material-ui/core/TextField';
import Box from '@material-ui/core/Box';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import DeleteIcon from '@material-ui/icons/Delete';
import DoneIcon from '@material-ui/icons/Done';
import ErrorIcon from '@material-ui/icons/Error';
import HourglassEmptyIcon from '@material-ui/icons/HourglassEmpty';
import { SAVED_CARD, ERROR_SAVING_CARD, SAVING_CARD, CREATE_CARD, EDITING_CARD } from '../../../../../containers/DashBoard/RollsList/RollsCard/RollsCardToEdit';

const useStyles = makeStyles(theme => ({
    textField: {
        marginLeft: theme.spacing(0),
        paddingLeft: theme.spacing(1),
        width: 100,
    },
    numberField: {
        margin: theme.spacing(0),
        width: 35,
    },
    done: {
        color: '#00b300',
    },
    error: {
        color: '#c62828',
    }
}));

export default function RollsCard(props) {
    const classes = useStyles();

    function handleAddButtonClick() {
        props.addNewProduct();
        setShowAddButton(false);
    }

    const {
        cardState,
        lastProduct
    } = props;
    const [showAddButton, setShowAddButton] = React.useState(lastProduct);

    let productIcon = null;
    if (showAddButton) {
        productIcon = (
            <IconButton onClick={handleAddButtonClick}>
                <AddCircleIcon />
            </IconButton>
        )
    } else {
        productIcon = (
            <IconButton onClick={props.deleteProduct} className={classes.error}>
                <DeleteIcon />
            </IconButton>
        )
    }

    if (cardState === SAVED_CARD) {

        productIcon = <IconButton className={classes.done}><DoneIcon /></IconButton>
    } else if (cardState === ERROR_SAVING_CARD) {

        productIcon = <IconButton className={classes.error}><ErrorIcon /></IconButton>
    } else if (cardState === SAVING_CARD) {

        productIcon = <IconButton><HourglassEmptyIcon /></IconButton>
    } else if (cardState === CREATE_CARD) {

        productIcon = null;
    }
    

    return (
        <Box display="flex" p={0} bgcolor="background.paper">
            <TextField
                className={classes.numberField}
                onChange={props.onQuantityChange}
                type="number"
                defaultValue={props.quantity ? props.quantity : 0}
                inputProps={{
                    min: "0"
                }}
                disabled={!(cardState===EDITING_CARD)}
            />
            <TextField
                className={classes.textField}
                onChange={props.onNameChange}
                defaultValue={props.name}
                disabled={!(cardState===EDITING_CARD)}
            />
            {productIcon}
        </Box>
    );
}