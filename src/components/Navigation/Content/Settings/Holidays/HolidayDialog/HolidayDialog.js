import React, { useState } from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';
import DateFnsUtils from "@date-io/date-fns";
import {
    MuiPickersUtilsProvider,
    KeyboardDatePicker,
} from '@material-ui/pickers';
import frLocale from "date-fns/locale/fr";
import Grid from '@material-ui/core/Grid';
import { useTranslation } from 'react-i18next';
import * as i18nUtils from "../../../../../../utils/I18nUtils";

/**
 * we gonna use react hooks in this example to keep it simple
 * @param {Object} props 
 */
export default function HolidayDialog(props) {

    const { t } = useTranslation();
    const [selectedDate, setSelectedDate] = useState(new Date());
    const [holidayName, setHolidayName] = useState("");
    const handleHolidayNameChange = event => {
        setHolidayName(event.target.value);
    }
    const handleSelectedDateChange = date => {
        setSelectedDate(date);
    }

    /**
     * save a new holiday
     * @param {Event} event 
     */
    const save = event => {
        props.saveNewHoliday(event, holidayName, selectedDate)
        props.handleClose();
        setSelectedDate(new Date());
        setHolidayName("");
    }

    return (
        <div>
            <Dialog
                open={props.open}
                onClose={props.handleClose}
            >
                <DialogTitle style={{ cursor: 'move' }}>
                    {t('settings.holidays.dialog.title')}
                </DialogTitle>
                <DialogContent>
                    <Grid container spacing={2}>
                        <Grid item xs={12}>
                            <TextField
                                autoFocus
                                margin="normal"
                                label={t('settings.holidays.dialog.holidayName')}
                                type="text"
                                fullWidth
                                value={holidayName}
                                onChange={handleHolidayNameChange}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <MuiPickersUtilsProvider utils={DateFnsUtils} locale={i18nUtils.isFrenchLanguage() ? frLocale : null}>
                                <KeyboardDatePicker
                                    margin="normal"
                                    id="date-picker-dialog"
                                    label={t('settings.holidays.dialog.holidayDate')}
                                    format={i18nUtils.isFrenchLanguage() ? "dd MMMM yyyy" : "MMMM dd, yyyy"}
                                    value={selectedDate}
                                    onChange={handleSelectedDateChange}
                                    fullWidth
                                    KeyboardButtonProps={{
                                        'aria-label': 'change date',
                                    }}
                                />
                            </MuiPickersUtilsProvider>
                        </Grid>
                    </Grid>
                </DialogContent>
                <DialogActions>
                    <Button onClick={props.handleClose} color="primary">
                        {t('common.cancel')}
                    </Button>
                    <Button onClick={save} color="primary">
                        {t('common.save')}
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}