import React from 'react';
import Holiday from './Holiday/Holiday';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import AddIcon from '@material-ui/icons/Add';
import Fab from '@material-ui/core/Fab';
import HolidayDialog from './HolidayDialog/HolidayDialog';
import HorizontalBox from '../../../../UI/HorizontalBox/HorizontalBox';
import LinearProgress from '../../../../UI/LinearProgress/LinearProgress';
import ModifyYear from "./ModifyYear/ModifyYear";
import TextField from '@material-ui/core/TextField';
import { useTranslation } from 'react-i18next';

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing(2),
        marginTop: theme.spacing(3),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    }
}));

function Holidays(props) {
    const { t } = useTranslation();
    const classes = useStyles();
    const [open, setOpen] = React.useState(false);

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const {
        loading,
        currentYear,
        holidays,
        onCurrentYearChange,
        onSaveNewHoliday,
        onDeleteHoliday,
        onUpdateHoliday,
        onSubmitCurrentYear
    } = props;

    return (
        <div className={classes.root}>
            <Paper className={classes.paper}>
                <Grid container spacing={3}>
                    <Grid item
                        lg={4}
                        md={4}
                        xl={12}
                        xs={12}
                    >
                        <HorizontalBox justifyContent="flex-start">
                            <Fab size="small" color="primary" aria-label="add" onClick={handleClickOpen} className={classes.margin}>
                                <AddIcon />
                            </Fab>
                        </HorizontalBox>
                    </Grid>
                    <Grid item
                        lg={4}
                        md={4}
                        xl={12}
                        xs={12}
                    >
                        <TextField
                            value={currentYear}
                            disabled={true}
                            label={t('settings.holidays.yearLabel')}
                        />
                    </Grid>
                    <Grid item
                        lg={4}
                        md={4}
                        xl={12}
                        xs={12}
                    >
                        <ModifyYear
                            currentYear={new Date(currentYear)}
                            onCurrentYearChange={onCurrentYearChange}
                            onSubmitCurrentYear={onSubmitCurrentYear}
                            loading={loading}
                        />
                    </Grid>
                    {loading ? <Grid item xs={12}><LinearProgress /></Grid> :
                        holidays.map(element => {
                            return (
                                <Grid item xs={12} md={3} xl={2} key={element.key}>
                                    <Holiday
                                        holiday={element}
                                        onDeleteHoliday={onDeleteHoliday}
                                        onUpdateHoliday={onUpdateHoliday}
                                    />
                                </Grid>
                            )
                        })
                    }
                </Grid>
            </Paper>
            <HolidayDialog
                handleClickOpen={handleClickOpen}
                handleClose={handleClose}
                open={open}
                saveNewHoliday={onSaveNewHoliday}
            />
        </div>
    )
}

export default Holidays;