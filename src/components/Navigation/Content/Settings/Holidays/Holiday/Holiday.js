import React, { useState } from 'react';
import DateFnsUtils from "@date-io/date-fns";
import {
    MuiPickersUtilsProvider,
    KeyboardDatePicker,
} from '@material-ui/pickers';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import HorizontalBox from '../../../../../UI/HorizontalBox/HorizontalBox';
import Grid from '@material-ui/core/Grid';
import IconButton from '@material-ui/core/IconButton';
import * as dateUtils from '../../../../../../utils/DateUtils';
import SaveIcon from '@material-ui/icons/Save';
import frLocale from "date-fns/locale/fr";
import * as i18nUtils from "../../../../../../utils/I18nUtils";

const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        padding: theme.spacing(2)
    }
}));

/**
 * we gonna use hooks here to keep it simple
 */
function Holiday(props) {

    const classes = useStyles();
    let date = null;
    let name = null;
    if (props.holiday) {
        date = props.holiday.date;
        name = props.holiday.name;
    }

    const [holidayDate, setHolidayDate] = useState(dateUtils.stringToJsDate(date));
    const [holidayName, setHolidayName] = useState(name);
    const [disableModification, setDisableModification] = useState(true);

    const handleHolidayNameChange = event => {
        setHolidayName(event.target.value);
    }
    const handleHolidayDateChange = date => {
        setHolidayDate(date);
    }
    const toggleDisableModification = () => {
        let currentState = disableModification;
        setDisableModification(!currentState);
    }

    /**
     * update the current holiday
     */
    const updateHoliday = () => {
        toggleDisableModification();
        props.onUpdateHoliday(holidayName, holidayDate, props.holiday.key)
    }

    return (
        <Paper className={classes.root}>
            <Grid container spacing={2}>
                <Grid item xs={12}>
                    <TextField
                        value={holidayName}
                        onChange={handleHolidayNameChange}
                        disabled={disableModification}
                        fullWidth
                    />
                </Grid>
                <Grid item xs={12}>
                    <MuiPickersUtilsProvider utils={DateFnsUtils} locale={i18nUtils.isFrenchLanguage() ? frLocale : null}>
                        <KeyboardDatePicker
                            value={holidayDate}
                            onChange={handleHolidayDateChange}
                            disabled={disableModification}
                            format={i18nUtils.isFrenchLanguage() ? "dd MMMM yyyy" : "MMMM dd, yyyy"}
                            fullWidth
                        />
                    </MuiPickersUtilsProvider>
                </Grid>
                <Grid item xs={12}>
                    <HorizontalBox justifyContent="center">
                        <IconButton onClick={() => props.onDeleteHoliday(props.holiday.key)}>
                            <DeleteIcon />
                        </IconButton>
                        {
                            disableModification
                                ?
                                <IconButton onClick={toggleDisableModification}>
                                    <EditIcon />
                                </IconButton>
                                :
                                <IconButton onClick={updateHoliday}>
                                    <SaveIcon />
                                </IconButton>
                        }
                    </HorizontalBox>
                </Grid>
            </Grid>
        </Paper>
    )
}

export default Holiday;