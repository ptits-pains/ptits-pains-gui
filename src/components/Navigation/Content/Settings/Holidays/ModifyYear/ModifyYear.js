import React from "react";
import { makeStyles } from '@material-ui/core/styles';
import {
    Card,
    CardContent,
    Button,
    Switch,
    Paper,
    Collapse,
    FormControlLabel,
    LinearProgress
} from '@material-ui/core';
import DateFnsUtils from "@date-io/date-fns";
import { DatePicker, MuiPickersUtilsProvider } from '@material-ui/pickers';
import { useTranslation } from 'react-i18next';

const useStyles = makeStyles(theme => ({
    root: {
        '& > *': {
            margin: theme.spacing(1),
        },
    },
    container: {
        display: 'flex',
    },
    paper: {
        margin: theme.spacing(1),
    },
    polygon: {
        fill: theme.palette.common.white,
        stroke: theme.palette.divider,
        strokeWidth: 1,
    },
    circular: {
        marginTop: theme.spacing(2),
    }
}));

function ModifyYear(props) {

    const { t } = useTranslation();
    const classes = useStyles();
    const [checked, setChecked] = React.useState(false);

    const handleChange = () => {
        setChecked(prev => !prev);
    };

    const {
        loading,
        currentYear,
        onCurrentYearChange,
        onSubmitCurrentYear
    } = props;

    return (
        <React.Fragment>
            <div>
                <FormControlLabel
                    control={<Switch checked={checked} onChange={handleChange} />}
                    label={t('settings.holidays.modifyYear.title')}
                />
                <div className={classes.container}>
                    <Collapse in={checked}>
                        <Paper elevation={4} className={classes.paper}>
                            <Card className={classes.polygon}>
                                <CardContent className={classes.root}>
                                    <div className={classes.root}>
                                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                            <DatePicker
                                                views={["year"]}
                                                label={t('settings.holidays.modifyYear.yearLabel')}
                                                value={currentYear}
                                                onChange={onCurrentYearChange}
                                            />
                                        </MuiPickersUtilsProvider>
                                        <Button
                                            color="primary"
                                            variant="contained"
                                            onClick={onSubmitCurrentYear}
                                            disabled={loading}
                                        >
                                            {t('common.apply')}
                                        </Button>
                                    </div>
                                </CardContent>
                            </Card>
                            {loading ? <LinearProgress /> : null}
                        </Paper>
                    </Collapse>
                </div>
            </div>
        </React.Fragment>
    );
}

export default ModifyYear;
