import React from 'react';
import Checkbox from '@material-ui/core/Checkbox';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import { useTranslation } from 'react-i18next';

/**
 * this will check if the passed element exists in the passed array
 */
const checkExisting = (element, array) => {

    if (!array || array.size === 0) return false;
    let index = array.indexOf("" + element);
    if (index === -1) {
        return false;
    }
    return true;
}

function NonWorkDays(props) {

    const { t } = useTranslation();

    return (
        <FormControl component="fieldset">
            <FormLabel component="legend"> {t('settings.settingsHeader.nonWorkingDays.title')}</FormLabel>
            <FormGroup aria-label="position" name="position" onChange={props.onNonWorkingDayChange} row>
                {props.days.map(item => {
                    return (
                        <FormControlLabel
                            key={item.index}
                            value={item.index}
                            control={<Checkbox color="primary" />}
                            label={item.label}
                            labelPlacement="end"
                            checked={checkExisting(item.index, props.nonWorkingDays)}
                        />
                    )
                })}
            </FormGroup>
        </FormControl>
    );
}

export default NonWorkDays;