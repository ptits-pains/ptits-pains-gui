import React from 'react';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import { useTranslation } from 'react-i18next';

export default function RollDay(props) {

    const { t } = useTranslation();

    return (
        <FormControl component="fieldset">
            <FormLabel component="legend"> {t('settings.settingsHeader.rollDay.title')} </FormLabel>
            <RadioGroup aria-label="position" name="position" value={parseInt(props.rollsDay)} onChange={props.onRollsDayChange} row>
                {props.days.map(item => {
                    return (
                        <FormControlLabel
                            key={item.index}
                            value={item.index}
                            control={<Radio color="primary" />}
                            label={item.label}
                            labelPlacement="end"
                        />
                    )
                })}
            </RadioGroup>
        </FormControl>
    );
}