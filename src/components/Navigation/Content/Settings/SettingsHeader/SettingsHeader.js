import React from 'react';
import RollDay from './RollDay/RollDay';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import NonWorkDays from './NonWorkDays/NonWorkDays';
import LinearProgress from '../../../../UI/LinearProgress/LinearProgress';
import Typography from '@material-ui/core/Typography';
import { useTranslation } from 'react-i18next';

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
}));

export default function SettingsHeader(props) {
    const classes = useStyles();
    const { t } = useTranslation();
    const days = [
        { index: 0, label: t('settings.settingsHeader.days.sunday') },
        { index: 1, label: t('settings.settingsHeader.days.monday') },
        { index: 2, label: t('settings.settingsHeader.days.tuesday') },
        { index: 3, label: t('settings.settingsHeader.days.wednesday') },
        { index: 4, label: t('settings.settingsHeader.days.thursday') },
        { index: 5, label: t('settings.settingsHeader.days.friday') },
        { index: 6, label: t('settings.settingsHeader.days.saturday') }
    ];

    return (

        <div className={classes.root}>
            <Grid container spacing={3}>
                <Grid item xs={12}>
                    <Paper className={classes.paper}>
                        <Typography variant="h6" gutterBottom>
                            {t('settings.settingsHeader.note.noteLabel')}
                        </Typography>
                        <Typography variant="body1" gutterBottom>
                            {t('settings.settingsHeader.note.noteContent')}
                        </Typography>
                    </Paper>
                </Grid>
                <Grid item xs={12}>
                    <Paper className={classes.paper}>
                        {props.loadingRollsDay ? <LinearProgress /> :
                            <RollDay
                                rollsDay={props.rollsDay}
                                onRollsDayChange={props.onRollsDayChange}
                                days={days}
                            />
                        }
                    </Paper>
                </Grid>
                <Grid item xs={12}>
                    <Paper className={classes.paper}>
                        {props.loadingNonWorkingDays ? <LinearProgress /> :
                            <NonWorkDays
                                nonWorkingDays={props.nonWorkingDays}
                                onNonWorkingDayChange={props.onNonWorkingDayChange}
                                days={days}
                            />
                        }
                    </Paper>
                </Grid>
            </Grid>
        </div>
    );
}