import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Container from '@material-ui/core/Container';
import Banner from '../Banner/Banner';
import Menu from '../Menu/Menu';
import Footer from '../Footer/Footer';


const Layout = (props) => {
    const classes = useStyles();
    const [open, setOpen] = React.useState(false);
    const handleDrawerOpen = () => {
      setOpen(true);
    };
    const handleDrawerClose = () => {
      setOpen(false);
    };
    return (
        <div className={classes.root}>
          <CssBaseline />
          <Banner handleDrawerOpen={handleDrawerOpen} open={open}/>
         <Menu classes={classes} open={open} handleDrawerClose={handleDrawerClose}/>
          <main className={classes.content}>
            <div className={classes.appBarSpacer} />
            <Container maxWidth="lg" className={classes.container}>
                {props.children}
            </Container>
            <Footer />
          </main>
        </div>
      );
}

export default Layout;

const useStyles = makeStyles(theme => ({
  root: {
    display: "flex"
  },
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    height: "100vh",
    overflow: "auto"
  },

  container: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4)
  }
}));