import React, { Fragment } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import CssBaseline from '@material-ui/core/CssBaseline';
import PersonIcon from '@material-ui/icons/Person';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import { Redirect } from 'react-router-dom';
import * as appRoutes from '../../../../constants/routes';
import { withRouter } from 'react-router-dom';
import IconButton from '@material-ui/core/IconButton';
import SettingsIcon from '@material-ui/icons/Settings';
import { useTranslation } from 'react-i18next';

const StyledMenu = withStyles({
    paper: {
        border: '1px solid #d3d4d5',
    },
})(props => (
    <Menu
        elevation={0}
        getContentAnchorEl={null}
        anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'center',
        }}
        transformOrigin={{
            vertical: 'top',
            horizontal: 'center',
        }}
        {...props}
    />
));

const StyledMenuItem = withStyles(theme => ({
    root: {
        '&:focus': {
            backgroundColor: theme.palette.primary.main,
            '& .MuiListItemIcon-root, & .MuiListItemText-primary': {
                color: theme.palette.common.white,
            },
        },
    },
}))(MenuItem);

function LogOut(props) {

    const { t } = useTranslation();
    const [anchorEl, setAnchorEl] = React.useState(null);
    const [userLoggedOut, setUserLoggedOut] = React.useState(false);

    function handleClick(event) {
        setAnchorEl(event.currentTarget);
    }

    function handleDeconnectionClick(event) {
        setUserLoggedOut(true)
    }

    function handleAccountClick(event) {
        props.history.push(appRoutes.ACCOUNT)
    }

    function handleSettingsClick(event) {
        props.history.push(appRoutes.ACCOUNT_SETTINGS)
    }

    function handleClose() {
        setAnchorEl(null);
    }


    if (userLoggedOut) {
        return <Redirect to={appRoutes.LOG_OUT} />
    }

    return (
        <Fragment>
            <IconButton onClick={handleClick} style={{color:'white'}}>
                <AccountCircleIcon fontSize="large" style={{ cursor: 'pointer' }} />
            </IconButton>
            <StyledMenu
                id="customized-menu"
                anchorEl={anchorEl}
                keepMounted
                open={Boolean(anchorEl)}
                onClose={handleClose}
            >
                <StyledMenuItem onClick={(event) => handleSettingsClick(event)}>
                    <ListItemIcon>
                        <SettingsIcon />
                    </ListItemIcon>
                    <ListItemText primary={t('personButton.settings')} />
                </StyledMenuItem>
                <StyledMenuItem onClick={(event) => handleAccountClick(event)}>
                    <ListItemIcon>
                        <PersonIcon />
                    </ListItemIcon>
                    <ListItemText primary={t('personButton.profile')} />
                </StyledMenuItem>
                <CssBaseline />
                <StyledMenuItem onClick={(event) => handleDeconnectionClick(event)}>
                    <ListItemIcon>
                        <ExitToAppIcon />
                    </ListItemIcon>
                    <ListItemText primary={t('personButton.logout')} />
                </StyledMenuItem>
            </StyledMenu>
        </Fragment>
    );
}

export default withRouter(LogOut);
