import React from 'react';
import NotificationsIcon from '@material-ui/icons/Notifications';
import { withRouter } from 'react-router-dom';
import IconButton from '@material-ui/core/IconButton';
import * as appRoutes from '../../../../constants/routes';

function Notifications(props) {

    function handleClick(event) {
        props.history.push(appRoutes.NOTIFICATIONS)
    }

    return (<IconButton color="primary" onClick={handleClick} style={{color:'white'}}>
        <NotificationsIcon style={{ cursor: 'pointer' }} />
    </IconButton>
    )
}

export default withRouter(Notifications);