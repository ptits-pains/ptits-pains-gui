import React from 'react';
import clsx from 'clsx';
import Drawer from '@material-ui/core/Drawer';
import IconButton from '@material-ui/core/IconButton';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import Divider from '@material-ui/core/Divider';
import List from '@material-ui/core/List';
import MenuItem from '../../UI/MenuItem/MenuItem'
import DashboardIcon from '@material-ui/icons/Dashboard';
import PeopleIcon from '@material-ui/icons/People';
import SettingsIcon from '@material-ui/icons/Settings';
import AutorenewIcon from '@material-ui/icons/Autorenew';
import GitHubIcon from '@material-ui/icons/GitHub';
import { NavLink } from 'react-router-dom';
import * as appRoutes from '../../../constants/routes';
import { makeStyles } from "@material-ui/core/styles";
import croissant from '../../../assets/croissant-ico.png';
import HorizontalBox from '../../UI/HorizontalBox/HorizontalBox';
import SubTitle from '../../UI/SubTitle/SubTitle';
import TelegramIcon from '@material-ui/icons/Telegram';
import ScheduleIcon from '@material-ui/icons/Schedule';
import { useTranslation } from 'react-i18next';
import { connect } from 'react-redux';

const drawerWidth = 240;
const useStyles = makeStyles(theme => ({
    root: {
        backgroundColor: "#c62828c7",
        display: "flex"
    },
    ico: {
        height: "40px",
        width: "40px",
        marginRight: theme.spacing(3)
    },
    toolbarIcon: {
        display: "flex",
        alignItems: "center",
        justifyContent: "flex-end",
        padding: "0 8px",
        ...theme.mixins.toolbar
    },
    drawerPaper: {
        position: "relative",
        whiteSpace: "nowrap",
        width: drawerWidth,
        transition: theme.transitions.create("width", {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen
        })
    },
    drawerPaperClose: {
        overflowX: "hidden",
        transition: theme.transitions.create("width", {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen
        }),
        width: theme.spacing(7),
        [theme.breakpoints.up("sm")]: {
            width: theme.spacing(9)
        }
    },
    appBarSpacer: theme.mixins.toolbar,
    content: {
        flexGrow: 1,
        height: "100vh",
        overflow: "auto"
    },
    fixedHeight: {
        height: 240
    }
}));

const Menu = (props) => {

    const { t } = useTranslation();
    const classes = useStyles();
    const { isAdmin } = props;

    return (
        <Drawer
            variant="permanent"
            classes={{ paper: clsx(classes.drawerPaper, !props.open && classes.drawerPaperClose), }}
            open={props.open}
        >
            <div className={classes.toolbarIcon}>
                <HorizontalBox justifyContent="flex-start">
                    <img src={croissant} alt="Ptit's Pains" className={classes.ico} />
                    <SubTitle>
                        Ptit's Pains
                    </SubTitle>
                </HorizontalBox>
                <IconButton onClick={props.handleDrawerClose}>
                    <ChevronLeftIcon />
                </IconButton>
            </div>
            <Divider />
            <List>

                <NavLink exact to={appRoutes.DASH_BOARD} activeClassName={classes.root}>
                    <MenuItem menuName="Dashboard">
                        <DashboardIcon />
                    </MenuItem>
                </NavLink>

                <NavLink exact to={appRoutes.SWITCH} activeClassName={classes.root}>
                    <MenuItem menuName={t('menu.switch')}>
                        <AutorenewIcon />
                    </MenuItem>
                </NavLink>
                <NavLink exact to={appRoutes.REQUESTS} activeClassName={classes.root}>
                    <MenuItem menuName={t('menu.requests')}>
                        <TelegramIcon />
                    </MenuItem>
                </NavLink>
                {isAdmin ?
                    <React.Fragment>
                        <NavLink exact to={appRoutes.USERS} activeClassName={classes.root}>
                            <MenuItem menuName={t('menu.users')}>
                                <PeopleIcon />
                            </MenuItem>
                        </NavLink>
                        <NavLink exact to={appRoutes.SCHEDULE} activeClassName={classes.root}>
                            <MenuItem menuName={t('menu.schedule')}>
                                <ScheduleIcon />
                            </MenuItem>
                        </NavLink>
                        <NavLink exact to={appRoutes.SETTINGS} activeClassName={classes.root}>
                            <MenuItem menuName={t('menu.settings')}>
                                <SettingsIcon />
                            </MenuItem>
                        </NavLink>
                    </React.Fragment>
                    : null}
                <NavLink exact to={appRoutes.TECHNOLOGIES} activeClassName={classes.root}>
                    <MenuItem menuName={t('menu.technologies')}>
                        <GitHubIcon />
                    </MenuItem>
                </NavLink>
            </List>
        </Drawer >
    );
}

const mapStateToProps = state => {
    return {
        isAdmin: state.userDetails.isAdmin
    };
};

export default connect(mapStateToProps)(Menu);