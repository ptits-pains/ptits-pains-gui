import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Footer from '../Footer/Footer';
import DynamicTextField from '../../UI/DynamicTextField/DynamicTextField';
import LinearProgress from '../../UI/LinearProgress/LinearProgress';
import { createMuiTheme } from "@material-ui/core/styles";
import { ThemeProvider } from "@material-ui/styles";
import { NavLink } from 'react-router-dom';
import * as appRoutes from '../../../constants/routes';
import { useTranslation } from 'react-i18next';

const useStyles = makeStyles(theme => ({
  '@global': {
    body: {
      backgroundColor: theme.palette.common.white,
    },
  },
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

const outerTheme = createMuiTheme({
  palette: {
    primary: {
      main: "#c62828"
    }
  }
});

export default function SignUp(props) {

  const classes = useStyles();
  const { t } = useTranslation();

  return (
    <ThemeProvider theme={outerTheme}>
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            {t('signUp.title')}
          </Typography>
          <form className={classes.form} noValidate>
            <Grid container spacing={2}>
              <Grid item xs={12} sm={6}>
                <DynamicTextField
                  type="text"
                  label={t('signUp.firstName')}
                  error={props.firstNameError}
                  autoFocus={true}
                  handleChangeFunction={props.handleFirstNameChange}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <DynamicTextField
                  type="text"
                  label={t('signUp.lastName')}
                  error={props.lastNameError}
                  handleChangeFunction={props.handleLastNameChange}
                />
              </Grid>
              <Grid item xs={12}>
                <DynamicTextField
                  type="email"
                  label={t('signUp.email')}
                  error={props.emailError}
                  handleChangeFunction={props.handleEmailAddressChange}
                />
              </Grid>
              <Grid item xs={12}>
                <DynamicTextField
                  type="password"
                  label={t('signUp.password')}
                  error={props.passwordError}
                  handleChangeFunction={props.handlePasswordChange}
                />
              </Grid>
              <Grid item xs={12}>
                <DynamicTextField
                  type="password"
                  label={t('signUp.confirm')}
                  error={props.confirmPasswordError}
                  handleChangeFunction={props.handleConfirmPasswordChange}
                />
              </Grid>
            </Grid>
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
              onClick={props.handleSubmit}
              disabled={props.loading}
            >
              {t('signUp.subscribe')}
            </Button>
            {props.loading ? <LinearProgress /> : null}
            <Grid container justify="flex-end">
              <Grid item>
                <NavLink exact to={appRoutes.ROOT} >
                  {t('signUp.existAccount')}
                </NavLink>
              </Grid>
            </Grid>
          </form>
        </div>
        <Box mt={5}>
          <Footer />
        </Box>
      </Container>
    </ThemeProvider>
  );
}