import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import { NavLink } from 'react-router-dom';
import * as appRoutes from '../../../../constants/routes';
import { useTranslation } from 'react-i18next';

const useStyles = makeStyles(theme => ({
    typography:{
        margin: theme.spacing(2),
    },
  link: {
    margin: theme.spacing(1),
  },
}));

export default function Links() {

  const classes = useStyles();
  const { t } = useTranslation();

  return (
    <Typography  className={classes.typography}>
      <NavLink exact to={appRoutes.SIGN_UP} className={classes.link}>
        {t('signIn.createAccountLabel1')}
      </NavLink>
      {t('signIn.createAccountLabel2')}
    </Typography>
  );
}