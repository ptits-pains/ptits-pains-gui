import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import Footer from '../Footer/Footer';
import SignUpLink from './SignUpLink/SignUpLink';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import IconButton from '@material-ui/core/IconButton';
import InputAdornment from '@material-ui/core/InputAdornment';
import LinearProgress from '../../UI/LinearProgress/LinearProgress';
import SnackBar from '../../UI/SnackBar/SnackBar';
import * as alertTypes from '../../../constants/alerts';
import { createMuiTheme } from "@material-ui/core/styles";
import { ThemeProvider } from "@material-ui/styles";
import { useTranslation } from 'react-i18next';

const useStyles = makeStyles(theme => ({
    '@global': {
        body: {
            backgroundColor: theme.palette.common.white,
        },
    },
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));


const outerTheme = createMuiTheme({
    palette: {
        primary: {
            main: "#c62828"
        }
    }
});

const SignIn = (props) => {

    const classes = useStyles();
    const { t } = useTranslation();
    const [values, setValues] = React.useState({
        showPassword: false,
    });
    const handleClickShowPassword = () => {
        setValues({ ...values, showPassword: !values.showPassword });
    };
    const handleMouseDownPassword = event => {
        event.preventDefault();
    };

    let button = (
        <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
        >
            {t('signIn.buttonLabel')}
        </Button>
    );

    if (props.loading) {
        button = (
            <React.Fragment>
                <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    color="primary"
                    className={classes.submit}
                    disabled={true}
                >
                    {t('signIn.buttonLabel')}
            </Button>
                <LinearProgress />
            </React.Fragment>
        )
    }

    return (
        <ThemeProvider theme={outerTheme}>
            <Container component="main" maxWidth="xs">
                <CssBaseline />
                <div className={classes.paper}>
                    <Avatar className={classes.avatar}>
                        <LockOutlinedIcon />
                    </Avatar>
                    <Typography component="h1" variant="h5">
                    {t('signIn.buttonLabel')}
                </Typography>
                    <form className={classes.form} onSubmit={props.handleSubmit}>
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            label={t('signIn.identifier')}
                            autoFocus
                            onChange={props.handleEmailChange}
                        />
                        <TextField
                            variant="outlined"
                            type={values.showPassword ? 'text' : 'password'}
                            margin="normal"
                            label={t('signIn.password')}
                            required
                            fullWidth
                            value={values.password}
                            onChange={props.handlePasswordChange}
                            InputProps={{
                                endAdornment: (
                                    <InputAdornment position="end">
                                        <IconButton
                                            edge="end"
                                            aria-label="toggle password visibility"
                                            onClick={handleClickShowPassword}
                                            onMouseDown={handleMouseDownPassword}
                                        >
                                            {values.showPassword ? <VisibilityOff /> : <Visibility />}
                                        </IconButton>
                                    </InputAdornment>
                                ),
                            }}
                        />
                        <SignUpLink />
                        {button}
                    </form>
                </div>
                <Footer />
                {props.error ? <SnackBar type={alertTypes.ERROR} message={props.error} /> : null}
            </Container>
        </ThemeProvider>
    );
}

export default SignIn;