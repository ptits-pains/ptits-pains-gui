import React from 'react';
import TextField from '@material-ui/core/TextField';


export default function DynamicTextField({ type, label, handleChangeFunction, error, autoFocus }) {

    let errorMessage;
    if (error)
        error.some(element => {
            return element.toShowError && (errorMessage = element.errorMessage);
        });

    return (
        <TextField
            variant="outlined"
            fullWidth
            type={type}
            label={label}
            helperText={errorMessage ? errorMessage : null}
            error={errorMessage ? true : false}    
            autoFocus={autoFocus}
            onChange={handleChangeFunction}
        />
    )
}