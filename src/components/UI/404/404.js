import React from "react";
import CssBaseline from "@material-ui/core/CssBaseline";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import Footer from "../../Navigation/Footer/Footer";
import { NavLink } from "react-router-dom";
import SubTitle from "../SubTitle/SubTitle";
import croissant from "../../../assets/croissant-ico.png";
import { useTranslation } from 'react-i18next';

const useStyles = makeStyles(theme => ({
  "@global": {
    body: {
      backgroundColor: theme.palette.common.white
    }
  },
  paper: {
    marginTop: theme.spacing(15),
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  },
  image: {
    height: "200px",
    width: "200px"
  },
}));

export default function Page404() {

  const { t } = useTranslation();
  const classes = useStyles();

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <img src={croissant} alt="Ptit's Pains" className={classes.image} />
        <Typography variant="h1" component="h2" gutterBottom>
          404
        </Typography>
        <Typography variant="h6" gutterBottom>
          {t('404.title')}
        </Typography>
        <Typography variant="subtitle1" gutterBottom>
          {t('404.subtitle')}
        </Typography>
        <NavLink exact to="/">
          <SubTitle> {t('404.back')}</SubTitle>
        </NavLink>
        <Footer />
      </div>
    </Container>
  );
}
