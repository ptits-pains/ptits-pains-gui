import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { useTranslation } from 'react-i18next';

const DialogLayout = (props) => {

    const [open, setOpen] = React.useState(true);
    const { t } = useTranslation();

    const {
        onAcceptClick,
        onCancelClick,
        title,
        content
    } = props;

    const handleClose = (event, choice) => {
        setOpen(false);
        if (choice === 'ok') {
            onAcceptClick();
        } else {
            if (onCancelClick)
                onCancelClick();
        }
    };

    return (
        <Dialog
            open={open}
            onClose={handleClose}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
        >
            <DialogTitle id="alert-dialog-title">{title}</DialogTitle>
            <DialogContent>
                <DialogContentText id="alert-dialog-description">
                    {content}
                </DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button onClick={(event) => handleClose(event, 'ko')} color="primary">
                    {t('common.no')}
                </Button>
                <Button onClick={(event) => handleClose(event, 'ok')} color="primary" autoFocus>
                    {t('common.yes')}
                </Button>
            </DialogActions>
        </Dialog>
    )
}

export default DialogLayout;