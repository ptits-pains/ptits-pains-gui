import React from "react";
import Box from "@material-ui/core/Box";

const HorizontalBox = props => {
  return (
    <Box display="flex" justifyContent={props.justifyContent} p={1}>
      {props.children}
    </Box>
  );
};

export default HorizontalBox;

/*
SOURCE https://material-ui.com/system/flexbox/
<Box justifyContent="flex-start">…
<Box justifyContent="flex-end">…
<Box justifyContent="center">… 
*/