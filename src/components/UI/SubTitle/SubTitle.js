import React from 'react';
import Typography from '@material-ui/core/Typography';

export default function SubTitle(props) {
  return (
    <Typography variant="subtitle2" gutterBottom>
      {props.children}
    </Typography>
  );
}