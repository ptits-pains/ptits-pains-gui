import React from 'react'
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';

export default function MenuItem(props){

    return(
        <ListItem button>
        <ListItemIcon>
            {props.children}
        </ListItemIcon>
        <ListItemText primary={props.menuName} />
        </ListItem>
    );
}