import React from "react";
import CircularProgress from '@material-ui/core/CircularProgress';

export default function SmallCircularProgress (){

    return <CircularProgress size={20} thickness={5}/>
}