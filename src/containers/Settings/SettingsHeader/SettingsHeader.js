import React from 'react';
import SettingsLayout from '../../../components/Navigation/Content/Settings/SettingsHeader/SettingsHeader';
import firebase from '../../../config/firebase';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import * as appRoutes from '../../../constants/routes';

class SettingsHeader extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            rollsDay: 5,  //dinner rolls day 
            nonWorkingDays: [],
            loadingRollsDay: true,
            loadingNonWorkingDays: true
        }
    }

    componentDidMount() {
        this.ref = firebase.syncState('rollsDay', {
            context: this,
            state: 'rollsDay',
            then: () => {
                this.setState({ loadingRollsDay: false })
            },
            onFailure: function () {
                console.log("Failed!");
            }
        })

        this.refNonWorkingDays = firebase.syncState('nonWorkingDays', {
            context: this,
            state: 'nonWorkingDays',
            asArray: true,
            then: () => {
                this.setState({ loadingNonWorkingDays: false })
            },
            onFailure: function () {
                console.log("Failed!");
            }
        })
    }

    componentWillUnmount() {
        firebase.removeBinding(this.ref);
        firebase.removeBinding(this.refNonWorkingDays);
    }

    handleRollsDayChange = (event) => {
        this.setState({ rollsDay: event.target.value })
    }

    handleNonWorkingDayChange = (event) => {
        let newState = Array.from(this.state.nonWorkingDays);
        let index = newState.indexOf(event.target.value);
        if (index === -1) {
            newState.push(event.target.value);
        } else {
            newState.splice(index, 1);
        }
        this.setState({ nonWorkingDays: newState })
    }

    render() {
        if(!this.props.userId){
            return <Redirect to={appRoutes.ROOT} />
        }
        return (
                <SettingsLayout
                    rollsDay={this.state.rollsDay}
                    onRollsDayChange={this.handleRollsDayChange}
                    nonWorkingDays={this.state.nonWorkingDays}
                    onNonWorkingDayChange={this.handleNonWorkingDayChange}
                    loadingRollsDay={this.state.loadingRollsDay}
                    loadingNonWorkingDays={this.state.loadingNonWorkingDays}
                />
        )
    }
}

const mapStateToProps = state => {
    return {
        userId: state.auth.userId
    };
};

export default connect(mapStateToProps)(SettingsHeader);