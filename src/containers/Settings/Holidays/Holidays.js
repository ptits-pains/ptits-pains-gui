import React from 'react';
import HolidaysLayout from '../../../components/Navigation/Content/Settings/Holidays/Holidays';
import base from '../../../config/firebase';
import * as dateUtils from '../../../utils/DateUtils';
import SnackBar from '../../../components/UI/SnackBar/SnackBar';
class Holidays extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            currentYear: "" + new Date().getFullYear(),      //object to store current year
            holidays: [],                                   //list of holidays
            error: false,
            errorMessage: null,
            loading: true
        }
    }


    componentDidMount() {

        this.initError();
        base.fetch('currentYear', {
            context: this
        }).then(currentYear => {
            
            // if the current year does not exist we gonna add it
            if (!currentYear || (typeof currentYear !== "string")) {

                return base.post(`currentYear`, {
                    data: this.state.currentYear
                })
            } else {
                this.setState({ currentYear: currentYear})
                return null;
            }
        }).then(() => {
            //after getting the current year we will bind holidays state to the firebase database
            this.ref = base.syncState('holidays', {
                context: this,
                state: 'holidays',
                asArray: true,
                then: () => {
                    this.setState({ loading: false })
                },
                onFailure: function (err) {
                    this.setError("error to sync the holidays : " + err.message)
                }
            })
        }).catch(err => {
            this.setError(err.message)
        })
    }

    /**
     * unbind the firebase link
     */
    componentWillUnmount() {
        base.removeBinding(this.ref);
    }

    /**
     * init the error
     */
    initError = () => {

        this.setState({ loading: true, error: false, errorMessage: null })
    }

    /**
     * set an error
     */
    setError = (message) => {

        this.setState({ loading: false, error: true, errorMessage: message })
    }

    /**
     * used to save a new holiday
     */
    handleSaveNewHoliday = (event, name, date) => {
        this.initError();
        const stringDate = dateUtils.jsDateToString(date);
        base.post(`holidays/${stringDate}`, {
            data: {
                name: name, date: stringDate
            }
        }).then(() => {
            this.setState({
                loading: false
            })
        }).catch(err => {
            this.setError(err.message)
        });
    }

    /**
    * update an existing holiday
    */
    handleUpdateHoliday = (name, date, key) => {
        this.initError();
        base.remove(`holidays/${key}`)
            .then(() => {
                this.handleSaveNewHoliday(null, name, date)
            }).catch(err => {
                this.setError(err.message)
            });
    }

    /**
     * delete an existing holiday
     */
    handleDeleteHoliday = (key) => {
        this.initError();
        base.remove(`holidays/${key}`)
            .then(() => {
                this.setState({
                    loading: false
                })
            }).catch(err => {
                this.setError(err.message)
            });
    }

    handleCurrentYearChange = (date) => {
        this.setState({
            currentYear: "" + date.getFullYear()
        })
    }

    /**
     * when the user clicks on apply we will change the year of all our holidayss
     */
    handleSubmitCurrentYear = () => {
        this.initError();
        base.post('currentYear', {
            data: this.state.currentYear
        }).then(() => {
            let holidays = [...this.state.holidays];
            holidays.forEach(holiday => {
                let date = holiday.date;
                date = this.state.currentYear + date.substring(4, date.length);
                holiday.date = date;
                holiday.key = date;
            })

            // sort the rolls dinners by date
            holidays.sort((a, b) => {
                return dateUtils.realDiffTwoDatesDaysString(a.date, b.date);
            });

            //and now we gonna format our holidays
            let holidaysToStore = {};
            holidays.forEach(holiday => {
                holidaysToStore[holiday.key] = {
                    date: holiday.date,
                    name: holiday.name
                }
            })

            //finally we gonna store them
            return base.post('holidays', {
                data: holidaysToStore
            })
        }).then(() => {
            this.setState({
                loading: false
            })
        }).catch(err => {
            this.setError(err.message);
        });
    }

    render() {
        return (
            <React.Fragment>
                <HolidaysLayout
                    holidays={this.state.holidays}
                    loading={this.state.loading}
                    currentYear={this.state.currentYear}
                    onCurrentYearChange={this.handleCurrentYearChange}
                    onSaveNewHoliday={this.handleSaveNewHoliday}
                    onDeleteHoliday={this.handleDeleteHoliday}
                    onUpdateHoliday={this.handleUpdateHoliday}
                    onSubmitCurrentYear={this.handleSubmitCurrentYear}
                />
                {this.state.error ?
                    <SnackBar type="error" message={this.state.errorMessage} />
                    : null}
            </React.Fragment>
        )
    }
}

export default Holidays;