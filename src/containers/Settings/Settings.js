import React from 'react';
import Holidays from './Holidays/Holidays';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import * as appRoutes from '../../constants/routes';
import SettingsHeader from "./SettingsHeader/SettingsHeader";

class Settings extends React.Component {

    render() {
        if(!this.props.userId){
            return <Redirect to={appRoutes.ROOT} />
        }
        return (
            <React.Fragment>
                <SettingsHeader />
                <Holidays />
            </React.Fragment>
        )
    }
}

const mapStateToProps = state => {
    return {
        userId: state.auth.userId
    };
};

export default connect(mapStateToProps)(Settings);