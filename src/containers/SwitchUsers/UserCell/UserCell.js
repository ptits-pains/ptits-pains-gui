import React from "react";
import TableCell from '@material-ui/core/TableCell';
import SwapHorizIcon from '@material-ui/icons/SwapHoriz';
import IconButton from '@material-ui/core/IconButton';
import * as stateConstants from '../../../constants/state';
import * as dateUtils from "../../../utils/DateUtils";
import base from '../../../config/firebase';
import * as alertConstants from '../../../constants/alerts';
import SnackBar from '../../../components/UI/SnackBar/SnackBar';
import NotInterestedIcon from '@material-ui/icons/NotInterested';
import SmallCircularProgress from '../../../components/UI/SmallCircularProgress/SmallCircularProgress';
import { withTranslation } from 'react-i18next';

class UserCell extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            error: false,
            errorMessage: null
        }
    }

    /**
     * in this function we gonna send a notification to a college to ask him to switch his day versus the current user day
     * we gonna send this data
     *      the current user day    : collegeDay
     *      the college day         : myDay
     *      my user id  : collegeID
     * 
     * we gonna also archive this request so the user can delete it if he want to
     */
    handleSwitchClick = () => {
        this.init();
        //create a unique id using the timestamp
        let timestamp = dateUtils.timestamp();
        Promise.all(
            [this.sendNotification(timestamp, this.props), this.sendRequest(timestamp, this.props)]
        ).then(values => {
            this.setState({
                loading: false
            })
        }).catch(error => {
            this.setState({
                loading: false,
                error: true,
                errorMessage: error.message
            })
        })
    }

    //send a notification to college, we will transfer the current user data
    sendNotification = (timestamp, props) => {
        return new Promise(function (resolve, reject) {
            base.post(`notifications/${props.userId}/${timestamp}`, {
                data: {
                    myDay: props.date,
                    collegeDay: props.currentUserRollsDay,
                    collegeID: props.currentUserId,
                    state: stateConstants.IN_PROGRESS,
                    requestTime: dateUtils.isoStringDate()
                }
            }).then(() => {
                resolve("user notified successfully")
            }).catch(err => {
                reject(err)
            });
        })
    }

    //archive this request
    sendRequest = (timestamp, props) => {
        return new Promise(function (resolve, reject) {
            base.post(`requests/${props.currentUserId}/${timestamp}`, {
                data: {
                    myDay: props.currentUserRollsDay,
                    collegeDay: props.date,
                    collegeID: props.userId,
                    state: stateConstants.SENT,
                    requestTime: dateUtils.isoStringDate()
                }
            }).then(() => {
                resolve("request sended successfully")
            }).catch(err => {
                reject(err)
            });
        })
    }

    init = () => {
        this.setState({
            loading: true,
            error: false,
            errorMessage: null
        })
    }

    render() {
        const { t } = this.props;
        if (this.state.error) {
            return <SnackBar type={alertConstants.ERROR} message={this.state.errorMessage} />
        }

        let iconToShow = <IconButton disabled={true}><NotInterestedIcon /></IconButton>;
        if (this.state.loading) {
            iconToShow = <IconButton><SmallCircularProgress /></IconButton>
        } else if (this.props.userId !== this.props.currentUserId) {
            iconToShow = <IconButton onClick={this.handleSwitchClick} disabled={this.props.date === undefined || this.props.date === null}><SwapHorizIcon fontSize="small" /></IconButton>
        }
        return (<React.Fragment>
            <TableCell component="th" scope="row">
                {this.props.name}
            </TableCell>
            <TableCell >{this.props.date ? t('moment.shortDate', { date: this.props.date }) : null}</TableCell>
            <TableCell >
                {iconToShow}
            </TableCell>
        </React.Fragment>
        )
    }
}

export default withTranslation()(UserCell);