import React from 'react';
import base from '../../config/firebase';
import SnackBar from '../../components/UI/SnackBar/SnackBar';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import * as appRoutes from '../../constants/routes';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import LinearProgress from '../../components/UI/LinearProgress/LinearProgress';
import UserCell from './UserCell/UserCell';
import * as alertConstants from '../../constants/alerts';
import { withTranslation } from 'react-i18next';

function createData(name, date, userId) {
    return { name, date, userId };
}

function getRows(data) {

    return data.map(item => createData(
        item.firstName + " " + item.lastName,
        item.nextDate,
        item.userId
    )
    )
}

class SwitchUsers extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            loading: true,
            usersData: null,
            error: false,
            errorMessage: null,
            userRollsDay: null
        }
    }

    /**
     * in this function we will get users rolls days an then foreach user we will call users/ endpoint to get
     * user details
     */
    componentDidMount() {

        this.init();
        base.fetch('rollsDinner', {
            context: this,
            asArray: true
        }).then(data => {

            // now foreach user we gonna get his profile data to show last name and first name
            let promises = [];
            data.forEach(element => {
                promises.push(
                    new Promise(function (resolve, reject) {
                        base.fetch(`users/${element.userId}`, {
                            context: this
                        }).then((response) => resolve({ ...element, ...response }))
                            .catch((error) => reject(error))
                    })
                )
            });

            Promise.all(promises).then((values) => {
                this.setState({
                    loading: false,
                    usersData: values
                })
                let userRolls = values.filter(item => item.userId === this.props.userId);
                if (userRolls && userRolls.length > 0) {
                    this.setState({
                        userRollsDay: userRolls[0].nextDate
                    })
                }
            }).catch(error => {
                this.initError(error)
            })
        }).catch(error => {
            this.initError(error)
        })
    }

    initError = (error) => {
        this.setState({
            loading: false,
            error: true,
            errorMessage: error.message
        })
    }

    init = () => {
        this.setState({
            loading: true,
            error: false,
            errorMessage: null
        })
    }

    render() {
        const { t } = this.props;
        if (!this.props.userId) {
            return <Redirect to={appRoutes.ROOT} />
        }
        if (this.state.error) {
            return <SnackBar type={alertConstants.ERROR} message={this.state.errorMessage} />
        }
        let rows = [];
        if (this.state.usersData) {
            rows = getRows(this.state.usersData);
        }
        return (
            <Paper>
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell>{t('switchUsers.name')}</TableCell>
                            <TableCell>{t('switchUsers.date')}</TableCell>
                            <TableCell></TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {rows.map((row, index) => (
                            <TableRow key={index}>
                                <UserCell
                                    name={row.name}
                                    date={row.date}
                                    currentUserId={this.props.userId}
                                    userId={row.userId}
                                    currentUserRollsDay={this.state.userRollsDay}
                                />
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
                {this.state.loading ? <LinearProgress /> : null}
            </Paper>
        );
    }
}

const mapStateToProps = state => {
    return {
        userId: state.auth.userId
    };
};

export default connect(mapStateToProps)(withTranslation()(SwitchUsers));