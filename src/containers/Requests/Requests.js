import React from "react";
import { Redirect } from 'react-router-dom';
import * as appRoutes from '../../constants/routes';
import { connect } from 'react-redux';
import Request from './Request/Request';
import base from '../../config/firebase';
import SnackBar from '../../components/UI/SnackBar/SnackBar';
import { Grid } from '@material-ui/core';
import CircularProgress from '@material-ui/core/CircularProgress';
import * as alertConstants from '../../constants/alerts';

class Requests extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            requests: null,
            loading: false,
            error: false,
            errorMessage: null,
        }
    }

    componentDidMount() {
        // get all requests
        const {
            userId
        } = this.props;

        if (userId) {
            this.setState({
                loading: true
            })
            base.fetch(`requests/${userId}`, {
                context: this,
                asArray: true
            }).then(data => {
                this.setState({
                    requests: data,
                    loading: false
                })
                console.log('data request', `requests/${userId}`, data)
            }).catch(err => {
                this.setState({
                    error: true,
                    errorMessage: err.message
                })
            })
        }
    }

    handleDeleteRequest = (requestId) => {
        let requests = [...this.state.requests];
        requests = requests.filter(request => request.key !== requestId);
        this.setState({
            requests: requests
        })
    }

    render() {

        if (!this.props.userId) {
            return <Redirect to={appRoutes.ROOT} />
        }

        if (this.state.loading || !this.state.requests) {
            return <CircularProgress />;
        }

        return (
            <React.Fragment>
                <Grid
                    container
                    spacing={4}
                >
                    {this.state.requests.map(request =>
                        <Grid
                            key={request.key}
                            item
                            lg={6}
                            md={6}
                            xl={4}
                            xs={12}
                        >
                            <Request
                                requestId={request.key}
                                userId={this.props.userId}
                                myDay={request.myDay}
                                collegeDay={request.collegeDay}
                                collegeID={request.collegeID}
                                state={request.state}  //state is accepted, refused, deleted or sent
                                onDeleteRequest={this.handleDeleteRequest}
                            />
                        </Grid>
                    )}
                </Grid>
                {this.state.error ? <SnackBar type={alertConstants.ERROR} message={this.state.errorMessage} /> : null}
            </React.Fragment>
        )
    }
}

const mapStateToProps = state => {
    return {
        userId: state.auth.userId
    };
};

export default connect(mapStateToProps)(Requests);