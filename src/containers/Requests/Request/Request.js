import React from "react";
import RequestLayout from '../../../components/Navigation/Content/Request/Request';
import base from '../../../config/firebase';
import SnackBar from '../../../components/UI/SnackBar/SnackBar';
import * as alertConstants from '../../../constants/alerts';
import { withTranslation } from 'react-i18next';

class Request extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            firstName: null,
            lastName: null,
            loading: false,
            error: false,
            errorMessage: null
        }
    }

    componentDidMount() {
        this.init();
        base.fetch(`users/${this.props.collegeID}`, {
            context: this
        }).then(data => {
            this.setState({
                firstName: data.firstName,
                lastName: data.lastName,
                loading: false
            })
        }).catch(err => {
            this.setError(err.message)
        })
    }

    handleDeleteClick = () => {
        this.init();
        base.remove(`requests/${this.props.userId}/${this.props.requestId}`)
            .then(() => {
                this.setState({
                    loading: false
                })
            }).then(() => {
                //update the parent state
                this.props.onDeleteRequest(this.props.requestId)
            }).catch(error => {
                this.setError(error.message)
            });
    }

    init = () => {
        this.setState({
            loading: true,
            error: false,
            errorMessage: null
        })
    }

    setError = (message) => {
        this.setState({
            loading: false,
            error: true,
            errorMessage: message
        })
    }

    render() {
        const { t } = this.props;
        let myDay = t('moment.fullDate', { date:  this.props.myDay });
        let collegeDay = t('moment.fullDate', { date:  this.props.collegeDay });
        let requestTime = t('moment.fullDateTime', { date:  this.props.fullDateTime });

        return (<React.Fragment>
            <RequestLayout
                firstName={this.state.firstName}
                lastName={this.state.lastName}
                myDay={myDay}
                collegeDay={collegeDay}
                loading={this.state.loading}
                state={this.props.state}
                onDeleteClick={this.handleDeleteClick}
                requestTime={requestTime}
            />
            {this.state.error ? <SnackBar type={alertConstants.ERROR} message={this.state.errorMessage} /> : null}
        </React.Fragment>
        )
    }
}

export default withTranslation()(Request);