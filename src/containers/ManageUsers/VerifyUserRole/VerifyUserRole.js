import React from "react";
import VerifiedUserIcon from '@material-ui/icons/VerifiedUser';
import PersonAddDisabledIcon from '@material-ui/icons/PersonAddDisabled';
import SmallCircularProgress from "../../../components/UI/SmallCircularProgress/SmallCircularProgress";
import IconButton from '@material-ui/core/IconButton';
import * as alertConstants from '../../../constants/alerts';
import SnackBar from '../../../components/UI/SnackBar/SnackBar';
import base from '../../../config/firebase';
import { withTranslation } from 'react-i18next';

class VerfifyUserRole extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            loading: false,
            alert: null,
            alertMessage: null
        }
    }

    handleAddAdminClick = () => {
        this.setState({
            loading: true,
            alert: null,
            alertMessage: null
        })
        base.update(`users/${this.props.userId}`, {
            data: {
                isAdmin: true
            }
        }).then(() => {
            const { t } = this.props;
            this.setState({ loading: false, alert: alertConstants.SUCCESS, alertMessage: t('manageUsers.makeAdmin.successAlert') });
        }).then(() => {
            //update store
            this.props.onUpdateUserRole(this.props.userId, this.props.userIndex);
        }).catch(err => {
            this.setState({ loading: false, alert: alertConstants.ERROR, alertMessage: err.message });
        });
    }

    render() {

        let iconToShow = null;

        if (this.state.loading) {

            iconToShow = <SmallCircularProgress />
        } else if (this.props.deleteAccount) {

            iconToShow = <PersonAddDisabledIcon />
        } else if (this.props.isAdmin) {

            iconToShow = (
                <IconButton disabled={true}>
                    <VerifiedUserIcon style={{ color: "#4caf50" }} />
                </IconButton>
            )
        } else {

            iconToShow = (
                <IconButton onClick={this.handleAddAdminClick} disabled={this.props.deleteAccount}>
                    <VerifiedUserIcon />
                </IconButton>
            )
        }

        return (
            <React.Fragment>
                {iconToShow}
                {this.state.alert ? <SnackBar type={this.state.alert} message={this.state.alertMessage} /> : null}
            </React.Fragment>
        )
    }
}

export default withTranslation()(VerfifyUserRole);