import React from 'react';
import base from '../../config/firebase';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import SnackBar from '../../components/UI/SnackBar/SnackBar';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import * as appRoutes from '../../constants/routes';
import LinearProgress from '../../components/UI/LinearProgress/LinearProgress';
import * as alertConstants from '../../constants/alerts';
import DeleteAccount from "./DeleteAccount/DeleteAccount";
import VerifyUserRole from "./VerifyUserRole/VerifyUserRole";
import { withTranslation } from 'react-i18next';

class ManagerUsers extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            loading: false,
            usersData: null,
            error: false,
            errorMessage: null
        }
    }

    componentDidMount() {

        this.init();
        base.fetch('users', {
            context: this,
            asArray: true
        }).then(data => {
            this.setState({
                usersData: data,
                loading: false
            })
        }).catch(error => {
            this.initError(error)
        })
    }

    initError = (error) => {
        this.setState({
            loading: false,
            error: true,
            errorMessage: error.message
        })
    }

    init = () => {
        this.setState({
            loading: true,
            error: false,
            errorMessage: null
        })
    }

    handleUpdateUserRole = (uid, index) => {

        let users = [...this.state.usersData];
        let toUpdateUser = users.filter(user => user.key === uid)[0];
        if (toUpdateUser) {
            let newUsers = users.filter(user => user.key !== uid);
            toUpdateUser.isAdmin = true;
            newUsers.splice(index, 0, toUpdateUser);
            this.setState({
                usersData: newUsers
            })
        }
    }

    handleDeleteUserAccount = (uid, index) => {
        let users = [...this.state.usersData];
        let toUpdateUser = users.filter(user => user.key === uid)[0];
        if (toUpdateUser) {
            let newUsers = users.filter(user => user.key !== uid);
            toUpdateUser.deleteAccount = true;
            newUsers.splice(index, 0, toUpdateUser);
            this.setState({
                usersData: newUsers
            })
        }
    }

    handleCancelDeleteUserAccount = (uid, index) => {
        let users = [...this.state.usersData];
        let toUpdateUser = users.filter(user => user.key === uid)[0];
        if (toUpdateUser) {
            let newUsers = users.filter(user => user.key !== uid);
            toUpdateUser.deleteAccount = false;
            newUsers.splice(index, 0, toUpdateUser);
            this.setState({
                usersData: newUsers
            })
        }
    }


    render() {
        if (!this.props.userId) {
            return <Redirect to={appRoutes.ROOT} />
        }
        if (this.state.error) {
            return <SnackBar type={alertConstants.ERROR} message={this.state.errorMessage} />
        }
        let rows = [];
        if (this.state.usersData) {
            rows = getRows(this.state.usersData);
        }
        const { t } = this.props;
        return (
            <div>
                <Paper>
                    <Table>
                        <TableHead>
                            <TableRow>
                                <TableCell align="center">{t('manageUsers.userHeader')}</TableCell>
                                <TableCell align="center">{t('manageUsers.isAdminHeader')}</TableCell>
                                <TableCell align="center">{t('common.delete')}</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {rows.map((row, index) => (
                                <TableRow key={index}>
                                    <TableCell align="center" component="th" scope="row">
                                        {row.name}
                                    </TableCell>
                                    <TableCell align="center">
                                        <VerifyUserRole
                                            isAdmin={row.isAdmin}
                                            onUpdateUserRole={this.handleUpdateUserRole}
                                            userId={row.uid}
                                            userIndex={row.index}
                                            deleteAccount={row.deleteAccount === true}
                                        />
                                    </TableCell>
                                    <TableCell align="center">
                                        <DeleteAccount
                                            name={row.name}
                                            deleteAccount={row.deleteAccount === true}
                                            userId={row.uid}
                                            userIndex={row.index}
                                            onDeleteUserAccount={this.handleDeleteUserAccount}
                                            onCancelDeleteUserAccount={this.handleCancelDeleteUserAccount}
                                        />
                                    </TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                    {this.state.loading ? <LinearProgress /> : null}
                    {this.state.error ? <SnackBar type={alertConstants.ERROR} message={this.state.errorMessage} /> : null}
                </Paper>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        userId: state.auth.userId
    };
};

export default connect(mapStateToProps)(withTranslation()(ManagerUsers));

function getRows(data) {

    return data.map((item, index) => {
        return {
            name: item.firstName + " " + item.lastName,
            isAdmin: item.isAdmin,
            uid: item.key,
            deleteAccount: item.deleteAccount,
            index: index
        }
    })
}