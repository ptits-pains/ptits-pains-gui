import React from "react";
import DeleteAccountLayout from "../../../components/Navigation/Content/ManageUsers/DeleteAccount/DeleteAccount";
import SnackBar from '../../../components/UI/SnackBar/SnackBar';
import * as alertConstants from '../../../constants/alerts';
import base from '../../../config/firebase';
import { withTranslation } from 'react-i18next';

class DeleteAccount extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            loading: false,
            alert: false,
            alertMessage: null
        }
    }

    handleDeleteAccount = () => {
        this.init();
        base.update(`users/${this.props.userId}`, {
            data: {
                deleteAccount: true
            }
        }).then(() => {
            const { t } = this.props;
            this.setState({ loading: false, alert: alertConstants.SUCCESS, alertMessage: t('manageUsers.deleteAccount.deleteSuccess') });
        }).then(() => {
            this.props.onDeleteUserAccount(this.props.userId, this.props.userIndex);
        }).catch(err => {
            this.setState({ loading: false, alert: alertConstants.ERROR, alertMessage: err.message });
        });
    }

    handleCancelDeleteAccount = () => {
        this.init();
        base.update(`users/${this.props.userId}`, {
            data: {
                deleteAccount: false
            }
        }).then(() => {
            const { t } = this.props;
            this.setState({ loading: false, alert: alertConstants.SUCCESS, alertMessage: t('manageUsers.deleteAccount.cancelSuccess') });
        }).then(() => {
            this.props.onCancelDeleteUserAccount(this.props.userId, this.props.userIndex);
        }).catch(err => {
            this.setState({ loading: false, alert: alertConstants.ERROR, alertMessage: err.message });
        });
    }

    init = () => {
        this.setState({
            loading: true,
            alert: null,
            alertMessage: null
        })
    }

    render() {
        return (<React.Fragment>
            <DeleteAccountLayout
                deleteAccount={this.props.deleteAccount}
                loading={this.state.loading}
                name={this.props.name}
                onCancelDeleteAccount={this.handleCancelDeleteAccount}
                onDeleteAccount={this.handleDeleteAccount}
            />
            {this.state.alert ? <SnackBar type={this.state.alert} message={this.state.alertMessage} /> : null}
        </React.Fragment>)
    }
}

export default withTranslation()(DeleteAccount);