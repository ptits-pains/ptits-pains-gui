import React, { Component } from "react";
import AccountProfileLayout from "../../../components/Navigation/Content/Account/AccountProfile/AccountProfile";
import { connect } from 'react-redux';
import * as stringUtils from '../../../utils/StringUtils';
import { withTranslation } from 'react-i18next';

class AccountProfile extends Component {

    calculatePFCompleteness = (firstName, lastName, job, description, phone) => {

        // actually the passed parameters are the data that user can modify
        const allParameters = 5;

        let notEmptyParameters = 5;
        if (stringUtils.isEmpty(firstName)) notEmptyParameters--;
        if (stringUtils.isEmpty(lastName)) notEmptyParameters--;
        if (stringUtils.isEmpty(job)) notEmptyParameters--;
        if (stringUtils.isEmpty(description)) notEmptyParameters--;
        if (stringUtils.isEmpty(phone)) notEmptyParameters--;

        return notEmptyParameters / allParameters * 100;
    }

    render() {

        const {
            firstName,
            lastName,
            job,
            description,
            createdAt,
            phone,
            pictureURL,
            t
        } = this.props;

        let withUsSince = t('moment.monthLabel', { date: createdAt });

        let pfCompleteness = this.calculatePFCompleteness(
            firstName, lastName, job, description, phone
        );

        return <AccountProfileLayout
            firstName={firstName}
            lastName={lastName}
            job={job}
            description={description}
            withUsFrom={withUsSince}
            pfCompleteness={pfCompleteness}
            pictureURL={pictureURL}
            loading={this.props.loading}
        />
    }
}
const mapStateToProps = state => {
    return {
        emailAddress: state.userDetails.emailAddress,
        firstName: state.userDetails.firstName,
        lastName: state.userDetails.lastName,
        job: state.userDetails.job,
        description: state.userDetails.description,
        loading: state.userDetails.loading,
        createdAt: state.userDetails.createdAt,
        phone: state.userDetails.phone,
        pictureURL: state.userDetails.pictureURL,
    };
};

export default connect(mapStateToProps)(withTranslation()(AccountProfile));