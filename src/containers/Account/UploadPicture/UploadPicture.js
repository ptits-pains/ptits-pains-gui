import React from "react";
import UploadPictureLayout from "../../../components/Navigation/Content/Account/UploadPicture/UploadPicture";
import { storage } from "../../../config/firebase";
import { connect } from 'react-redux';
import SnackBar from '../../../components/UI/SnackBar/SnackBar';
import * as alertConstants from '../../../constants/alerts';
import base from '../../../config/firebase';
import * as userDetailsActions from '../../../store/actions/getUserDetails';

//source : https://github.com/ikramhasib007/react-drawer/blob/image-upload/src/components/ImageUpload.jsx
class UploadPicture extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      image: null,
      progress: null,
      uploading: false,
      error: false,
      errorMessage: null,
      url: this.props.pictureURL
    };
  }

  handlePictureChange = e => {
    let image = e.target.files[0];
    if (image) {
      this.setState({
        image: image
      });
    }
  };

  init = () => {
    this.setState({
      uploading: true,
      error: false,
      errorMessage: null
    })
  }

  handlePictureUpload = () => {

    this.init();
    const image = this.state.image;
    const uploadTask = storage.ref(`images/${this.props.userId}/${image.name}`).put(image);
    uploadTask.on('state_changed',
      (snapshot) => {
        // progress function 
        const progress = Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
        this.setState({
          progress: progress
        });
      },
      (error) => {
        // error function ....
        console.log(error);
        this.setState({
          uploading: false,
          error: true,
          errorMessage: error.message
        });
      },
      () => {
        // complete function ....
        storage.ref('images').child(this.props.userId).child(image.name).getDownloadURL().then(url => {
          //update user picture location
          this.setState({ progress: null })
          if (url) this.updateUserPictureURL(url);
          else this.setState({ uploading: false });
        })
      });
  };

  handlePictureDelete = () => {

    this.init();
    var desertRef = storage.refFromURL(this.state.url);
    desertRef.delete().then(() => {
      // File deleted successfully
      this.updateUserPictureURL(null);
    }).catch((error) => {
      // Uh-oh, an error occurred!
      this.setState({
        uploading: false,
        error: true,
        errorMessage: error.message
      });
    });
  }

  updateUserPictureURL = (url) => {

    base.update(`users/${this.props.userId}`, {
      data: {
        pictureURL: url,
      }
    }).then(() => {
      this.setState({ uploading: false, url: url });
    }).then(() => {
      //update store
      this.props.getDetails(this.props.userId);
    }).catch(err => {
      this.setState({ uploading: false, error: true, errorMessage: err.message })
    });
  }

  render() {

    return (
      <React.Fragment>
        <UploadPictureLayout
          progress={this.state.progress}
          uploading={this.state.uploading}
          image={this.state.image}
          url={this.state.url}
          onPictureChange={this.handlePictureChange}
          onPictureUpload={this.handlePictureUpload}
          onPictureDelete={this.handlePictureDelete}
        />
        {this.state.error ? <SnackBar type={alertConstants.ERROR} message={this.state.errorMessage} /> : null}
      </React.Fragment>
    )
  }
}

const mapStateToProps = state => {
  return {
    userId: state.auth.userId,
    pictureURL: state.userDetails.pictureURL
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getDetails: (userId) => dispatch(userDetailsActions.process(userId))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(UploadPicture);
