import React, { Component } from "react";
import AccountDetailsLayout from "../../../components/Navigation/Content/Account/AccountDetails/AccountDetails";
import { connect } from 'react-redux';
import * as userDetailsActions from '../../../store/actions/getUserDetails';
import base from '../../../config/firebase';
import * as alertConstants from '../../../constants/alerts';
import SnackBar from '../../../components/UI/SnackBar/SnackBar';
import { withTranslation } from 'react-i18next';
import * as objectUtils from "../../../utils/ObjectUtils";
class AccountDetails extends Component {

    constructor(props) {
        super(props);
        let {
            firstName,
            lastName,
            initials,
            job,
            phone,
            description
        } = this.props;

        this.state = {
            firstName: firstName,
            lastName: lastName,
            initials: initials,
            job: job,
            phone: phone,
            description: description,
            alert: null,
            alertMessage: null,
            loading: false

        }
    }

    handleFirstNameChange = (event) => {
        this.setState({
            firstName: event.target.value.toLocaleLowerCase()
        })
    }

    handleLastNameChange = (event) => {
        this.setState({
            lastName: event.target.value.toUpperCase()
        })
    }

    handleInitialsChange = (event) => {
        this.setState({
            initials: event.target.value.toUpperCase()
        })
    }

    handleJobChange = (event) => {
        this.setState({
            job: event.target.value
        })
    }

    handlePhoneChange = (event) => {
        this.setState({
            phone: event.target.value
        })
    }

    handleDescriptionChange = (event) => {
        this.setState({
            description: event.target.value
        })
    }

    submit = () => {
        this.setState({ loading: true, alert: null, alertMessage: null });
        base.update(`users/${this.props.userId}`, {
            data: objectUtils.setToEmptyIfNull(
                {
                    firstName: this.state.firstName,
                    lastName: this.state.lastName,
                    initials: this.state.initials,
                    phone: this.state.phone,
                    job: this.state.job,
                    description: this.state.description
                }
            )
        }).then(() => {
            this.setState({ loading: false });
            this.setState({ alert: alertConstants.SUCCESS, alertMessage: this.t('account.accountDetails.successAlert') })
            //update store
            this.props.getDetails(this.props.userId);
        }).catch(err => {
            this.setState({ loading: false });
            this.setState({ alert: alertConstants.ERROR, alertMessage: err.message })
        });
    }

    render() {

        const { t } = this.props;
        this.t = t;

        return (
            <React.Fragment>
                <AccountDetailsLayout
                    firstName={this.state.firstName}
                    lastName={this.state.lastName}
                    initials={this.state.initials}
                    job={this.state.job}
                    phone={this.state.phone}
                    description={this.state.description}
                    loading={this.state.loading || this.props.loading}
                    onFirstNameChange={this.handleFirstNameChange}
                    onLastNameChange={this.handleLastNameChange}
                    onInitialsChange={this.handleInitialsChange}
                    onJobChange={this.handleJobChange}
                    onPhoneChange={this.handlePhoneChange}
                    onDescriptionChange={this.handleDescriptionChange}
                    onSubmit={this.submit}
                />
                {this.state.alert ? <SnackBar type={this.state.alert} message={this.state.alertMessage} /> : null}
            </React.Fragment>
        )
    }
}
const mapStateToProps = state => {
    return {
        userId: state.auth.userId,
        emailAddress: state.userDetails.emailAddress,
        firstName: state.userDetails.firstName,
        lastName: state.userDetails.lastName,
        initials: state.userDetails.initials,
        job: state.userDetails.job,
        phone: state.userDetails.phone,
        description: state.userDetails.description,
        loading: state.userDetails.loading,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        getDetails: (userId) => dispatch(userDetailsActions.process(userId))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(withTranslation()(AccountDetails));