import React from 'react';
import { Grid } from '@material-ui/core';
import AccountProfile from "./AccountProfile/AccountProfile";
import AccountDetails from "./AccountDetails/AccountDetails";
import UploadPicture from "./UploadPicture/UploadPicture";
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import * as appRoutes from '../../constants/routes';


/**
 * The user account container
 * source : https://react-material-dashboard.devias.io/account
 * github: https://github.com/devias-io/react-material-dashboard/tree/master/src/views/Account
 */
class Account extends React.Component {

  render() {
    if (!this.props.userId) {
      return <Redirect to={appRoutes.ROOT} />
    }
    return (
      <Grid
        container
        spacing={4}
      >
        <Grid
          item
          lg={4}
          md={6}
          xl={4}
          xs={12}
        >
          <AccountProfile />
        </Grid>
        <Grid
          item
          lg={4}
          md={6}
          xl={4}
          xs={12}
        >
          <UploadPicture />
        </Grid>
        <Grid
          item
          lg={8}
          md={6}
          xl={8}
          xs={12}
        >
          <AccountDetails/>
        </Grid>
      </Grid>
    );
  }
};

const mapStateToProps = state => {
  return {
    userId: state.auth.userId
  };
};

export default connect(mapStateToProps)(Account);