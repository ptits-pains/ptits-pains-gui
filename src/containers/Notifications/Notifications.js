import React from "react";
import base from '../../config/firebase';
import { connect } from 'react-redux';
import CircularProgress from '@material-ui/core/CircularProgress';
import Notification from "./Notification/Notification";
import { Grid } from '@material-ui/core';
import { Redirect } from 'react-router-dom';
import * as appRoutes from '../../constants/routes';
import * as alertConstants from '../../constants/alerts';
import SnackBar from '../../components/UI/SnackBar/SnackBar';
import * as stateConstants from "../../constants/state";

class Notifications extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            notifications: null,
            loading: false,
            error: false,
            errorMessage: null,
        }
    }

    componentDidMount() {
        // get all notifications
        const {
            userId
        } = this.props;

        if (userId) {
            this.setState({
                loading: true
            })
            base.fetch(`notifications/${userId}`, {
                context: this,
                asArray: true
            }).then(data => {
                this.setState({
                    notifications: data,
                    loading: false
                })
            }).catch(err => {
                this.setState({
                    error: true,
                    errorMessage: err.message
                })
            })
        }
    }

    handleDeleteNotification = (notificationId) => {
        let notifications = [...this.state.notifications];
        notifications = notifications.filter(notification => notification.key !== notificationId);
        this.setState({
            notifications: notifications
        })
    }

    /**
     * when user accept one notification we gonna automatically refuse notifications that concerns the same date
     */
    handleAcceptNotification = (notificationId) => {

        let notifications = [...this.state.notifications];
        //update state
        notifications.forEach(notification => {
            if (notification.key !== notificationId) {
                notification.state = stateConstants.DISABLED;
            }
        })
        this.setState({ notifications: notifications })
    }

    render() {

        if (!this.props.userId) {
            return <Redirect to={appRoutes.ROOT} />
        }

        if (this.state.loading || !this.state.notifications) {
            return <CircularProgress />;
        }

        return (
            <React.Fragment>
                <Grid
                    container
                    spacing={4}
                >
                    {this.state.notifications.map(notification =>
                        <Grid
                            key={notification.key}
                            item
                            lg={4}
                            md={6}
                            xl={4}
                            xs={12}
                        >
                            <Notification
                                notificationId={notification.key}
                                userId={this.props.userId}
                                myDay={notification.myDay}
                                collegeDay={notification.collegeDay}
                                collegeID={notification.collegeID}
                                state={notification.state}  //state is accepted or refused
                                onDeleteNotification={this.handleDeleteNotification}
                                onAcceptNotification={this.handleAcceptNotification}
                            />
                        </Grid>
                    )}
                </Grid>
                {this.state.error ? <SnackBar type={alertConstants.ERROR} message={this.state.errorMessage} /> : null}
            </React.Fragment>
        )
    }
}

const mapStateToProps = state => {
    return {
        userId: state.auth.userId
    };
};

export default connect(mapStateToProps)(Notifications);