import React from "react";
import NotificationLayout from "../../../components/Navigation/Content/Notification/Notification";
import base from '../../../config/firebase';
import * as stateConstants from "../../../constants/state";
import * as alertConstants from '../../../constants/alerts';
import SnackBar from '../../../components/UI/SnackBar/SnackBar';
import { withTranslation } from 'react-i18next';

class Notification extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            firstName: null,
            lastName: null,
            photoUrl: null,
            loading: false,
            error: false,
            errorMessage: null,
            state: this.props.state
        }
    }

    componentDidMount() {

        this.setState({
            loading: true
        })
        base.fetch(`users/${this.props.collegeID}`, {
            context: this
        }).then(data => {
            this.setState({
                firstName: data.firstName,
                lastName: data.lastName,
                photoUrl: data.pictureURL,
                loading: false
            })
        }).catch(err => {
            this.setError(err.message)
        })
    }

    componentDidUpdate(prevProps) {
        if (this.props.state !== prevProps.state) {
            this.setState({ state: this.props.state })
        }
    }

    handleAcceptClick = () => {
        const {
            userId,
            notificationId,
            myDay
        } = this.props;

        this.init();
        base.update(`notifications/${userId}/${notificationId}`, {
            data: {
                state: stateConstants.ACCEPTED,
            }
        }).then(() => {
            this.setState({ state: stateConstants.ACCEPTED });
        }).then(() => {
            return this.props.onAcceptNotification(notificationId);
        }).then(() => {
            this.updateRequestState(stateConstants.ACCEPTED);
        }).catch(err => {
            this.setError(err.message)
        });
    }

    handleRefuseClick = () => {
        this.init();
        base.update(`notifications/${this.props.userId}/${this.props.notificationId}`, {
            data: {
                state: stateConstants.REFUSED,
            }
        }).then(() => {
            this.setState({ state: stateConstants.REFUSED });
        }).then(() => {
            this.updateRequestState(stateConstants.REFUSED)
        }).catch(err => {
            this.setError(err.message)
        });
    }

    handleDeleteClick = () => {
        this.init();
        base.remove(`notifications/${this.props.userId}/${this.props.notificationId}`)
            .then(() => {

                // update the college request
                this.updateRequestState(stateConstants.DELETED)
            }).then(() => {

                //update the parent state
                this.props.onDeleteNotification(this.props.notificationId)
            })
            .catch(error => {
                this.setError(error.message)
            });
    }

    /**
     * this will update the college request so he can know the state of his request
     */
    updateRequestState = (state) => {
        base.update(`requests/${this.props.collegeID}/${this.props.notificationId}`, {
            data: {
                state: state
            }
        }).then(() => {
            this.stopLoading();
        })
            .catch(error => {
                this.setError(error.message)
            });
    }

    init = () => {
        this.setState({
            loading: true,
            error: false,
            errorMessage: null
        })
    }

    stopLoading = () => {
        this.setState({
            loading: false
        })
    }

    setError = (message) => {
        this.setState({
            loading: false,
            error: true,
            errorMessage: message
        })
    }

    render() {

        const { t, myDay, collegeDay } = this.props;

        return (<React.Fragment>
            <NotificationLayout
                firstName={this.state.firstName}
                lastName={this.state.lastName}
                photoUrl={this.state.photoUrl}
                myDay={t('moment.fullDate', { date: myDay })}
                collegeDay={t('moment.fullDate', { date: collegeDay })}
                loading={this.state.loading}
                state={this.state.state}
                onAcceptClick={this.handleAcceptClick}
                onRefuseClick={this.handleRefuseClick}
                onDeleteClick={this.handleDeleteClick}
            />
            {this.state.error ? <SnackBar type={alertConstants.ERROR} message={this.state.errorMessage} /> : null}
        </React.Fragment>)
    }

}

export default withTranslation()(Notification);