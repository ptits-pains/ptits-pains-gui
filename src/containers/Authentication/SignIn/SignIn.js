import React from 'react';
import SignInUI from '../../../components/Navigation/SignIn/SignIn';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import * as authActions from '../../../store/actions/authentication';
import * as appRoutes from '../../../constants/routes';
import * as userDetailsActions from '../../../store/actions/getUserDetails';

class SignIn extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            email: "",
            password: ""
        };
    }

    handleEmailChange = event => {
        this.setState({ email: event.target.value });
    };

    handlePasswordChange = event => {
        this.setState({ password: event.target.value });
    };

    handleSubmit = event => {
        event.preventDefault();
        this.props.authenticate(this.state.email, this.state.password);
    };

    render() {
        if (this.props.userId) {

            //because of this is the first component to load we gonna update the user details store here
            this.props.getDetails(this.props.userId)
            // redirect to the dashboard
            return <Redirect to={appRoutes.DASH_BOARD} />;
        }
        return (
            <React.Fragment>
                <SignInUI
                    handleSubmit={this.handleSubmit}
                    handleEmailChange={this.handleEmailChange}
                    handlePasswordChange={this.handlePasswordChange}
                    loading={this.props.loading}
                    error={this.props.error}
                />
            </React.Fragment>
        )
    }
}

const mapStateToProps = state => {
    return {
        userId: state.auth.userId,
        loading: state.auth.loading,
        error: state.auth.error
    };
};

const mapDispatchToProps = dispatch => {
    return {
        authenticate: (email, password) => dispatch(authActions.authenticate(email, password)),
        getDetails: (userId) => dispatch(userDetailsActions.process(userId))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(SignIn);