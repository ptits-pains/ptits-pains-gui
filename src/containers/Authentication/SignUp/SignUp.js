import React from 'react';
import SignUpUI from '../../../components/Navigation/SignUp/SignUp';
import * as stringUtils from '../../../utils/StringUtils';
import * as errorUtils from '../../../utils/ErrorUtils';
import * as emailUtils from '../../../utils/EmailUtils';
import base, { fireApp } from '../../../config/firebase';
import 'firebase/auth';
import * as labelUtils from '../../../utils/LabelUtils';
import * as alertConstants from '../../../constants/alerts';
import SnackBar from '../../../components/UI/SnackBar/SnackBar';
import { withTranslation } from 'react-i18next';
class SignUp extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            firstName: "",
            lastName: "",
            emailAddress: "",
            password: "",
            confirmPassword: "",
            alert: null,
            alertMessage: null,
            loading: false
        };
    }

    /**
     * init the error
     */
    initError = () => {

        this.setState({ alert: null, alertMessage: null })
    }

    handleFirstNameChange = event => {
        this.setState({ firstName: event.target.value });
    };

    /**
     * in this function we will build an error object to be used in the dynamic text field
    */
    firstNameErrorBuild = () => {

        //check if required + check  if it's is greater than 3
        return errorUtils.buildError([
            this.t('formErrorLabels.requiredField'),
            stringUtils.isEmpty(this.state.firstName),
            this.t('formErrorLabels.minLength2'),
            stringUtils.doesNotHasMinSize(this.state.firstName, 2)
        ]);
    }

    handleLastNameChange = event => {
        this.setState({ lastName: event.target.value });
    };

    lastNameErrorBuild = () => {

        return errorUtils.buildError([
            this.t('formErrorLabels.requiredField'),
            stringUtils.isEmpty(this.state.lastName),
            this.t('formErrorLabels.minLength2'),
            stringUtils.doesNotHasMinSize(this.state.lastName, 2)
        ]);
    }

    handleEmailAddressChange = event => {
        this.setState({ emailAddress: event.target.value });
    };

    emailErrorBuild = () => {

        return errorUtils.buildError([
            this.t('formErrorLabels.requiredField'),
            stringUtils.isEmpty(this.state.emailAddress),
            this.t('formErrorLabels.validEmail'),
            !emailUtils.validateEmail(this.state.emailAddress)
        ]);
    }

    handlePasswordChange = event => {
        this.setState({ password: event.target.value });
    };

    passwordErrorBuild = () => {

        return errorUtils.buildError([
            this.t('formErrorLabels.requiredField'),
            stringUtils.isEmpty(this.state.password),
            this.t('formErrorLabels.minLength8'),
            stringUtils.doesNotHasMinSize(this.state.password, 8)
        ]);
    }

    handleConfirmPasswordChange = event => {
        this.setState({ confirmPassword: event.target.value });
    };

    confirmPasswordErrorBuild = () => {

        return errorUtils.buildError([
            this.t('formErrorLabels.requiredField'),
            stringUtils.isEmpty(this.state.confirmPassword),
            this.t('formErrorLabels.confirmPassword'),
            !stringUtils.isEqual(this.state.password, this.state.confirmPassword)
        ]);
    }

    /**
     * use firebase to signup
     */
    handleSubmit = event => {

        this.initError();
        event.preventDefault();
        let isNotValid = stringUtils.isEmpty(this.state.firstName) || stringUtils.doesNotHasMinSize(this.state.firstName, 2) ||
            stringUtils.isEmpty(this.state.lastName) || stringUtils.doesNotHasMinSize(this.state.lastName, 2) ||
            stringUtils.isEmpty(this.state.emailAddress) || !emailUtils.validateEmail(this.state.emailAddress) ||
            stringUtils.isEmpty(this.state.password) || stringUtils.doesNotHasMinSize(this.state.password, 8) ||
            stringUtils.isEmpty(this.state.confirmPassword) || !stringUtils.isEqual(this.state.password, this.state.confirmPassword);

        if (!isNotValid) {

            this.setState({ loading: true });
            fireApp
                .auth()
                .createUserWithEmailAndPassword(this.state.emailAddress, this.state.password)
                .then((data) => {
                    //create a user account
                    base.post(`users/${data.user.uid}`, {
                        data: {
                            firstName: this.state.firstName.toLocaleLowerCase(),
                            lastName: this.state.lastName.toUpperCase(),
                            emailAddress: this.state.emailAddress,
                            initials: labelUtils.getInitials(this.state.firstName, this.state.lastName)
                        }
                    }).then(() => {
                        this.setState({ loading: false });
                        this.setState({ alert: alertConstants.SUCCESS, alertMessage: this.t('signUp.success') })
                    }).catch(err => {
                        this.setState({ loading: false });
                        this.setState({ alert: alertConstants.ERROR, alertMessage: err.message })
                    });
                }).catch((error) => {

                    this.setState({ loading: false });
                    this.setState({ alert: alertConstants.ERROR, alertMessage: error.message })
                })
        }
    }

    render() {

        const { t } = this.props;
        this.t = t;

        return (
            <React.Fragment>
                <SignUpUI
                    handleFirstNameChange={this.handleFirstNameChange}
                    firstNameError={this.firstNameErrorBuild()}
                    handleLastNameChange={this.handleLastNameChange}
                    lastNameError={this.lastNameErrorBuild()}
                    handleEmailAddressChange={this.handleEmailAddressChange}
                    emailError={this.emailErrorBuild()}
                    handlePasswordChange={this.handlePasswordChange}
                    passwordError={this.passwordErrorBuild()}
                    handleConfirmPasswordChange={this.handleConfirmPasswordChange}
                    confirmPasswordError={this.confirmPasswordErrorBuild()}
                    handleSubmit={this.handleSubmit}
                    loading={this.state.loading}
                />
                {this.state.alert ?
                    <SnackBar type={this.state.alert} message={this.state.alertMessage} />
                    : null}
            </React.Fragment>
        )
    }
}

export default withTranslation()(SignUp);