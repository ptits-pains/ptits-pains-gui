import React from 'react';
import {Redirect} from 'react-router-dom';
import { connect } from 'react-redux';
import * as authActions from '../../../store/actions/authentication';

class LogOut extends React.Component{

    componentDidMount(){
        this.props.logout();
    }

    render(){

        return (
            <Redirect to="/"/>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        logout: () => dispatch(authActions.logout())
    };
};

export default connect(null, mapDispatchToProps)(LogOut);