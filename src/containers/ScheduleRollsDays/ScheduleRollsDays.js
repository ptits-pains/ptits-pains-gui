import React from 'react';
import base from '../../config/firebase';
import SnackBar from '../../components/UI/SnackBar/SnackBar';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import * as appRoutes from '../../constants/routes';
import * as alertConstants from '../../constants/alerts';
import CircularProgress from '@material-ui/core/CircularProgress';
import { Grid } from '@material-ui/core';
import UserCard from "./UserCard/UserCard";

class ScheduleRollsDays extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            loading: true,
            usersData: null,
            error: false,
            errorMessage: null
        }
    }


    /**
     * get all users data
     */
    componentDidMount() {

        this.init();
        this.initStore();
    }

    initStore = () => {
        Promise.all([
            base.fetch('rollsDinner', { context: this, asArray: true }),
            base.fetch(`users`, { context: this })
        ])
            .then((values) => {

                let rollsDinners = values[0];
                let users = values[1];
                let usersData = [];
                rollsDinners.forEach(user => {

                    usersData.push({
                        ...user,
                        firstName: users[user.userId].firstName,
                        lastName: users[user.userId].lastName,
                        isAdmin: users[user.userId].isAdmin,
                    })
                });
                this.setState({
                    loading: false,
                    usersData: usersData
                })
            })
            .catch(error => {
                this.initError(error)
            })
    }

    /**
     * this method will be called if we have a conflict between users data and the admin forces the user dinner rolls day
     * the child component (UserCard) will send the user id to update
     */
    handleUpdateUsersData = (userId) => {

        this.initStore();
    }

    initError = (error) => {
        this.setState({
            loading: false,
            error: true,
            errorMessage: error.message
        })
    }

    init = () => {
        this.setState({
            loading: true,
            error: false,
            errorMessage: null
        })
    }

    render() {

        if (!this.props.userId) {
            return <Redirect to={appRoutes.ROOT} />
        }
        if (!this.state.usersData) {
            return <CircularProgress />;
        }

        return (
            <React.Fragment>
                <Grid
                    container
                    spacing={4}
                >
                    {this.state.usersData.map(user =>
                        <Grid
                            key={user.key}
                            item
                            md={6}
                            xs={12}
                        >
                            <UserCard
                                userId={user.userId}
                                currentUserId={this.props.userId}
                                firstName={user.firstName}
                                lastName={user.lastName}
                                isAdmin={user.isAdmin}
                                nextDate={user.nextDate}
                                onUpdateUsersData={this.handleUpdateUsersData}
                            />
                        </Grid>
                    )}
                </Grid>
                {this.state.error ? <SnackBar type={alertConstants.ERROR} message={this.state.errorMessage} /> : null}
            </React.Fragment>
        )
    }
}

const mapStateToProps = state => {
    return {
        userId: state.auth.userId
    };
};

export default connect(mapStateToProps)(ScheduleRollsDays);