import React from "react";
import base from '../../../config/firebase';
import SnackBar from '../../../components/UI/SnackBar/SnackBar';
import UserCardLayout from "../../../components/Navigation/Content/ScheduleRollsDays/UserCard/UserCard";
import * as alertConstants from '../../../constants/alerts';
import * as dateUtils from "../../../utils/DateUtils";
import { withTranslation } from 'react-i18next';

class UserCard extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            nextDate: props.nextDate,
            showDialog: false,
            loading: false,
            alertType: false,
            alertMessage: null
        }
        this.conflictedUserId = null;   // this variable will holds the used id in case of conflict dates
        this.conflictedUserDisplayName = null;
        this.prevNextDate = props.nextDate; //  this will store the user next date before modification
    }

    componentDidUpdate(prevProps) {
        if (this.props.nextDate !== prevProps.nextDate) {
            this.setState({ nextDate: this.props.nextDate });
            this.prevNextDate = this.props.nextDate;
        }
    }

    handleNextDateChange = (date) => {

        this.setState({
            nextDate: dateUtils.jsDateToString(date)
        })
    }

    handleSubmitClick = () => {

        this.init();
        let date = this.state.nextDate;
        const monthLabel = date.substring(0, 7);
        base
            .fetch(`rollsDinnerMonth/${monthLabel}/${date}`, { context: this })
            .then(response => {
                if (response && response.userId) {
                    this.resolveConflict(response.userId);
                } else {
                    this.continueUpdating();
                }
            }).catch(err => {
                this.setError(err.message)
            });
    }

    resolveConflict = (userId) => {

        //we gonna store the user id cause the conflict
        this.conflictedUserId = userId;

        //if there is a conflict we gonna first get the conflicted user data to show them in a dialog
        base
            .fetch(`users/${this.conflictedUserId}`, { context: this })
            .then(response => {
                //we gonna store the conflicted user name
                this.conflictedUserDisplayName = response.firstName + " " + response.lastName;

                //well there is a conflict, we gonna show the dialog to get the admin decision
                this.setState({
                    loading: false,
                    showDialog: true
                })
            }).catch(err => {
                this.setError(err.message)
            });
    }

    /**
     * if there is no dates conflict we will execute this method
     */
    continueUpdating = () => {

        this.updateRollsDay()
            .then(() => {
                this.stopLoading();
            })
            .then(() => {
                this.setSuccess();
            })
            .catch(err => {
                this.setError(err.message)
            });
    }

    /**
     * this function will be called to update the dinner rolls day of the chosen user
     */
    updateRollsDay = () => {

        let date = this.state.nextDate;
        let monthLabel = date.substring(0, 7);
        let prevMonthLabel = this.prevNextDate ? this.prevNextDate.substring(0, 7) : null;

        const {
            userId
        } = this.props;

        return Promise.all([
            base.post(`rollsDinnerMonth/${monthLabel}/${date}`, {
                data: {
                    userId: userId,
                }
            }),
            base.update(`rollsDinner/${userId}`, {
                data: {
                    nextDate: date
                }
            }),
            base.remove(`rollsDinnerMonth/${prevMonthLabel}/${this.prevNextDate}`)
        ]).then(() => {

            this.prevNextDate = "" + this.state.nextDate;
        })
    }

    /**
     * this function will be executed if the admin accepts to force the updating of a dinner rolls date even if there is
     * someone (user who will brings the dinner rolls) on that date
     */
    handleAcceptOverwriteRollsDay = () => {

        const {
            onUpdateUsersData
        } = this.props;

        this.updateRollsDay()
            .then(() => {
                return base.update(`rollsDinner/${this.conflictedUserId}`, {
                    data: {
                        nextDate: null,
                    }
                })
            })
            .then(() => {
                onUpdateUsersData(this.conflictedUserId);
            })
            .then(() => {

                this.stopLoading();
            })
            .then(() => {

                this.setSuccess();
            }).catch(err => {
                this.setError(err.message)
            });
    }

    init = () => {
        this.setState({
            loading: true,
            alertType: false,
            alertMessage: null,
            showDialog: false
        })
    }

    stopLoading = () => {
        this.setState({
            loading: false
        })
    }

    setError = (message) => {
        this.setState({
            loading: false,
            alertType: alertConstants.ERROR,
            alertMessage: message
        })
    }

    setSuccess = () => {
        this.setState({
            loading: false,
            alertType: alertConstants.SUCCESS,
            alertMessage: this.props.t('common.successAlert')
        })
    }

    render() {

        const {
            firstName,
            lastName,
            userId,
            isAdmin,
            currentUserId
        } = this.props;
        return (<React.Fragment>
            <UserCardLayout
                firstName={firstName}
                lastName={lastName}
                isAdmin={isAdmin}
                isCurrentUser={userId === currentUserId}
                nextDate={dateUtils.stringToJsDate(this.state.nextDate)}
                disableSubmit={!this.state.nextDate || this.state.nextDate === this.prevNextDate}
                loading={this.state.loading}
                showDialog={this.state.showDialog}  // will be true if there is a dates conflict
                conflictedUserName={this.conflictedUserDisplayName}
                onNextDateChange={this.handleNextDateChange}
                onAcceptOverwriteCollegeDay={this.handleAcceptOverwriteRollsDay}
                onSubmitClick={this.handleSubmitClick}
            />
            {this.state.alertType ? <SnackBar type={this.state.alertType} message={this.state.alertMessage} /> : null}
        </React.Fragment>)
    }

}

export default withTranslation()(UserCard);