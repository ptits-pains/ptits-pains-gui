import React from "react";
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import * as appRoutes from '../../constants/routes';
import Password from "./Password/Password";
import EmailAddress from "./EmailAddress/EmailAddress";
import DeleteAccount from "./DeleteAccount/DeleteAccount";
import EditLanguage from "./EditLanguage/EditLanguage";
import { Grid } from '@material-ui/core';
import * as userDetailsActions from '../../store/actions/getUserDetails';

class AccountSettings extends React.Component {

    handleUpdateUserProfileStore = () => {
        this.props.getDetails(
            this.props.userId
        )
    }

    render() {
        if (!this.props.userId) {
            return <Redirect to={appRoutes.ROOT} />
        }
        return (
            <Grid
                container
                spacing={4}
            >
                <Grid
                    item
                    lg={4}
                    md={6}
                    xl={4}
                    xs={12}
                >
                    <Password />
                </Grid>
                <Grid
                    item
                    lg={4}
                    md={6}
                    xl={4}
                    xs={12}
                >
                    <EmailAddress
                        userId={this.props.userId}
                        email={this.props.email}
                        onUpdateUserProfileStore={this.handleUpdateUserProfileStore}
                    />
                </Grid>
                <Grid
                    item
                    lg={8}
                    md={6}
                    xl={8}
                    xs={12}
                >
                    <EditLanguage />
                </Grid>
                <Grid
                    item
                    lg={8}
                    md={6}
                    xl={8}
                    xs={12}
                >
                    <DeleteAccount
                        userId={this.props.userId}
                        onUpdateUserProfileStore={this.handleUpdateUserProfileStore}
                    />
                </Grid>
            </Grid >
        );
    }
}

const mapStateToProps = state => {
    return {
        userId: state.auth.userId,
        email: state.userDetails.emailAddress
    };
};

const mapDispatchToProps = dispatch => {
    return {
        getDetails: (userId) => dispatch(userDetailsActions.process(userId))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(AccountSettings);