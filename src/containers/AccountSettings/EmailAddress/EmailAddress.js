import React from "react";
import EmailAddressLayout from "../../../components/Navigation/Content/AccountSettings/EmailAddress/EmailAddress";
import base from '../../../config/firebase';
import SnackBar from '../../../components/UI/SnackBar/SnackBar';
import * as alertConstants from '../../../constants/alerts';
import * as emailUtils from '../../../utils/EmailUtils';
import { fireApp } from '../../../config/firebase';
import 'firebase/auth';
import { withTranslation } from 'react-i18next';

class EmailAddress extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            alert: null,
            alertMessage: null,
            email: this.props.email
        };
    }

    handleEmailChange = event => {
        this.setState({
            email: event.target.value
        })
    }

    handleSubmit = () => {
        this.setState({
            loading: true,
            alert: null,
            alertMessage: null
        })
        var user = fireApp.auth().currentUser;

        user.updateEmail(this.state.email)
            .then(() => {
                // Update successful.
                return base.update(`users/${this.props.userId}`, {
                    data: {
                        emailAddress: this.state.email
                    }
                })
            }).then(() => {
                this.setState({
                    loading: false,
                    alert: alertConstants.SUCCESS,
                    alertMessage: this.t('accountSettings.email.successAlert')
                });
            }).then(() => {
                //update store
                this.props.onUpdateUserProfileStore();
            }).catch(err => {
                this.setState({ loading: false, alert: alertConstants.ERROR, alertMessage: err.message });
            });
    }

    render() {

        const { t } = this.props;
        this.t = t;

        let isNotValidEmail = null;
        if (!emailUtils.validateEmail(this.state.email)) {
            isNotValidEmail = t('formErrorLabels.validEmail');
        }
        return (<React.Fragment>
            <EmailAddressLayout
                email={this.state.email}
                loading={this.state.loading}
                onEmailChange={this.handleEmailChange}
                onSubmit={this.handleSubmit}
                isNotValidEmail={isNotValidEmail}
            />
            {this.state.alert ? <SnackBar type={this.state.alert} message={this.state.alertMessage} /> : null}
        </React.Fragment>
        )

    }
}

export default withTranslation()(EmailAddress);