import React from "react";
import PasswordLayout from "../../../components/Navigation/Content/AccountSettings/Password/Password";
import { fireApp } from '../../../config/firebase';
import 'firebase/auth';
import SnackBar from '../../../components/UI/SnackBar/SnackBar';
import * as alertConstants from '../../../constants/alerts';
import * as stringUtils from "../../../utils/StringUtils";
import { withTranslation } from 'react-i18next';

class Password extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            alert: null,
            alertMessage: null,
            password: "",
            confirmPassword: ""
        };
    }

    handlePasswordChange = event => {
        this.setState({
            password: event.target.value
        })
    }

    handleConfirmPasswordChange = event => {
        this.setState({
            confirmPassword: event.target.value
        })
    }

    handleSubmitPassword = () => {
        this.setState({
            loading: true,
            alert: null,
            alertMessage: null
        })
        var user = fireApp.auth().currentUser;
        user.updatePassword(this.state.password).then(() => {
            // Update successful.
            this.setState({
                loading: false,
                alert: alertConstants.SUCCESS,
                alertMessage: this.t('accountSettings.password.successAlert')
            });
        }).catch((error) => {
            // An error happened.
            this.setState({ loading: false, alert: alertConstants.ERROR, alertMessage: error.message });
        });
    }

    render() {

        const { t } = this.props;
        this.t = t;

        let passwordError = null, confirmPasswordError = null;
        if (stringUtils.isEmpty(this.state.password) || stringUtils.doesNotHasMinSize(this.state.password, 8)) {
            passwordError = t('formErrorLabels.minLength8');
        }
        if (stringUtils.isEmpty(this.state.confirmPassword) || !stringUtils.isEqual(this.state.password, this.state.confirmPassword)) {
            confirmPasswordError = t('formErrorLabels.confirmPassword');
        }

        return (<React.Fragment>
            <PasswordLayout
                password={this.state.password}
                confirmPassword={this.state.confirmPassword}
                loading={this.state.loading}
                onPasswordChange={this.handlePasswordChange}
                onConfirmPasswordChange={this.handleConfirmPasswordChange}
                onSubmit={this.handleSubmitPassword}
                passwordError={passwordError}
                confirmPasswordError={confirmPasswordError}
            />
            {this.state.alert ? <SnackBar type={this.state.alert} message={this.state.alertMessage} /> : null}
        </React.Fragment>
        )
    }
}

export default withTranslation()(Password);