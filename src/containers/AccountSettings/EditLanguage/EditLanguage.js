import React from "react";
import EditLanguageLayout from "../../../components/Navigation/Content/AccountSettings/EditLanguage/EditLanguage";
import i18n from '../../../i18n';

class EditLanguage extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            value: "en"
        };
    }

    componentDidMount() {
        this.setState({
            value: i18n.languages[0]
        })
    }

    handleLanguageChange = event => {
        let newlang = event.target.value;
        this.setState({
            value: newlang
        })
        i18n.changeLanguage(newlang);
    };

    render() {

        return <EditLanguageLayout
            value={this.state.value}
            onLanguageChange={this.handleLanguageChange}
        />
    }
}

export default EditLanguage;