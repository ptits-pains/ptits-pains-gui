import React from "react";
import DeleteAccountLayout from "../../../components/Navigation/Content/AccountSettings/DeleteAccount/DeleteAccount";
import base from '../../../config/firebase';
import SnackBar from '../../../components/UI/SnackBar/SnackBar';
import * as alertConstants from '../../../constants/alerts';
import { connect } from 'react-redux';
import { withTranslation } from 'react-i18next';

class DeleteAccount extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            alert: null,
            alertMessage: null,
        };
    }

    handleDeleteAccountClick = () => {

        this.updateAccountState(true, this.t('accountSettings.deleteAccount.deleteAlert'));
    }

    handleCancelAccountClick = () => {

        this.updateAccountState(false,  this.t('accountSettings.deleteAccount.cancelAlert'));
    }

    updateAccountState = (state, message) => {
        this.setState({
            loading: true,
            alert: null,
            alertMessage: null
        })
        base.update(`users/${this.props.userId}`, {
            data: {
                deleteAccount: state
            }
        }).then(() => {
            this.setState({ loading: false, alert: alertConstants.SUCCESS, alertMessage: message });
        }).then(() => {
            //update store
            this.props.onUpdateUserProfileStore();
        }).catch(err => {
            this.setState({ loading: false, alert: alertConstants.ERROR, alertMessage: err.message });
        });
    }

    render() {
        
        const { t } = this.props;
        this.t = t;

        let creationTime = t('moment.fullDateTime', { date:  this.props.createdAt });
        return (<React.Fragment>
            <DeleteAccountLayout
                onDeleteAccount={this.handleDeleteAccountClick}
                onCancelAccount={this.handleCancelAccountClick}
                loading={this.state.loading || this.props.loading}
                toBeDeleted={this.props.deleteAccount}
                creationTime={creationTime}
            />
            {this.state.alert ? <SnackBar type={this.state.alert} message={this.state.alertMessage} /> : null}
        </React.Fragment>
        )
    }
}

const mapStateToProps = state => {
    return {
        deleteAccount: state.userDetails.deleteAccount,
        createdAt: state.userDetails.createdAt,
        loading: state.userDetails.loading
    };
};


export default connect(mapStateToProps)(withTranslation()(DeleteAccount));