import React, { Component } from "react";
import Grid from '@material-ui/core/Grid';
import RollsCardToEdit from './RollsCard/RollsCardToEdit';
import RollsCardToShow from './RollsCard/RollsCardToShow';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import * as appRoutes from '../../../constants/routes';
import base from '../../../config/firebase';
import * as alertConstants from '../../../constants/alerts';
import SnackBar from '../../../components/UI/SnackBar/SnackBar';
import LinearProgress from "../../../components/UI/LinearProgress/LinearProgress";

class RollsList extends Component {

    constructor(props) {
        super(props)
        this.state = {
            loading: false,
            error: false,
            errorMessage: null,
            users: null
        }
    }

    componentDidMount() {

        //we gonna retrieve all users profile data and pass it to the cards
        this.setState({
            loading: true,
            error: false,
            errorMessage: null
        })
        base.fetch(`users`, {
            context: this
        }).then(data => {
            this.setState({
                users: data,
                loading: false
            })
        }).catch(err => {
            this.setState({
                loading: false,
                error: true,
                errorMessage: err.message
            })
        })
    }

    render() {
        if (!this.props.userId) {
            return <Redirect to={appRoutes.ROOT} />
        }
        const {
            rollsMonth
        } = this.props;

        let screenToShow = null;
        if (rollsMonth) {
            screenToShow = [];
            for (const [key, value] of rollsMonth.entries()) {

                if (value.userId === this.props.userId) {
                    //this is the current user he can edit his rolls
                    screenToShow.push(
                        <Grid item xs={12} md={4} key={key}>
                            <RollsCardToEdit
                                userName={value.firstName + " " + value.lastName}
                                products={value.products}
                                rollsDay={key}
                            />
                        </Grid>
                    )
                } else {
                    //another user, we will just show the rolls
                    screenToShow.push(
                        <Grid item xs={12} md={4} key={key}>
                            <RollsCardToShow
                                userName={value.firstName + " " + value.lastName}
                                products={value.products}
                                rollsDay={key}
                                users={this.state.users}
                                userId={this.props.userId}
                                firstName={this.props.firstName}
                                lastName={this.props.lastName}
                            />
                        </Grid>
                    )
                }
            }
        }

        return (
            <React.Fragment>
                {this.state.loading ? <LinearProgress /> :
                    <Grid container spacing={3}>
                        {screenToShow}
                    </Grid>
                }
                {this.state.error ? <SnackBar type={alertConstants.ERROR} message={this.state.errorMessage} /> : null}
            </React.Fragment>
        )
    }
}

const mapStateToProps = state => {
    return {
        userId: state.auth.userId,
        firstName: state.userDetails.firstName,
        lastName: state.userDetails.lastName
    };
};

export default connect(mapStateToProps)(RollsList);