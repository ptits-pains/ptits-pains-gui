import React, { Component } from "react";
import ProductLayout from '../../../../components/Navigation/Content/DashBoard/Product/Product';

class Product extends Component {

    constructor(props) {
        super(props);
        this.state = {
            state: "created",   // created or deleted
            name: this.props.name,
            quantity: this.props.quantity,
            index: this.props.index
        }
    }

    deleteProduct = () => {
        this.props.removeTheProduct(this.state.index);
        this.setState({
            state: "deleted"
        })
    }

    render() {

        let layoutToShow = null;
        if (this.state.state === "created") {
            layoutToShow = (
                <ProductLayout
                    name={this.state.name}
                    quantity={this.state.quantity}
                    addNewProduct={this.props.addNewProduct}
                    deleteProduct={this.deleteProduct}
                    onQuantityChange={(event) => this.props.onQuantityChange(event, this.state.index)}
                    onNameChange={(event) => this.props.onNameChange(event, this.state.index)}
                    cardState={this.props.cardState}
                    lastProduct={this.props.lastProduct}
                />
            )
        }

        return (
            layoutToShow
        )
    }
}

export default Product;