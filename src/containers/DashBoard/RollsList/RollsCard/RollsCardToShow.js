import React, { Component } from "react";
import RollsCardLayout from '../../../../components/Navigation/Content/DashBoard/RollsCard/RollsCardToShow';
import Avatar from '@material-ui/core/Avatar';
import Chip from '@material-ui/core/Chip';
import Box from '@material-ui/core/Box';
import base from '../../../../config/firebase';
import * as alertConstants from '../../../../constants/alerts';
import SnackBar from '../../../../components/UI/SnackBar/SnackBar';
class RollsCardToShow extends Component {

    constructor(props) {
        super(props)
        this.state = {
            loading: false,
            error: false,
            errorMessage: null,
            participant: null,
            usersNames: null
        }
    }

    componentDidMount() {

        this.init();
        let userId = this.props.userId;
        let rollsMonthLabel = this.props.rollsDay.substring(0, 7);
        base.fetch(`rollsDinnerMonth/${rollsMonthLabel}/${this.props.rollsDay}/participants/${userId}`, {
            context: this
        }).then(data => {
            //check if the current user is a participant or not
            if (data.userId) {
                this.setState({
                    participant: true
                })
            } else {
                this.setState({
                    participant: false
                })
            }
        }).then(() => {
            // get the list of participants for this roll day
            return base.fetch(`rollsDinnerMonth/${rollsMonthLabel}/${this.props.rollsDay}/participants`, {
                context: this
            })
        }).then((participants) => {
            // all users profiles data are passed as a prop
            let users = this.props.users;
            let usersNames = [];
            Object.keys(participants).forEach(participant => {
                usersNames.push(
                    {
                        [participant]: users[participant].firstName + " " + users[participant].lastName
                    }
                )
            })
            this.setState({
                loading: false,
                usersNames: usersNames
            })
        }).catch(err => {
            this.setError(err.message)
        })
    }

    handleAddMeToTheList = () => {

        this.init();
        let userId = this.props.userId;
        let rollsMonthLabel = this.props.rollsDay.substring(0, 7);
        base.post(`rollsDinnerMonth/${rollsMonthLabel}/${this.props.rollsDay}/participants/${userId}`, {
            data: {
                userId
            }
        }).then(() => {
            this.setState({
                loading: false,
                participant: true
            })
        }).then(() => {
            let usersNames = [...this.state.usersNames];
            usersNames.push(
                {
                    [userId]: this.props.firstName + " " + this.props.lastName
                }
            )
            this.setState({
                usersNames: usersNames
            })
        }).catch(err => {
            this.setError(err.message)
        });
    }

    handleRemoveMeFromTheList = () => {

        this.init();
        let userId = this.props.userId;
        let rollsMonthLabel = this.props.rollsDay.substring(0, 7);
        base.remove(`rollsDinnerMonth/${rollsMonthLabel}/${this.props.rollsDay}/participants/${userId}`)
            .then(() => {
                // let usersNames = [...this.state.usersNames];
                // usersNames.filter(userName => )
                this.setState({
                    loading: false,
                    participant: false
                })
            })
            .then(() => {
                let usersNames = [...this.state.usersNames];
                usersNames = usersNames.filter(userName => Object.keys(userName)[0] !== userId)
                this.setState({
                    usersNames: usersNames
                })
            }).catch(err => {
                this.setError(err.message)
            });
    }

    init = () => {
        this.setState({
            loading: true,
            error: false,
            errorMessage: null
        })
    }

    setError = (message) => {
        this.setState({
            loading: false,
            error: true,
            errorMessage: message
        })
    }

    render() {

        // init the list of products to be show in the card
        let screen = null;
        if (this.props.products && this.props.products.length > 0) {
            screen = (
                this.props.products.map((pdt, index) => <Box m={1} key={index}>
                    <Chip avatar={<Avatar>{pdt.quantity}</Avatar>} label={pdt.name ? pdt.name.substring(0, 20) : ""} color="primary" variant="outlined" />
                </Box>
                )
            )
        }
        return (
            <React.Fragment>
                <RollsCardLayout
                    userName={this.props.userName}
                    rollsDay={this.props.rollsDay}
                    participant={this.state.participant}
                    usersNames={this.state.usersNames}
                    loading={this.state.loading}
                    onAddMeToTheList={this.handleAddMeToTheList}
                    onRemoveMeFromTheList={this.handleRemoveMeFromTheList}
                >
                    {screen}
                </RollsCardLayout>
                {this.state.error ? <SnackBar type={alertConstants.ERROR} message={this.state.errorMessage} /> : null}
            </React.Fragment>
        )
    }
}

export default RollsCardToShow;