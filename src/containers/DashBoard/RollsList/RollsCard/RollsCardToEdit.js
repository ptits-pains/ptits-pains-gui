import React, { Component } from "react";
import RollsCardLayout from '../../../../components/Navigation/Content/DashBoard/RollsCard/RollsCardToEdit';
import Product from '../Product/Product';
import * as arrayUtils from '../../../../utils/ArrayUtils';
import base from '../../../../config/firebase';

export const CREATE_CARD = "CREATED";
export const SAVING_CARD = "SAVING";
export const SAVED_CARD = "SAVED";
export const ERROR_SAVING_CARD = "ERROR";
export const EDITING_CARD = "EDITING";
class RollsCardToEdit extends Component {

    constructor(props) {
        super(props)
        this.state = {
            state: CREATE_CARD,   //default state , container is created
            products: this.props.products || [],    //the user products quantity
        }
        this.toDeleteProducts = []; //this variable will holds the keys of the deleted products
    }

    /**
     * this will be called when user clicks on the edit button
     */
    editCard = () => {

        //at the application start the state is created and products are empty
        if (this.state.products.length === 0) {

            let newProducts = [...this.state.products];
            newProducts.push(
                {
                    name: null,
                    index: 0,
                    quantity: 0
                }
            );
            this.setState({ products: newProducts });
        }
        this.setState({ state: EDITING_CARD })
    }

    /**
     * each time the user clicks on the + button to add a new product this method will be called
     * it will add a blank product to the list of products
     */
    addNewProduct = () => {
        let newProducts = [...this.state.products];
        newProducts.push(
            {
                name: null,
                index: this.state.products.length,
                quantity: 0
            }
        );
        this.setState({ products: newProducts });
    }

    deleteExistingProduct = (key) => {
        this.toDeleteProducts.push(key);
    }

    handleProductsNameChange = (event, key) => {
        let products = [...this.state.products];
        products[key].name = event.target.value;
        this.setState({ products: products });
    }

    handleProductQuantityChange = (event, key) => {
        let products = [...this.state.products];
        products[key].quantity = event.target.value;
        this.setState({ products: products });
    }

    saveAllProducts = () => {
        let products = [...this.state.products];
        if (this.toDeleteProducts.length > 0){
            products = products.filter(product => !arrayUtils.contains(this.toDeleteProducts, product.index));
            //re index array 
            products.forEach((product, key) => {
                products[key].index = key
            })
        }
        this.setState({ state: SAVING_CARD });

        let rollsDayNode = this.props.rollsDay;
        let rollsMonthNode = rollsDayNode.substring(0, 7);
        base.post(`rollsDinnerMonth/${rollsMonthNode}/${rollsDayNode}/products`, {
            data: products
        }).then(() => {
            this.setState({ state: SAVED_CARD });
        }).catch(err => {
            console.log(err.message)
            this.setState({ state: ERROR_SAVING_CARD });
        });
    }



    render() {

        let screen = null;
        let products = this.state.products;
        if (products && products.length > 0) {
            screen = (
                products.map((product,index) => {
                    let isLastProduct = ((index+1)===products.length) || (products.length===1);
                    return (
                        <Product
                            key={index}
                            name={product.name}
                            quantity={product.quantity}
                            index={product.index}
                            cardState={this.state.state}
                            onQuantityChange={this.handleProductQuantityChange}
                            onNameChange={this.handleProductsNameChange}
                            addNewProduct={this.addNewProduct}
                            removeTheProduct={this.deleteExistingProduct}
                            lastProduct={isLastProduct} 
                        />
                    )
                })
            )
        }

        return (
            <RollsCardLayout
                cardState={this.state.state}
                editCard={this.editCard}
                saveAllProducts={this.saveAllProducts}
                userName={this.props.userName}
                rollsDay={this.props.rollsDay}
            >
                {screen}
            </RollsCardLayout>
        )
    }
}

export default RollsCardToEdit;