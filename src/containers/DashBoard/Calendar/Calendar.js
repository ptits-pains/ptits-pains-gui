import React, { Component } from "react";
import EventCalendar from "../../../components/EventCalendar/EventCalendar";
import SnackBar from '../../../components/UI/SnackBar/SnackBar';
import firebase from '../../../config/firebase';
import * as calendarUtils from '../../../utils/CalendarUtils';
import * as mapUtils from '../../../utils/MapUtils';

/**
 * in this container we will prepare data to be shown in the calendar
 * we will also load all calendar settings
 */
class Calendar extends Component {

    indexedLeavesApiData = null; //this will help us to determine witch day the user clicked

    constructor(props) {
        super(props);
        this.state = {
            selectedDay: null,
            error: null,
            calendarSettings: null,
            loading: false
        };
    }

    //each time the calendar will be mounted we gonna get and store the calendar settings
    componentDidMount() {

        Promise.all([this.setHolidays(), this.setNonWorkingDays(), this.setRollsDay()])
            .then((values) => {
                this.holidaysSettings = values[0];
                this.nonWorkingDays = values[1];
                this.rollsDay = values[2];
            })
    }

    /**
     * the dinner rolls state is in the dashboard container each time this state changed
     * react will call this lifecycle function to update the child (this container)
     * @param {Object} prevProps previous dashboard state
     */
    componentDidUpdate(prevProps) {

        if (
            !mapUtils.compareMaps(this.props.rollsMonth, prevProps.rollsMonth)
            ||
            !this.props.month.isSame(prevProps.month)
        ) { // if the month or the rolls of the month are different we gonna update the component
            this.fillCalendar(
                this.holidaysSettings,
                this.nonWorkingDays,
                this.rollsDay
            )
        }

    }

    setHolidays = () => {

        return new Promise(
            (resolve, reject) => firebase.fetch('holidays', {
                context: this,
            }).then(holidays => {
                resolve(holidays)
            })
        )
    }

    setRollsDay = () => {

        return new Promise(
            (resolve, reject) => firebase.fetch('rollsDay', {
                context: this
            }).then(rollsDay => {
                resolve(rollsDay)
            })
        )
    }

    setNonWorkingDays = () => {

        return new Promise(
            (resolve, reject) => firebase.fetch('nonWorkingDays', {
                context: this,
                asArray: true
            }).then(nonWorkDays => {
                resolve(nonWorkDays)
            })
        )
    }

    /**
     * in this function we will load all data to show in the calendar
     * disable non working days and holidays, show rolls days
     */
    fillCalendar = (holidays, nonWorkingDays, rollsDay) => {
        let settings = calendarUtils.configure(
            nonWorkingDays,
            rollsDay,
            holidays,
            this.props.month,
            this.props.rollsMonth
        );
        this.setState({ calendarSettings: settings })
    }

    setError = (error) => {
        this.setState({
            error: error
        });
    };

    initError = () => {
        this.setState({
            error: null
        });
    };

    render() {
        return (
            <React.Fragment>
                <EventCalendar
                    month={this.props.month}
                    selected={this.state.selectedDay}
                    settings={this.state.calendarSettings}
                    loading={this.state.loading || this.props.loading}
                    previous={this.props.previous}
                    next={this.props.next}
                    selectHoliday={this.props.selectHoliday}
                />
                {this.state.error ? <SnackBar type="error" message={this.state.error} /> : null}
            </React.Fragment>
        );
    }
}


export default Calendar;
