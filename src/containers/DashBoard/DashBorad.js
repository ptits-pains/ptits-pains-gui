import React, { Component } from "react";
import Calendar from './Calendar/Calendar';
import firebase from '../../config/firebase';
import * as mapUtils from '../../utils/MapUtils';
import * as dateUtils from '../../utils/DateUtils';
import moment from "moment";
import RollsList from './RollsList/RollsList';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import * as appRoutes from '../../constants/routes';
import ShowHoliday from "../../components/Navigation/Content/DashBoard/ShowHoliday/ShowHoliday";

class DashBoard extends Component {

    constructor(props) {
        super(props);
        this.state = {
            month: moment(),
            rollsMonth: null,
            selectedHoliday: null,
            loading: false,
            error: false,
            errorMessage: null
        }
    }

    componentDidMount() {
        this.fetchRollsMonth(this.state.month);
    }

    initLoading = () => {
        this.setState({
            loading: true,
            rollsMonth: null,
            error: false,
            errorMessage: null,
            selectedHoliday: null
        })
    }

    setError = (err) => {
        this.setState({
            loading: false,
            error: true,
            errorMessage: err.message
        })
    }

    /**
     * fetch the rollsDinnerMonth node content from real time database
     */
    fetchRollsMonth = month => {

        this.initLoading();
        let clonedMonth = month.clone();
        let monthLabel = this.returnMonthLabel(clonedMonth);
        Promise.all([
            this.getRollsMonth(monthLabel),
            this.getUsers()
        ]).then((values) => {

            let map = mapUtils.fireObjectToMap(values[0]);
            let users = values[1];

            //add users details to the map
            for (let [key, value] of map) {

                let userId = value.userId;
                let userDetails = users[userId];
                map.set(key, { ...value, ...userDetails })
            }
            this.setState({ rollsMonth: map, month: clonedMonth, loading: false })
        }).catch(error => {
            this.setError(error)
        })
    }

    /**
     * get rolls days of the passed month
     */
    getRollsMonth = (monthLabel) => {

        return new Promise(
            (resolve, reject) => firebase.fetch(`rollsDinnerMonth/${monthLabel}`, {
                context: this,
            }).then(data => {
                resolve(data)
            })
        )
    }

    /**
     * get application users
     */
    getUsers = () => {

        return new Promise(
            (resolve, reject) => firebase.fetch(`users`, {
                context: this,
            }).then(users => {
                resolve(users)
            })
        )
    }

    /**
     * calendar previous month button click
     */
    previous = () => {
        let previousMonth = this.state.month.clone().subtract(1, "month");
        this.fetchRollsMonth(previousMonth);
    };

    /**
     * calendar next month button click
     */
    next = () => {
        let nextMonth = this.state.month.clone().add(1, "month");
        this.fetchRollsMonth(nextMonth);
    };

    returnMonthLabel = month => {

        return dateUtils.momentDateToString(month).substring(0,7);
    }

    /**
     * this will be called when you click on a holiday from the calendar
     */
    selectHoliday = day => {

        this.setState({
            selectedHoliday: day
        })
    };

    render() {
        if (!this.props.userId) {
            return <Redirect to={appRoutes.ROOT} />
        }
        return (
            <React.Fragment>
                <Calendar
                    month={this.state.month}
                    rollsMonth={this.state.rollsMonth}
                    previous={this.previous}
                    next={this.next}
                    selectHoliday={this.selectHoliday}
                    loading={this.state.loading}
                />
                {this.state.selectedHoliday ? <ShowHoliday holiday={this.state.selectedHoliday} /> : null}
                <RollsList
                    rollsMonth={this.state.rollsMonth}
                />
            </React.Fragment>
        )
    }
}
const mapStateToProps = state => {
    return {
        userId: state.auth.userId
    };
};

export default connect(mapStateToProps)(DashBoard);